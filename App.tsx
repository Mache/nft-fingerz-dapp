import React from "react";
import { NativeBaseProvider } from "native-base";
import Home from "./src/home";
import AppLoading from "expo-app-loading";
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { initializeAppCheck, ReCaptchaV3Provider } from "firebase/app-check";
import { getDatabase } from "firebase/database";
import MyBg from "./src/home/components/my_bg";
import {
  customTheme,
  dependencies,
  Lerty_Regular,
  Wizland,
} from "./src/settings/theme";
import { useFonts } from "expo-font";
import { Provider } from "react-redux";
import { store } from "./src/settings/store";
import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  HttpLink,
} from "@apollo/client";
import { RetryLink } from "@apollo/client/link/retry";
import { DEV } from "./src/home/contracts/api";

export enum GRAPHQL_PROVIDER {
  TZ_DOMAINS = "TZ_DOMAINS",
  TZ_PROFILES = "TZ_PROFILES",
}
const directionalLink = new RetryLink().split(
  (operation) =>
    operation.getContext().provider === GRAPHQL_PROVIDER.TZ_DOMAINS,
  new HttpLink({ uri: "https://api.tezos.domains/graphql" }),
  new HttpLink({ uri: "https://indexer.tzprofiles.com/v1/graphql" })
);

// Initialize Apollo Client
const client = new ApolloClient({
  link: directionalLink,
  cache: new InMemoryCache(),
});
const config = require("./config.json");

export const app = initializeApp(config.firebase);
const analytics = getAnalytics(app);
if (!DEV) {
  const appCheck = initializeAppCheck(app, {
    provider: new ReCaptchaV3Provider(config.recaptcha.token),
    isTokenAutoRefreshEnabled: true,
  });
}

export const database = getDatabase(app);

const App = () => {
  let [fontsLoaded] = useFonts({
    Lerty_Regular,
    Wizland,
  });

  if (!fontsLoaded) {
    return <AppLoading />;
  }
  return (
    <ApolloProvider client={client}>
      <Provider store={store}>
        <NativeBaseProvider theme={customTheme} config={{ dependencies }}>
          <MyBg>
            <Home />
          </MyBg>
        </NativeBaseProvider>
      </Provider>
    </ApolloProvider>
  );
};
export default App;
