const fs = require("fs");
const rev = () => {
  const head = fs.readFileSync(".git/HEAD").toString().trim();
  let rev = "";
  if (head.indexOf(":") === -1) {
    rev = head;
  } else {
    rev = fs
      .readFileSync(".git/" + head.substring(5))
      .toString()
      .trim();
  }
  fs.writeFileSync("./revision.json", JSON.stringify({ hash: rev }));
};
rev();
