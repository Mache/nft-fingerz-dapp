import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { BeaconWallet } from "@taquito/beacon-wallet";
import { TezosToolkit } from "@taquito/taquito";
import { User } from "firebase/auth";
import { IDiscordIdentity } from "../quest/discord/api";

export interface IQueryParams {
  [key: string]: string;
}
export interface IApiResponse {
  hostname: string;
  path: string;
  queryParams: IQueryParams;
  scheme: string;
}

export enum PROFILE_PROVIDER {
  TZ_DOMAINS = "TZ_DOMAINS",
  TZ_PROFILES = "TZ_PROFILES",
  TZ_FINGERZ = "TZ_FINGERZ",
}
export enum ASYNC_STORAGE {
  TOKEN = "TOKEN",
  CREDENTIAL = "CREDENTIAL",
  UID = "UID",
  PROFILE = "PROFILE",
  COOKIES = "COOKIES",
}

export enum COOKIES_SETTING {
  ACCEPT = "ACCEPT",
}
export interface ITezosProfile {
  [PROFILE_PROVIDER.TZ_DOMAINS]: { alias: null | string };
  [PROFILE_PROVIDER.TZ_PROFILES]: { alias: null | string };
}
export interface IProfile {
  discord: null | IDiscordIdentity;
  wallet: null | ITezosProfile;
}
interface IWalletApi {
  tezos: null | TezosToolkit;
  beacon: null | BeaconWallet;
  address: null | string;
  profile: null | IProfile;
  user: null | User;
  cookies: null | string;
}
const initial_state: IWalletApi = {
  tezos: null,
  beacon: null,
  address: null,
  profile: null,
  user: null,
  cookies: null,
};
const wallet_api = createSlice({
  name: "wallet_api",
  initialState: initial_state,
  reducers: {
    set_tezos: (state, action: PayloadAction<null | TezosToolkit>) => {
      state.tezos = action.payload;
    },
    set_beacon: (state, action: PayloadAction<null | BeaconWallet>) => {
      state.beacon = action.payload;
    },
    set_address: (state, action: PayloadAction<null | string>) => {
      state.address = action.payload;
    },
    set_profile: (state, action: PayloadAction<null | IProfile>) => {
      state.profile = action.payload;
    },
    set_user: (state, action: PayloadAction<null | User>) => {
      state.user = action.payload;
    },
    set_cookies: (state, action: PayloadAction<null | string>) => {
      state.cookies = action.payload;
    },
  },
});

export const {
  set_tezos,
  set_beacon,
  set_address,
  set_profile,
  set_user,
  set_cookies,
} = wallet_api.actions;
export default wallet_api.reducer;
