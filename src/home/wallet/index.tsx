import React, { useEffect } from "react";
import { Box, Button, Text } from "native-base";
import { useDispatch, useSelector } from "react-redux";

import { RootState } from "../../settings/store";
import {
  getWalletData,
  walletConnect,
  walletDisconnect,
  walletStatus,
} from "./api";
import {
  ASYNC_STORAGE,
  COOKIES_SETTING,
  set_address,
  set_cookies,
  set_profile,
  set_user,
} from "./reducers";
import { useApolloClient } from "@apollo/client";
import {
  set_active,
  set_fetching_active,
  set_fetching_inactive,
  set_inactive,
} from "../wanted/reducers";
import { set_loading } from "../leftPanel/reducers";
import { getMissions } from "../wanted/api";
import { getUnixTime } from "../time/api";
import { set_unix_time } from "../time/reducers";
const Wallet = () => {
  const dispatch = useDispatch();
  const {
    wallet: { beacon, address },
    contract: { fa2_storage, salez_storage },
  } = useSelector((state: RootState) => state);
  const client = useApolloClient();

  const onSync = async (current_address: string | null) => {
    let next_address = current_address;
    try {
      if (beacon) {
        dispatch(set_loading(true));
        if (current_address === null) {
          next_address = await walletStatus(beacon);
        }

        const { profile, credential } = await getWalletData(
          next_address,
          client
        );
        dispatch(set_profile(profile));
        dispatch(set_user(credential));
        dispatch(set_address(next_address));
        dispatch(set_loading(false));
        const unix_time = await getUnixTime();
        dispatch(set_unix_time(unix_time));
        if (fa2_storage && salez_storage && next_address) {
          dispatch(set_fetching_active(true));
          dispatch(set_fetching_inactive(true));
          const { active, inactive } = await getMissions(
            salez_storage.rewards_list,
            fa2_storage.ledger,
            fa2_storage.token_metadata,
            next_address,
            unix_time
          );
          dispatch(set_active(active));
          dispatch(set_inactive(inactive));
          dispatch(set_fetching_active(false));
          dispatch(set_fetching_inactive(false));
        }
      }
    } catch (error) {
      console.log(error);
      dispatch(set_loading(false));
    }
  };

  useEffect(() => {
    onSync(address);
  }, [beacon]);

  return (
    <Box justifyContent="center" alignItems="center">
      {address ? (
        <Button
          h={{ base: "20px", lg: "36px" }}
          onPress={() => {
            if (beacon) {
              walletDisconnect(beacon, () => {
                dispatch(set_address(null));
                dispatch(set_user(null));
              });
            }
          }}
        >
          <Text>UNSYC</Text>
        </Button>
      ) : (
        <Button
          h={{ base: "20px", lg: "36px" }}
          onPress={() => {
            if (beacon) {
              walletConnect(beacon, (next_address) => {
                onSync(next_address);
                dispatch(set_address(next_address));
              });
            }
          }}
        >
          <Text>SYNC</Text>
        </Button>
      )}
    </Box>
  );
};

export default Wallet;
