import { BeaconWallet } from "@taquito/beacon-wallet";
import { MichelCodecPacker, TezosToolkit } from "@taquito/taquito";
import { Tzip16Module } from "@taquito/tzip16";
import { getFunctions, httpsCallable } from "firebase/functions";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { DEV, network, RPC_NODE } from "../contracts/api";
import {
  ASYNC_STORAGE,
  PROFILE_PROVIDER,
  IProfile,
  ITezosProfile,
} from "./reducers";
import { ApolloClient, gql } from "@apollo/client";
import { app, database, GRAPHQL_PROVIDER } from "../../../App";
import { getUnixTime } from "../time/api";
import { getMissions } from "../wanted/api";
import {
  IAddress,
  IContractFA2Storage,
  IContractSalezStorage,
} from "../contracts/types";
import { t_mission_list } from "../wanted/reducers";
import {
  browserLocalPersistence,
  getAuth,
  setPersistence,
  signInWithCustomToken,
  User,
} from "firebase/auth";
import { get, ref, update } from "firebase/database";
export const walletStart = () => {
  const tezos = new TezosToolkit(RPC_NODE);
  tezos.setPackerProvider(new MichelCodecPacker());
  tezos.addExtension(new Tzip16Module());
  const beacon = new BeaconWallet({
    name: "FINGERZ TZ - XPENDING MACHINE",
    preferredNetwork: network.type,
  });
  tezos.setWalletProvider(beacon);
  return {
    tezos,
    beacon,
  };
};

export const walletStatus = async (
  beacon: BeaconWallet,
  onStatus?: (address: null | string) => void
) => {
  let address: string | null = null;
  try {
    console.log("Requesting status...");
    if (beacon) {
      const wallet = await beacon.client.getActiveAccount();
      if (wallet) {
        console.log("Already connected:", wallet.address);
        address = wallet.address;
      } else {
        console.log("Not connected!");
      }
    } else {
      throw new Error("BeaconWallet not defined");
    }
  } catch (error) {
    console.log("Error:", error);
  }
  onStatus && onStatus(address);
  return address;
};

export const walletConnect = async (
  beacon: BeaconWallet,
  onConnect?: (address: null | string) => void
) => {
  let address: string | null = null;
  try {
    console.log("Requesting permissions...");
    if (beacon) {
      const permissions = await beacon.client.requestPermissions({
        network: network,
      });
      address = permissions.address;
    } else {
      throw new Error("BeaconWallet not defined");
    }
  } catch (error) {
    console.log("Error:", error);
  }
  onConnect && onConnect(address);
  return address;
};

export const walletDisconnect = async (
  beacon: BeaconWallet,
  onDisconnect?: () => void
) => {
  try {
    if (beacon) {
      //await AsyncStorage.removeItem(ASYNC_STORAGE.TOKEN);
      //await AsyncStorage.removeItem(ASYNC_STORAGE.PROFILE);
      await beacon.clearActiveAccount();
    } else {
      throw new Error("BeaconWallet not defined");
    }
  } catch (error) {
    console.log("Error:", error);
  }
  onDisconnect && onDisconnect();
};

export const getGoogleUID = async (
  address: string,
  onSign?: (token: string) => void
) => {
  let token: string = "";
  try {
    const myFunctions = getFunctions();
    const signIn = httpsCallable<{ address: string }, string>(
      myFunctions,
      "signIn"
    );
    const result = await signIn({
      address,
    });

    token = result.data;
  } catch (error) {
    console.log(error);
  }
  onSign && onSign(token);
  return token;
};

type t_tz_domain_result = {
  reverseRecord: {
    domain: {
      name: string;
    };
  };
};
type t_tz_profile_result = {
  tzprofiles: {
    alias: string;
  }[];
};

const GET_TZ_DOMAINS = gql`
  query getExternalProfile($address: String!) {
    reverseRecord(address: $address) {
      domain {
        name
        data {
          key
          value
        }
      }
    }
  }
`;
const GET_TZ_PROFILES = gql`
  query getExternalProfile($address: String!) {
    tzprofiles(where: { account: { _eq: $address } }) {
      alias
    }
  }
`;
export const getProfile = async (address: IAddress) => {
  let profile: null | IProfile = null;
  try {
    const profile_ref = ref(database, `profiles/${address}`);
    const query_profile = await get(profile_ref);
    if (query_profile.exists()) {
      profile = query_profile.val() as IProfile;
    }
  } catch (error) {
    console.log(error);
  }
  return profile;
};
export const getExternalProfile = async (
  client: ApolloClient<object>,
  address: string
) => {
  let profile: ITezosProfile = {
    [PROFILE_PROVIDER.TZ_DOMAINS]: { alias: null },
    [PROFILE_PROVIDER.TZ_PROFILES]: { alias: null },
  };
  try {
    const tz_domains_result = await client.query<t_tz_domain_result>({
      query: GET_TZ_DOMAINS,
      variables: { address },
      context: { provider: GRAPHQL_PROVIDER.TZ_DOMAINS },
    });
    if (
      tz_domains_result.data.reverseRecord &&
      tz_domains_result.data.reverseRecord.domain
    ) {
      const alias = tz_domains_result.data.reverseRecord.domain.name as string;
      profile = { ...profile, [PROFILE_PROVIDER.TZ_DOMAINS]: { alias } };
    }
    const tz_profiles_result = await client.query<t_tz_profile_result>({
      query: GET_TZ_PROFILES,
      variables: { address },
      context: { provider: GRAPHQL_PROVIDER.TZ_PROFILES },
    });
    if (tz_profiles_result.data.tzprofiles.length > 0) {
      const { alias } = tz_profiles_result.data.tzprofiles[0];
      profile = { ...profile, [PROFILE_PROVIDER.TZ_PROFILES]: { alias } };
    }
    const wallet_profile_ref = ref(database, `profiles/${address}/wallet`);
    await update(wallet_profile_ref, profile);
  } catch (error) {
    console.log(error);
  }
  return profile;
};

export const getWalletData = async (
  address: string | null,
  apollo_client: ApolloClient<object>
) => {
  let profile: null | IProfile = null;
  let credential: User | null = null;
  try {
    if (address) {
      const token = await getGoogleUID(address);
      const auth = getAuth(app);
      await setPersistence(auth, browserLocalPersistence);
      if (!auth.currentUser) {
        const result = await signInWithCustomToken(auth, token);
        credential = result.user;
      } else {
        credential = auth.currentUser;
      }
      const query_profile = await getProfile(address);
      profile = query_profile;

      if (!query_profile?.wallet) {
        const query_external_profile = await getExternalProfile(
          apollo_client,
          address
        );
        if (query_external_profile) {
          if (profile) {
            profile.wallet = query_external_profile;
          } else {
            profile = { wallet: query_external_profile, discord: null };
          }
        }
      }
    }
  } catch (error) {
    console.log(error);
  }
  return {
    profile,
    credential,
  };
};
