import React, { useState } from "react";
import {
  Box,
  Button,
  Input,
  InputGroup,
  InputLeftAddon,
  Stack,
  TextArea,
  VStack,
  Text,
  Heading,
  Image,
} from "native-base";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../settings/store";
import { set_score, set_token_id } from "./reducers";
import { calculateRarity, getTokenMetadata } from "./api";
import { DEV } from "../contracts/api";
type t_range_props = {
  score: number;
};
const Range = ({ score }: t_range_props) => {
  if (score >= 201) {
    if (score >= 251) {
      if (score >= 301) {
        if (score >= 401) {
          if (score >= 801) {
            if (score >= 1001) {
              if (score >= 1301) {
                if (score >= 1501) {
                  if (score >= 1701) {
                    return <Heading color="#ffffe5">IMMORTAL</Heading>;
                  } else {
                    return <Heading color="#ffd600">ANCIENT</Heading>;
                  }
                } else {
                  return <Heading color="#ff9f0c">LEGENDARY</Heading>;
                }
              } else {
                return <Heading color="#ff610c">MYTHICAL</Heading>;
              }
            } else {
              return <Heading color="#ff0033">EPIC</Heading>;
            }
          } else {
            return <Heading color="#ff33cc">EXOTIC</Heading>;
          }
        } else {
          return <Heading color="#9933cc">ULTRA RARE</Heading>;
        }
      } else {
        return <Heading color="#3366ff">RARE</Heading>;
      }
    } else {
      return <Heading color="#06B300">UNCOMMON</Heading>;
    }
  } else {
    return <Heading color="#666666">COMMON</Heading>;
  }
};
const Rarity = () => {
  const {
    contract: { fa2_storage },
    rarity: { token_id, score },
  } = useSelector((state: RootState) => state);
  const dispatch = useDispatch();
  const [fetching, setFetching] = useState(false);
  return (
    <Box
      alignItems="center"
      //bg="gray.900"
      //rounded="md"
      w={{ base: "350px", lg: "750px" }}
      minH={{ base: "250px", lg: "550px" }}
    >
      <Heading>RARITY</Heading>
      <Box
        mt="10px"
        bg="gray.900"
        rounded="md"
        p={{ base: "10px" }}
        w={{ base: "100%" }}
      >
        <Text textAlign={{ base: "center" }}>
          The higher the score the rarest your fingerz are.
        </Text>
      </Box>
      <Box
        m={{ base: "10px" }}
        flexDirection={{ base: "column", lg: "row" }}
        justifyContent={{ base: "center", lg: "space-around" }}
        alignItems={{ base: "center", lg: "start" }}
      >
        <VStack
          space={{ base: "10px" }}
          mt={{ lg: "25px" }}
          w={{ lg: "400px" }}
        >
          <Stack alignItems="center">
            <InputGroup w="100%">
              <InputLeftAddon children={"TOKEN ID #"} />
              <Input
                value={`${token_id}`}
                onChangeText={(text) => {
                  const my_score = Number.parseInt(text);
                  if (Number.isInteger(my_score) || text === "") {
                    dispatch(set_token_id(text));
                  }
                }}
                w="100%"
                placeholder="NUMBER"
              />
            </InputGroup>
          </Stack>
          <Button
            disabled={fetching}
            isLoading={fetching}
            onPress={() => {
              if (fa2_storage && token_id !== "") {
                setFetching(true);
                const my_token = Number.parseInt(token_id, 10);
                getTokenMetadata(fa2_storage, my_token, (metadata) => {
                  calculateRarity(metadata, (score) => {
                    dispatch(set_score(score));
                    setFetching(false);
                  });
                  //dispatch(set_metadata(metadata));
                }).catch(() => setFetching(false));
              }
            }}
          >
            <Text>GIVE ME THE SCORE</Text>
          </Button>
          <Heading>{`SCORE #${score !== null ? score : 0}`}</Heading>
          {score !== null ? <Range score={score} /> : null}
        </VStack>
        <Box>
          <Image
            alt="Rarity"
            w={{ base: "200px", lg: "300px" }}
            h={{ base: "250px", lg: "375px" }}
            src={require("../../../assets/rareza.v2.png")}
            resizeMode="center"
          />
        </Box>
      </Box>
    </Box>
  );
};

export default Rarity;
