import axios from "axios";
import {
  IContractFA2Storage,
  IContractTokenMetadata,
} from "../contracts/types";
import { unpackData } from "../tools/d_api";
import { ITokenMetadata } from "../tools/types";

export const getTokenMetadata = async (
  fa2_storage: IContractFA2Storage,
  token_id: number,
  onMetadata: (metadata: ITokenMetadata) => void
) => {
  try {
    const query_token_metadata =
      await fa2_storage.token_metadata.get<IContractTokenMetadata>(token_id);
    if (query_token_metadata) {
      const query_token_info = query_token_metadata.token_info.get("");
      if (query_token_info) {
        const query_metadata: string = Object.values(
          unpackData<{ [key: string]: string }>(query_token_info, {
            prim: "string",
          })
        )[0]; //as string;
        //console.log("token_metadata_uri: ", query_metadata);
        const metadata = await axios.get<ITokenMetadata>(
          query_metadata.replace("ipfs://", "https://gateway.ipfs.io/ipfs/")
        );
        //console.log("token_metadata: ", metadata.data);
        onMetadata(metadata.data);
      }
    }
  } catch (error) {}
};
export interface ITraits {
  [name: string]: string;
}
export interface ICategory {
  totalWeight: string;
  traits: ITraits;
}

export type IAssets = { [category_name: string]: ICategory };

export const golden: { [level: string]: number } = {
  "0": 0,
  "1": 100,
  "2": 150,
  "3": 230,
  "4": 350,
  "5": 530,
  "6": 800,
  "7": 1200,
};
export const calculateRarity = (
  metadata: ITokenMetadata,
  onScore: (score: number) => void
) => {
  let score = 0;
  let level = 0;
  try {
    const rarities = require("./rarity.json") as IAssets;
    for (let attr of metadata.attributes) {
      const category = attr.name.replace("golden ", "").trim();
      if (category !== "level") {
        const trait = attr.value;
        const query_rarity = rarities[category].traits[trait];
        const rarity = Number.parseInt(query_rarity, 10);
        const query_weight = rarities[category].totalWeight;
        const totalWeight = Number.parseInt(query_weight, 10);
        console.log("category", category);
        console.log("trait", trait);
        console.log("rarity", rarity);
        console.log("totalWeight", totalWeight);
        score += totalWeight / rarity;
      } else {
        level = Number.parseInt(attr.value, 10);
        console.log("level #", level);
      }
      const bonuz = golden[`${level}`];
      console.log("bonuz #", bonuz);
      score += bonuz;
    }
    console.log(score);
    onScore(Math.floor(score));
    //console.log(rarities);
  } catch (error) {
    console.log(error);
  }
};
