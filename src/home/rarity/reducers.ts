import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ITokenMetadata } from "../tools/types";

interface t_rarity_api {
  token_id: string;
  metadata: ITokenMetadata | null;
  score: null | number;
}
const initial_state: t_rarity_api = {
  token_id: "",
  metadata: null,
  score: null,
};
const rarity_api = createSlice({
  name: "rarity_api",
  initialState: initial_state,
  reducers: {
    set_token_id: (state, action: PayloadAction<string>) => {
      state.token_id = action.payload;
    },
    set_metadata: (state, action: PayloadAction<null | ITokenMetadata>) => {
      state.metadata = action.payload;
    },
    set_score: (state, action: PayloadAction<null | number>) => {
      state.score = action.payload;
    },
  },
});

export const { set_token_id, set_metadata, set_score } = rarity_api.actions;
export default rarity_api.reducer;
