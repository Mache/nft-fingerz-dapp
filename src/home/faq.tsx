import React from "react";
import { Box, Heading, HStack, Text, VStack } from "native-base";
import MySpeech from "./components/my_speech";

const Faq = () => {
  const data = [
    {
      question: "How many Fingerz are there?",
      answer: "10,000",
    },
    {
      question: "When is the launch date?",
      answer:
        "Pre-Sale: July 30th\nMain sale : July 31th\nFingerz Reveal : August 2nd",
    },
    {
      question: "How to mint?",
      answer: "1) Sync your wallet\n2) Type in your amount\n3) Press MINT!",
    },
    {
      question: "What happens after i mint?",
      answer:
        "After minting your Fingerz, the tokens will be visible in your wallet. Initially, you will only see an unknown symbol token and all features will be hidden.\nOnly after the sale ends, the actual Fingerz will be revealed to you. This is scheduled to happen 48hs after the sale ends.",
    },
    {
      question: "Rewards?",
      answer:
        "A pool made of minting and royalties share, if you manage to collect the specifics Fingerz listed on the mission you will be able to collect the prize",
    },
    {
      question: "What's a Dynamic NFT?",
      answer:
        "It's a piece of art that can permanently upgrade it's traits through contract interactions",
    },
  ];
  return (
    <Box alignItems="center" my="10px">
      <Heading>FAQ</Heading>
      {data.map((dialog, index) => {
        return (
          <Box w={{ base: "300px", lg: "750px" }} key={index}>
            <Box
              alignSelf="flex-start"
              marginRight={{ base: "25px", lg: "100px" }}
              my="10px"
            >
              <MySpeech text={`[Q] ${dialog.question}`} />
            </Box>
            <Box
              alignSelf="flex-end"
              marginLeft={{ base: "25px", lg: "100px" }}
              my="10px"
            >
              <MySpeech text={`[A] ${dialog.answer}`} />
            </Box>
          </Box>
        );
      })}
      <Box px="10px" w="100%">
        <Box
          bg="gray.900"
          rounded="md"
          py="5px"
          w="100%"
          justifyContent="center"
          alignItems="center"
        >
          <Text>For more info check out our Discord...</Text>
        </Box>
      </Box>
    </Box>
  );
};

export default Faq;
