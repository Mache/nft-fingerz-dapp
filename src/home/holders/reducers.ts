import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ISiteTab, ISiteTabs } from "../leftPanel/reducers";
export interface ISiteHolderTab extends Omit<ISiteTab, "destination"> {
  destination: HOLDERS_TAB;
}
export interface ISiteHolderTabs {
  [tab: string]: ISiteHolderTab;
}
export enum HOLDERS_TAB {
  VERIFY = "VERIFY",
  MINE = "MINE",
  QUESTS = "QUESTS",
  WANTED = "WANTED",
}
interface IHolderApi {
  tab: HOLDERS_TAB;
}
const initialState: IHolderApi = {
  tab: HOLDERS_TAB.QUESTS,
};
const holders_api = createSlice({
  name: "holders_api",
  initialState: initialState,
  reducers: {
    set_holders_tab: (state, action: PayloadAction<HOLDERS_TAB>) => {
      state.tab = action.payload;
    },
  },
});

export const { set_holders_tab } = holders_api.actions;
export default holders_api.reducer;
