import {
  Box,
  Button,
  HStack,
  useBreakpointValue,
  VStack,
  Text,
} from "native-base";
import React from "react";
import { HOLDERS_TAB, set_holders_tab, ISiteHolderTabs } from "./reducers";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import Quest from "../quest";
import { useDispatch, useSelector } from "react-redux";
import Wanted from "../wanted";
import { RootState } from "../../settings/store";
import Mine from "../mine";
import { DEV } from "../contracts/api";

const holders_tabs: ISiteHolderTabs = {
  [HOLDERS_TAB.QUESTS]: {
    icon: (size) => (
      <MaterialCommunityIcons
        name="comment-question"
        size={size}
        color="white"
      />
    ),
    //label: `${HOLDERS_TAB.QUESTS}`,
    component: Quest,
    destination: HOLDERS_TAB.QUESTS,
  },
  [HOLDERS_TAB.WANTED]: {
    icon: (size) => (
      <MaterialCommunityIcons
        name="account-cowboy-hat"
        size={size}
        color="white"
      />
    ),
    //label: `${HOLDERS_TAB.WANTED}`,
    component: Wanted,
    destination: HOLDERS_TAB.WANTED,
  },
  [HOLDERS_TAB.MINE]: {
    icon: (size) => (
      <MaterialCommunityIcons
        name="account-hard-hat"
        size={size}
        color="white"
      />
    ),
    //label: `${HOLDERS_TAB.WANTED}`,
    component: Mine,
    destination: HOLDERS_TAB.MINE,
    private: !DEV,
  },
};
type t_props = {
  tabs: ISiteHolderTabs;
};
const Tab = ({ tabs }: t_props) => {
  const dispatch = useDispatch();
  const iconSize = useBreakpointValue({
    base: 22,
    lg: 36,
  });
  return (
    <HStack space={{ base: "5px" }} justifyContent={{ base: "center" }}>
      {Object.keys(tabs).map((name) => {
        return tabs[name].private ? null : (
          <Box key={name}>
            <Button
              onPress={() => {
                dispatch(set_holders_tab(tabs[name].destination));
              }}
              leftIcon={tabs[name].icon(iconSize)}
            >
              <Text>{tabs[name].label || name}</Text>
            </Button>
          </Box>
        );
      })}
    </HStack>
  );
};
const Screen = ({ tabs }: t_props) => {
  const { tab } = useSelector((state: RootState) => state.holders);
  const Component = tabs[tab].component;
  return <Component />;
};
const Holders = () => {
  return (
    <Box
      //bg="black"
      //rounded="md"
      w={{ base: "100%" }}
      //minH={{ base: "250px", lg: "550px" }}
      //justifyContent={{ base: "start" }}
      //p={{ base: "10px" }}
    >
      <VStack>
        <Box my={{ base: "10px" }}>
          <Tab tabs={holders_tabs} />
        </Box>
        <Box>
          <Screen tabs={holders_tabs} />
        </Box>
      </VStack>
    </Box>
  );
};
export default Holders;
