import React from "react";
import { Box, HStack, Image, Text, useBreakpointValue } from "native-base";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import Wallet from "./wallet";

const Top = () => {
  const iconSize = useBreakpointValue({
    base: 22,
    lg: 50,
  });

  return (
    <Box
      position="fixed"
      top="0"
      zIndex="1"
      maxW="80%"
      alignSelf="center"
      bg={{
        linearGradient: {
          colors: ["fingerz.200", "fingerz.0"],
          start: [1, 0],
          end: [0, 1],
        },
      }}
      roundedBottom="3xl"
      borderBottomWidth="3px"
      borderBottomColor="gray.800"
      borderRightWidth="3px"
      borderRightColor="gray.800"
      borderLeftWidth="3px"
      borderLeftColor="gray.800"
      w="100%"
      h={{
        base: "50px",
        lg: "100px",
      }}
      px="30px"
      shadow="8"
    >
      <Box flex={1} pt="5px" alignSelf="start">
        <Image
          alt="Logo"
          src={require("../../assets/logo.png")}
          width={{
            base: "100px",
            lg: "200px",
          }} //"100px"
          height={{
            base: "30px",
            lg: "60px",
          }} //"30px"
          resizeMode="center"
        />
      </Box>

      <HStack
        justifyContent="center"
        alignItems="center"
        space="10px"
        flex={1}
        alignSelf="end"
        pb="5px"
      >
        <MaterialCommunityIcons
          name="twitter"
          size={iconSize}
          color="white"
          onPress={() => {
            window.open("https://twitter.com/NFTFingerz", "_blank");
          }}
        />
        <MaterialCommunityIcons
          name="youtube"
          size={iconSize}
          color="white"
          onPress={() => {
            window.open(
              "https://www.youtube.com/channel/UCDgfZRcWrTrAa0YfL18bn4Q",
              "_blank"
            );
          }}
        />
        <MaterialCommunityIcons
          name="discord"
          size={iconSize}
          color="white"
          onPress={() => {
            window.open("https://discord.gg/UxB82twcym", "_blank");
          }}
        />

        <Wallet />
      </HStack>
    </Box>
  );
};

export default Top;
