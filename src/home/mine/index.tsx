import React from "react";
import {
  Box,
  Text,
  Heading,
  Stack,
  InputGroup,
  InputLeftAddon,
  Input,
  Button,
  Pressable,
  Divider,
  HStack,
  Spinner,
  Image,
} from "native-base";

import { MaterialCommunityIcons } from "@expo/vector-icons";
import { useDispatch, useSelector } from "react-redux";
import {
  add_miner,
  remove_miner,
  set_fetching,
  set_manager,
  set_manager_error,
  set_manager_id,
  set_miners,
  set_miner_error,
  set_miner_id,
  ITokenInfo,
} from "./reducers";
import { RootState } from "../../settings/store";
import { ownership, send } from "./api";
const Fetching = () => {
  return (
    <HStack space={2} justifyContent="center" m="20px" w="500px">
      <Spinner accessibilityLabel="Loading" size="lg" />
      <Box h="100px" justifyContent="center">
        <Heading color="primary.500" fontSize="lg">
          Loading
        </Heading>
      </Box>
    </HStack>
  );
};
type Props = {
  token_info: ITokenInfo | null;
  onCancel: (token_id: number) => void;
};
const MinerContainer = ({ token_info, onCancel }: Props) => {
  return (
    <Box
      bg="gray.600"
      shadow="2"
      justifyContent="center"
      w={{ base: "80px", lg: "80px" }}
      h={{ base: "80px", lg: "80px" }}
      p="5px"
      m="5px"
      rounded="md"
      alignItems="center"
    >
      {token_info ? (
        <Box justifyContent="center" alignItems="center" w="100%" h="100%">
          <Text>{`ID #${token_info.token_id}`}</Text>
          <Text>{`EXP #${token_info.exp}`}</Text>
          <Text>{`THREAD LVL #${token_info.level}`}</Text>
          <Pressable
            onPress={() => {
              onCancel(token_info.token_id);
            }}
          >
            <MaterialCommunityIcons
              name="close-thick"
              size={24}
              color="black"
            />
          </Pressable>
        </Box>
      ) : (
        <MaterialCommunityIcons name="plus" size={24} color="black" />
      )}
    </Box>
  );
};
const Mine = () => {
  const {
    mine: {
      manager,
      manager_id,
      miner_id,
      miners,
      fetching,
      miner_error,
      manager_error,
    },
    contract: { fa2_storage },
    wallet: { address, tezos },
  } = useSelector((state: RootState) => state);
  const dispatch = useDispatch();
  if (fetching) {
    return <Fetching />;
  }
  return (
    <Box
      alignItems="center"
      bg="black"
      rounded="md"
      w={{ base: "350px", lg: "750px" }}
      minH={{ base: "250px", lg: "550px" }}
    >
      <Box m="10px" alignItems="center">
        <Box mt="10px">
          <Heading textAlign={{ base: "center" }}>GOLD FEVER</Heading>
          <Text>
            Peace has been interrupted in the world of the fingerz, the gold
            fever lurks and corruption has invaded the heart of the peaceful
            community, now they are sending their mates to work in the mines to
            dig the gold, the bloody gold stains their traits the more they
            succumb to greed.
          </Text>
        </Box>
        <HStack
          /*bg="gray.800"
          rounded="md"*/
          w="100%"
          m="10px"
          p="10px"
          //alignItems="center"
          justifyContent="space-evenly"
        >
          {[0, 1, 2].map((key) => {
            return (
              <Box bg="white" p="2px" shadow="2" key={key}>
                <Image
                  alt="Logo"
                  src={require("../../../assets/wanted.png")}
                  width={{ base: "80px", lg: "120px" }}
                  height={{ base: "100px", lg: "150px" }}
                  resizeMode="center"
                />
              </Box>
            );
          })}
        </HStack>

        <Box
          borderColor="gray.600"
          borderRadius="md"
          borderWidth="3px"
          w="100%"
          justifyContent="center"
          alignItems="center"
          p="10px"
        >
          <Heading>FOREMAN</Heading>
          <Stack alignItems="center" w="100%">
            <InputGroup w="100%">
              <InputLeftAddon children={"ID #:"} />
              <Input
                value={`${manager_id}`}
                onChangeText={(text) => {
                  const value = Number.parseInt(text, 10);
                  if (Number.isInteger(value)) {
                    dispatch(set_manager_id(value));
                  }
                }}
                w="100%"
                placeholder="address"
              />
            </InputGroup>
            <Button
              mt="10px"
              w="100%"
              onPress={() => {
                //check ownership then
                if (fa2_storage && address) {
                  dispatch(set_fetching(true));
                  ownership(
                    fa2_storage,
                    address,
                    manager_id,
                    ({ owned, level, exp }) => {
                      dispatch(set_fetching(false));
                      if (owned) {
                        dispatch(set_manager_id(0));
                        dispatch(set_manager_error(null));
                        dispatch(
                          set_manager({ token_id: manager_id, level, exp })
                        );
                      } else {
                        dispatch(
                          set_manager_error(`TOKEN ID #${manager_id} NOT OWNED`)
                        );
                      }
                    }
                  );
                }
              }}
            >
              SET MANAGER
            </Button>
          </Stack>
          <Box w="100%" my="10px">
            {manager_error ? (
              <HStack w="100%" bg="gray.600" rounded="md" px="10px">
                <MaterialCommunityIcons name="alert" size={24} color="black" />
                <Text>{manager_error}</Text>
              </HStack>
            ) : null}
          </Box>
          <Box justifyContent="center" alignItems="center">
            <MinerContainer
              token_info={manager}
              onCancel={() => dispatch(set_manager(null))}
            />
          </Box>

          <Divider thickness="2" my="10px" />
          <Heading>WORKERS</Heading>
          <Text>Be aware it's said that mine workers never come back</Text>
          <Stack alignItems="center" w="100%">
            <InputGroup w="100%">
              <InputLeftAddon children={"ID #:"} />
              <Input
                value={`${miner_id}`}
                onChangeText={(text) => {
                  const value = Number.parseInt(text, 10);
                  if (Number.isInteger(value)) {
                    dispatch(set_miner_id(value));
                  }
                }}
                w="100%"
                placeholder="address"
              />
            </InputGroup>
            <Button
              mt="10px"
              w="100%"
              onPress={() => {
                if (fa2_storage && address) {
                  dispatch(set_fetching(true));
                  ownership(
                    fa2_storage,
                    address,
                    miner_id,
                    ({ owned, level, exp }) => {
                      dispatch(set_fetching(false));
                      if (owned) {
                        dispatch(set_miner_id(0));
                        dispatch(set_miner_error(null));
                        dispatch(add_miner({ token_id: miner_id, level, exp }));
                      } else {
                        dispatch(
                          set_miner_error(`TOKEN ID #${miner_id} NOT OWNED`)
                        );
                      }
                    }
                  );
                }
              }}
            >
              ADD WORKER
            </Button>
          </Stack>
          <Box w="100%" my="10px">
            {miner_error ? (
              <HStack w="100%" bg="gray.600" rounded="md" px="10px">
                <MaterialCommunityIcons name="alert" size={24} color="black" />
                <Text>{miner_error}</Text>
              </HStack>
            ) : null}
          </Box>
          <HStack>
            {miners.map((miner) => {
              return (
                <MinerContainer
                  token_info={miner}
                  key={miner.token_id}
                  onCancel={(token_id) => dispatch(remove_miner(token_id))}
                />
              );
            })}
          </HStack>
          <Divider thickness="2" my="10px" />
          <Box w="100%">
            {miners.length > 0 && manager !== null ? (
              <Button
                onPress={async () => {
                  if (tezos) {
                    dispatch(set_fetching(true));
                    await send(
                      tezos,
                      manager.token_id,
                      miners.map((miner) => miner.token_id)
                    );
                    dispatch(set_fetching(false));
                    dispatch(set_miners([]));
                    dispatch(set_manager(null));
                  }
                }}
              >
                SEND WORKERS
              </Button>
            ) : null}
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default Mine;
