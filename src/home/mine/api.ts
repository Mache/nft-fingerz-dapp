import {
  IContractFA2Storage,
  IContractTokenMetadata,
} from "../contracts/types";
import { unpackDataBytes } from "@taquito/michel-codec";
import { OpKind, TezosToolkit } from "@taquito/taquito";
import { SALE_CONTRACT_ADDRESS } from "../contracts/api";

export const send = async (
  tezos: TezosToolkit,
  manager: number,
  miners: number[]
) => {
  try {
    const contract = await tezos.wallet.at(SALE_CONTRACT_ADDRESS);
    const operation = contract.methods.feed(miners, manager).toTransferParams();
    //const operation_cost = await tezos.estimate.transfer(operation);
    //console.log(operation_cost);
    const batch_op = await tezos.wallet.batch([
      {
        kind: OpKind.TRANSACTION,

        ...operation,
        //amount: quantity * cost + operation_cost.totalCost,
        //mutez: true,
      },
    ]);
    const batch_response = await batch_op.send();
    batch_response.confirmation();
  } catch (error) {
    console.log(error);
  }
  return null;
};
const unpackData = (
  bytes: string,
  type: any // { prim: string }
) => {
  return unpackDataBytes({ bytes }, type);
};
export const ownership = async (
  fa2_storage: IContractFA2Storage,
  address: string,
  token_id: number,
  onOwnership: ({
    owned,
    level,
    exp,
  }: {
    owned: boolean;
    level: number;
    exp: number;
  }) => void
) => {
  try {
    const query_token = await fa2_storage.ledger.get<string>(token_id);

    if (address) {
      const owned = query_token === address;
      if (owned) {
        const query_metadata =
          await fa2_storage.token_metadata.get<IContractTokenMetadata>(
            token_id
          );
        if (query_metadata) {
          const query_level = query_metadata.token_info.get("level");
          const query_exp = query_metadata.token_info.get("exp");
          if (query_level && query_exp) {
            const unpacked_level = Object.values(
              unpackData(query_level, { prim: "nat" })
            )[0] as string;
            const unpacked_exp = Object.values(
              unpackData(query_exp, { prim: "nat" })
            )[0] as string;
            const exp = Number.parseInt(unpacked_exp, 10);
            const level = Number.parseInt(unpacked_level, 10);
            onOwnership({ owned, level, exp });
          }
        }
      } else {
        onOwnership({ owned: false, level: 0, exp: 0 });
      }
    }
  } catch (error) {
    console.log(error);
  }
};
