import { createSlice, PayloadAction } from "@reduxjs/toolkit";
export interface ITokenInfo {
  token_id: number;
  exp: number;
  level: number;
}
interface IMineApi {
  manager: ITokenInfo | null;
  manager_error: null | string;
  fetching: boolean;
  manager_id: number;
  miner_id: number;
  miners: ITokenInfo[];
  miner_error: null | string;
}
const initial_state: IMineApi = {
  manager: null,
  manager_error: null,
  miner_error: null,
  fetching: false,
  manager_id: 0,
  miner_id: 0,
  miners: [],
};
const mine_api = createSlice({
  name: "mine_api",
  initialState: initial_state,
  reducers: {
    set_fetching: (state, action: PayloadAction<boolean>) => {
      state.fetching = action.payload;
    },
    set_manager_error: (state, action: PayloadAction<null | string>) => {
      state.manager_error = action.payload;
    },
    set_miner_error: (state, action: PayloadAction<null | string>) => {
      state.miner_error = action.payload;
    },
    set_manager: (state, action: PayloadAction<null | ITokenInfo>) => {
      state.manager = action.payload;
    },
    set_manager_id: (state, action: PayloadAction<number>) => {
      state.manager_id = action.payload;
    },
    set_miners: (state, action: PayloadAction<ITokenInfo[]>) => {
      state.miners = action.payload;
    },
    set_miner_id: (state, action: PayloadAction<number>) => {
      state.miner_id = action.payload;
    },
    add_miner: (state, action: PayloadAction<ITokenInfo>) => {
      if (
        state.miners.findIndex(
          (miner) => miner.token_id === action.payload.token_id
        ) < 0
      ) {
        state.miners = state.miners.concat(action.payload);
      }
    },
    remove_miner: (state, action: PayloadAction<number>) => {
      if (
        state.miners.findIndex((miner) => miner.token_id === action.payload) > 0
      ) {
        state.miners = state.miners.filter(
          (miner) => miner.token_id !== action.payload
        );
      }
    },
  },
});

export const {
  set_manager,
  set_miners,
  add_miner,
  remove_miner,
  set_manager_id,
  set_miner_id,
  set_fetching,
  set_manager_error,
  set_miner_error,
} = mine_api.actions;
export default mine_api.reducer;
