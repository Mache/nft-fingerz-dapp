import React from "react";
import {
  Box,
  Center,
  Heading,
  HStack,
  Image,
  Text,
  useBreakpointValue,
  VStack,
} from "native-base";

import { MaterialCommunityIcons } from "@expo/vector-icons";

const Team = () => {
  const iconSize = useBreakpointValue({
    base: 22,
    lg: 30,
  });
  return (
    <Box alignContent="center" alignItems="center" w="100%">
      <Heading>TEAM</Heading>
      <HStack
        justifyContent="center"
        alignItems="center"
        w={{ base: "350px", lg: "750px" }}
        h={{ base: "90px", lg: "150px" }}
      >
        <Box
          justifyContent="center"
          alignItems="center"
          bg="gray.900"
          rounded="md"
          p="15px"
          m="5px"
        >
          <Box w={{ base: "260px", lg: "600px" }}>
            <Text>
              We are a small group of 3 persons, 2 artist/graphic designers and
              1 coder.
            </Text>
          </Box>
        </Box>
        <Box w={{ base: "90px", lg: "150px" }}>
          <Image
            alt="Team"
            src={require("../../assets/team.png")}
            width={{ base: "90px", lg: "150px" }}
            height={{ base: "90px", lg: "150px" }}
            rounded="full"
            resizeMode="center"
            borderColor="gray.800"
            borderWidth="3px"
            zIndex="1"
          />
        </Box>
      </HStack>
      <HStack justifyContent="center" alignItems="center" w="100%" mt="10px">
        <Box
          bg="gray.900"
          rounded="md"
          p="15px"
          m="5px"
          w={{ base: "125px", lg: "250px" }}
          h={{ base: "150px", lg: "200px" }}
        >
          <Heading>M88ache</Heading>
          <Text bold>Marcelo David Moises</Text>
          <Text>Code Developer</Text>
          <Box flex={1} justifyContent="end">
            <HStack alignSelf="end">
              <MaterialCommunityIcons
                name="linkedin"
                size={iconSize}
                color="white"
                onPress={() => {
                  window.open(
                    "https://linkedin.com/in/marcelo-david-moises-3650ba136",
                    "_blank"
                  );
                }}
              />
              <MaterialCommunityIcons
                name="twitter"
                size={iconSize}
                color="white"
                onPress={() => {
                  window.open("https://twitter.com/m88ache", "_blank");
                }}
              />
              <MaterialCommunityIcons
                name="instagram"
                size={iconSize}
                color="white"
                onPress={() => {
                  window.open("https://www.instagram.com/m88ache/", "_blank");
                }}
              />
            </HStack>
          </Box>
        </Box>

        <Box
          bg="gray.900"
          rounded="md"
          p="15px"
          m="5px"
          w={{ base: "125px", lg: "250px" }}
          h={{ base: "150px", lg: "200px" }}
        >
          <Heading>POGO</Heading>
          <Text bold>Daniel Gonzalo Perona</Text>
          <Text>Graphic Designer/Artist</Text>
          <Box flex={1} justifyContent="end">
            <HStack alignSelf="end">
              <MaterialCommunityIcons
                name="twitch"
                size={iconSize}
                color="white"
                onPress={() => {
                  window.open("https://www.twitch.tv/danilamu3rte", "_blank");
                }}
              />
              <MaterialCommunityIcons
                name="twitter"
                size={iconSize}
                color="white"
                onPress={() => {
                  window.open("https://twitter.com/pogocollection", "_blank");
                }}
              />
              <MaterialCommunityIcons
                name="instagram"
                size={iconSize}
                color="white"
                onPress={() => {
                  window.open(
                    "https://www.instagram.com/danilamuerte23/",
                    "_blank"
                  );
                }}
              />
            </HStack>
          </Box>
        </Box>
        <Box
          bg="gray.900"
          rounded="md"
          p="15px"
          m="5px"
          w={{ base: "125px", lg: "250px" }}
          h={{ base: "150px", lg: "200px" }}
        >
          <Heading>Luchota</Heading>
          <Text bold>Luciano Caro</Text>
          <Text>Graphic Designer/Artist</Text>
          <Box flex={1} justifyContent="end">
            <HStack alignSelf="end">
              <MaterialCommunityIcons
                name="twitch"
                size={iconSize}
                color="white"
                onPress={() => {
                  window.open("https://www.twitch.tv/lucho_982", "_blank");
                }}
              />
              <MaterialCommunityIcons
                name="twitter"
                size={iconSize}
                color="white"
                onPress={() => {
                  window.open("https://twitter.com/luchota982", "_blank");
                }}
              />
              <MaterialCommunityIcons
                name="instagram"
                size={iconSize}
                color="white"
                onPress={() => {
                  window.open("https://www.instagram.com/lucho.982/", "_blank");
                }}
              />
            </HStack>
          </Box>
        </Box>
      </HStack>
    </Box>
  );
};

export default Team;
