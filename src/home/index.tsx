import React, { useEffect } from "react";
import { Box, Divider, VStack } from "native-base";
import Footer from "./footer";
import Top from "./top";
import LeftPanel, { main_tabs, Screen } from "./leftPanel";
import { walletStart } from "./wallet/api";

import { DEV, getFA2Storage, getSalezStorage } from "./contracts/api";
import { getAllowedCoins, getMaxPerTx } from "./mint/api";
import { useDispatch, useSelector } from "react-redux";
import { IApiResponse, set_beacon, set_tezos } from "./wallet/reducers";
import { set_fa2_storage, set_salez_storage } from "./contracts/recuders";
import { set_allowed_coins, set_max_mint_per_tx } from "./mint/reducers";
import { RootState } from "../settings/store";
import {
  MAIN_TAB,
  set_loading,
  set_main_tab,
  set_query,
} from "./leftPanel/reducers";
import Loading from "./loader";
import Cookies from "./cookies";
import Intro from "./intro";

import * as Linking from "expo-linking";

const Home = () => {
  const {
    wallet: { tezos, beacon },
  } = useSelector((state: RootState) => state);
  const dispatch = useDispatch();

  const onStart = async () => {
    try {
      if (tezos === null || beacon === null) {
        dispatch(set_loading(true));
        const { tezos, beacon } = walletStart();
        const fa2_storage = await getFA2Storage(tezos);
        const salez_storage = await getSalezStorage(tezos);
        if (salez_storage) {
          const max_mint_per_tx = getMaxPerTx(salez_storage);
          dispatch(set_max_mint_per_tx(max_mint_per_tx));
          const allowed_coins = getAllowedCoins(salez_storage);
          dispatch(set_allowed_coins(allowed_coins));
        }
        dispatch(set_fa2_storage(fa2_storage));
        dispatch(set_salez_storage(salez_storage));
        dispatch(set_tezos(tezos));
        dispatch(set_beacon(beacon));
        dispatch(set_loading(false));
      }
    } catch (error) {
      console.log(error);
      dispatch(set_loading(false));
    }
  };

  useEffect(() => {
    onStart();
    Linking.getInitialURL().then((query) => {
      if (query) {
        const fromURL = Linking.parse(query) as IApiResponse;
        dispatch(set_query(fromURL.queryParams));
        const rootPath = fromURL.path.split("/")[0];
        switch (rootPath) {
          case "holders":
            dispatch(set_main_tab(MAIN_TAB.HOLDERS));
            break;

          default:
            break;
        }
      }
    });
  }, []);
  return (
    <VStack>
      <Top />
      <LeftPanel />
      <Box
        mt={{ base: "110px", lg: "110px" }}
        justifyContent="center"
        alignItems="center"
      >
        <Screen tabs={main_tabs} />
      </Box>
      <Box my="10px">
        <Divider bg="black" />
      </Box>
      <Footer />
      <Intro />
      <Cookies />
      <Loading />
    </VStack>
  );
};

export default Home;
