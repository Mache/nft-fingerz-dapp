export const getUnixTime = async () => {
  let unixtime = 0;
  try {
    const response = await fetch(
      "https://worldtimeapi.org/api/timezone/America/Argentina/Salta"
    );
    const result = (await response.json()) as { unixtime: number };
    unixtime = result.unixtime;
    //console.log(moment.unix(result.unixtime).format());
    //console.log(result);
  } catch (error) {
    console.log(error);
  }
  return unixtime;
};
