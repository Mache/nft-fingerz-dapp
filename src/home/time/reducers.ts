import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface t_time_api {
  unix_time: number;
}
const initial_state: t_time_api = {
  unix_time: 0,
};
const time_api = createSlice({
  name: "time_api",
  initialState: initial_state,
  reducers: {
    set_unix_time: (state, action: PayloadAction<number>) => {
      state.unix_time = action.payload;
    },
    add_unix_time: (state, action: PayloadAction<number>) => {
      state.unix_time += action.payload;
    },
  },
});

export const { set_unix_time, add_unix_time } = time_api.actions;
export default time_api.reducer;
