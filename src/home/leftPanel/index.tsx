import React from "react";
import {
  Box,
  Button,
  Pressable,
  Text,
  useBreakpointValue,
  VStack,
} from "native-base";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../settings/store";

import { MaterialCommunityIcons } from "@expo/vector-icons";
import { MAIN_TAB, set_main_tab, set_open, ISiteTabs } from "./reducers";
import Landing from "../landing";
import Inbox from "../inbox";
import Rarity from "../rarity";
import Holders from "../holders";
import Tools from "../tools";

export const main_tabs: ISiteTabs = {
  [MAIN_TAB.LANDING]: {
    icon: (size) => (
      <MaterialCommunityIcons name="home" size={size} color="white" />
    ),
    label: `HOME`,
    component: Landing,
    destination: MAIN_TAB.LANDING,
  },
  [MAIN_TAB.INBOX]: {
    icon: (size) => (
      <MaterialCommunityIcons name="email-open" size={size} color="white" />
    ),
    component: Inbox,
    destination: MAIN_TAB.INBOX,
  },
  [MAIN_TAB.RARITY]: {
    icon: (size) => (
      <MaterialCommunityIcons
        name="star-box-outline"
        size={size}
        color="white"
      />
    ),
    component: Rarity,
    destination: MAIN_TAB.RARITY,
  },
  [MAIN_TAB.HOLDERS]: {
    icon: (size) => (
      <MaterialCommunityIcons name="hand-heart" size={size} color="white" />
    ),
    component: Holders,
    //label: "EARN",
    destination: MAIN_TAB.HOLDERS,
  },

  [MAIN_TAB.TOOLS]: {
    icon: (size) => (
      <MaterialCommunityIcons name="tools" size={size} color="white" />
    ),
    component: Tools,
    destination: MAIN_TAB.TOOLS,
    private: true,
  },
};
type t_props = {
  tabs: ISiteTabs;
};
const Tab = ({ tabs }: t_props) => {
  const dispatch = useDispatch();
  const {
    contract: { salez_storage },
    wallet: { address },
  } = useSelector((state: RootState) => state);
  const iconSize = useBreakpointValue({
    base: 22,
    lg: 36,
  });
  if (!address) {
    return null;
  }
  return (
    <VStack space={{ base: "5px" }}>
      <Box>
        <Button
          //justifyContent="start"
          leftIcon={
            <MaterialCommunityIcons
              name="chevron-left"
              size={iconSize}
              color="white"
            />
          }
          onPress={() => {
            dispatch(set_open(false));
          }}
        >
          <Text>CLOSE</Text>
        </Button>
      </Box>
      {Object.keys(tabs).map((name) => {
        return tabs[name].private === true ? null : (
          <Box key={name}>
            <Button
              onPress={() => {
                dispatch(set_main_tab(tabs[name].destination));
              }}
              leftIcon={tabs[name].icon(iconSize)}
            >
              <Text>{tabs[name]?.label || name}</Text>
            </Button>
          </Box>
        );
      })}
      {salez_storage?.managers.includes(address) ||
      salez_storage?.administrator === address ? (
        <Box>
          <Button
            //justifyContent="start"
            leftIcon={
              <MaterialCommunityIcons
                name="tools"
                size={iconSize}
                color="white"
              />
            }
            onPress={() => {
              dispatch(set_main_tab(MAIN_TAB.TOOLS));
            }}
          >
            <Text>TOOLS</Text>
          </Button>
        </Box>
      ) : null}
    </VStack>
  );
};
export const Screen = ({ tabs }: t_props) => {
  const { tab } = useSelector((state: RootState) => state.leftPanel);
  const Component = tabs[tab].component;
  return <Component />;
};

const LeftPanel = () => {
  const {
    leftPanel: { isOpen },
    wallet: { address },
  } = useSelector((state: RootState) => state);
  const dispatch = useDispatch();
  const iconSize = useBreakpointValue({
    base: 25,
    lg: 40,
  });
  return isOpen ? (
    <Box
      position={{ base: "fixed" }}
      left={{ base: "10px" }}
      top={{ base: "60px", lg: "110px" }}
      w={{ base: "100px", lg: "150px" }}
      alignItems={{ base: "center" }}
      justifyContent={{ base: "center" }}
      zIndex={{ base: 3 }}
    >
      <Tab tabs={main_tabs} />
    </Box>
  ) : address ? (
    <Box
      position="fixed"
      left={{ base: "20px" }}
      top={{ base: "60px", lg: "110px" }}
      h={{ base: "25px", lg: "40px" }}
      w={{ base: "25px", lg: "40px" }} //si lo pones full te tapa con una capa invisible los botones del sitio
      alignItems="center"
      zIndex="3"
      rounded="md"
    >
      <Button
        onPress={() => {
          dispatch(set_open(true));
        }}
      >
        <MaterialCommunityIcons name="menu" size={iconSize} color="white" />
      </Button>
    </Box>
  ) : null;
};
export default LeftPanel;
