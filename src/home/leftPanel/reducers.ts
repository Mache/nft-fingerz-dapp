import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { IQueryParams } from "../wallet/reducers";
export interface ISiteTab {
  icon: (size: number) => JSX.Element;
  label?: string;
  component: () => JSX.Element;
  destination: MAIN_TAB;
  private?: boolean;
}
export interface ISiteTabs {
  [tab: string]: ISiteTab;
}
export enum MAIN_TAB {
  TOOLS = "TOOLS",
  INBOX = "INBOX",
  RARITY = "RARITY",
  HOLDERS = "HOLDERS",
  LANDING = "LANDING",
}
interface ISiteLeftPanel {
  tab: MAIN_TAB;
  isOpen: boolean;
  isLoading: boolean;
  showCookies: boolean;
  query: IQueryParams;
}
const initialState: ISiteLeftPanel = {
  tab: MAIN_TAB.LANDING,
  isOpen: false,
  isLoading: false,
  showCookies: false,
  query: {},
};
const left_panel = createSlice({
  name: "left_panel",
  initialState: initialState,
  reducers: {
    set_main_tab: (state, action: PayloadAction<MAIN_TAB>) => {
      state.tab = action.payload;
      state.isOpen = false;
    },
    set_open: (state, action: PayloadAction<boolean>) => {
      state.isOpen = action.payload;
    },
    set_loading: (state, action: PayloadAction<boolean>) => {
      state.isLoading = action.payload;
      state.showCookies = !action.payload;
    },
    set_cookies: (state, action: PayloadAction<boolean>) => {
      state.showCookies = action.payload;
    },
    set_query: (state, action: PayloadAction<any>) => {
      state.query = action.payload;
    },
  },
});

export const { set_main_tab, set_open, set_loading, set_cookies, set_query } =
  left_panel.actions;
export default left_panel.reducer;
