import React from "react";
import { Box, Text } from "native-base";
type Props = {
  text: string;
};
const MySpeech = ({ text }: Props) => {
  return (
    <Box bg="gray.900" rounded="md" alignItems="center" p="5px" shadow="8">
      <Box px="5px">
        <Text>{text}</Text>
      </Box>
    </Box>
  );
};

export default MySpeech;
