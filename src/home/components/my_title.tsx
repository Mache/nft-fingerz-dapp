import React from "react";
import { Box, Heading } from "native-base";
type Props = {
  children: string;
  size?: number;
};
const MyTitle = ({ children, size = 40 }: Props) => {
  return (
    <Box textAlign="center" alignItems="center">
      <Heading color="white" fontSize={`${size}px`} zIndex="1">
        {children}
      </Heading>
      <Heading
        color="black"
        fontSize={`${size}px`}
        mt={`-${size * 1.1}px`}
        ml={`${size * 0.3}px`}
      >
        {children}
      </Heading>
    </Box>
  );
};

export default MyTitle;
