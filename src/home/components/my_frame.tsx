import React from "react";
import { Box } from "native-base";
type Props = {
  Body: React.ReactNode;
  Footer: React.ReactNode;
};
const MyFrame = ({ Body, Footer }: Props) => {
  return (
    <Box maxW="600" bg="black" rounded="2xl" p="10px" mx="20px" shadow="8">
      <Box
        //alignItems="center"
        bg={{
          linearGradient: {
            colors: ["fingerz.0", "fingerz.200"],
            start: [0, 0],
            end: [0, 1],
          },
        }}
        rounded="2xl"
        //m="30px"
        py="20px"
      >
        {Body}
      </Box>
      <Box bg="gray.900" rounded="2xl" marginTop="10px" p="10px">
        {Footer}
      </Box>
    </Box>
  );
};

export default MyFrame;
