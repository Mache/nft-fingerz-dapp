import React from "react";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { useBreakpointValue } from "native-base";
interface IIconProps {
  name: any;
  size?: { base: number; lg: number };
  color?: string;
}

const Icon = ({
  name,
  size = { base: 24, lg: 36 },
  color = "black",
}: IIconProps) => {
  const responsiveSize = useBreakpointValue(size);
  return (
    <MaterialCommunityIcons name={name} size={responsiveSize} color={color} />
  );
};

export default Icon;
