import React, { useState } from "react";
import { Box, Button, Heading, HStack, Text, Image } from "native-base";
import { Entypo } from "@expo/vector-icons";
type Props = {
  data: {
    image: string;
    heading: string;
    text: string;
  }[];
};
const MyCycling = ({ data }: Props) => {
  const [index, changeIndex] = useState(0);
  return (
    <Box
      w="800px"
      minH="350px"
      bg="gray.900"
      rounded="2xl"
      alignItems="center"
      p="10px"
      shadow="8"
    >
      <HStack w="full" h="90%" bg="gray.800" rounded="2xl">
        <Box w="20px" h="full" rounded="3xl" marginRight="-30px" zIndex="1">
          <Button
            bg="none"
            variant="ghost"
            w="20px"
            h="full"
            rounded="3xl"
            shadow="none"
            onPress={() => {
              changeIndex((index) => {
                if (index > 0) {
                  return --index;
                } else {
                  return data.length - 1;
                }
              });
            }}
          >
            <Entypo name="chevron-left" size={24} color="black" />
          </Button>
        </Box>

        <HStack w="full">
          <Box
            flex="1"
            alignItems="center"
            p="10px"
            justifyContent="center"
            alignContent="center"
          >
            <Image
              alt="Section"
              src={data[index].image}
              w="200px"
              h="200px"
              rounded="2xl"
              resizeMode="center"
              //borderColor="black"
              //borderWidth="5px"
            />
          </Box>

          <Box flex="1" p="30px">
            <Heading>{data[index].heading}</Heading>
            <Text>{data[index].text}</Text>
          </Box>
        </HStack>
        <Box w="20px" h="full" marginLeft="-20px" zIndex="1">
          <Button
            bg="none"
            variant="ghost"
            w="20px"
            h="full"
            rounded="3xl"
            shadow="none"
            onPress={() => {
              changeIndex((index) => {
                if (index < data.length - 1) {
                  return ++index;
                } else {
                  return 0;
                }
              });
            }}
          >
            <Entypo name="chevron-right" size={24} color="black" />
          </Button>
        </Box>
      </HStack>

      <HStack alignItems="center" h="10%">
        {data.map((item, i) => {
          return (
            <Entypo
              key={i}
              name="dot-single"
              size={50}
              color={index == i ? "white" : "black"}
            />
          );
        })}
      </HStack>
    </Box>
  );
};

export default MyCycling;
