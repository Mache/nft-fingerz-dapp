import React from "react";
import { Text, Box, useBreakpointValue } from "native-base";
import { MaterialCommunityIcons } from "@expo/vector-icons";
type GLYPHS = string;
type Props = {
  icon: React.ReactNode;
  description: string;
};
const NewBadge = ({ icon, description }: Props) => {
  const containerW = useBreakpointValue({
    base: "160px",
    lg: "200px",
  });
  const containerH = useBreakpointValue({
    base: "40px",
    lg: "50px",
  });
  const fontSize = useBreakpointValue({
    base: "12px",
    lg: "16px",
  });
  const iconSize = useBreakpointValue({
    base: 18,
    lg: 30,
  });
  return (
    <Box
      m="5px"
      //justifyContent="center"
      alignItems="center"
      w={containerW}
      h={containerH}
      rounded="md"
      /*bg={{
        linearGradient: {
          colors: ["fingerz.0", "fingerz.200"],
          start: [1, 0],
          end: [0, 1],
        },
      }}*/
      bg="gray.900"
      flexDirection="row"
      borderColor="black"
      borderWidth="3px"
      borderRadius="md"
    >
      <Box mx="5px">{icon}</Box>
      <Box w="60%">
        <Text textAlign="center" fontSize={fontSize} lineHeight="xs">
          {description}
        </Text>
      </Box>
      <Box mx="5px">
        <MaterialCommunityIcons
          name="checkbox-marked-outline"
          size={iconSize}
          color="white"
        />
      </Box>
    </Box>
  );
};

export default NewBadge;
