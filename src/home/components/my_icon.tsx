import React from "react";
import { Box, Heading } from "native-base";
type Props = {
  children: React.ReactElement;
  size?: number;
};
const MyIcon = ({ children, size = 20 }: Props) => {
  return (
    <Box>
      <Box zIndex="1">{React.cloneElement(children, { size })}</Box>
      <Box mt={`-${size * 0.95}px`} ml={`${size * 0.05}px`}>
        {React.cloneElement(children, { color: "black", size })}
      </Box>
    </Box>
  );
};

export default MyIcon;
