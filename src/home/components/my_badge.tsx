import React from "react";
import { Text, Box, Heading } from "native-base";
import { AntDesign } from "@expo/vector-icons";
import MyTitle from "./my_title";

interface IProps {
  icon: React.ReactElement;
  text: string;
}
const MyBadge = ({ icon, text }: IProps) => {
  return (
    <Box justifyContent="center" alignItems="center" w="180px">
      <Box
        rounded="full"
        /*bg={{
          linearGradient: {
            colors: ["fingerz.0", "fingerz.200"],
            start: [0, 0],
            end: [0, 1],
          },
        }}*/
        bg="gray.900"
        size="80px"
        alignItems="center"
        justifyContent="center"
        textAlign="center"
        shadow="8"
      >
        <Box
          rounded="full"
          borderColor="gray.800"
          borderWidth="5px"
          size="70px"
          alignItems="center"
          justifyContent="center"
          textAlign="center"
          p="5px"
        >
          {React.cloneElement(icon, { size: 40, color: "white" })}
        </Box>
      </Box>
      <Box flexDirection="row" justifyContent="center">
        {[1, 2, 3, 4, 5].map((i) => {
          return (
            <Box key={i}>
              <Box zIndex="1">
                <AntDesign name="star" size={12} color="white" />
              </Box>
              <Box mt="-12px" ml="2px">
                <AntDesign name="star" size={12} color="black" />
              </Box>
            </Box>
          );
        })}
      </Box>
      <MyTitle size={20}>{text}</MyTitle>
    </Box>
  );
};

export default MyBadge;
