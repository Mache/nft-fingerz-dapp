import { Box } from "native-base";
import React from "react";
import { useWindowDimensions } from "react-native";
type t_modal_props = {
  children: React.ReactNode;
  is_open: boolean;
  zIndex?: number;
};
const MyModal = ({ children, is_open, zIndex = 10 }: t_modal_props) => {
  const { width, height } = useWindowDimensions();
  if (!is_open) {
    return null;
  }
  return (
    <Box
      position={{ base: "fixed" }}
      top={{ base: "0px" }}
      left={{ base: "0px" }}
      w={{ base: width }}
      h={{ base: height }}
      justifyContent={{ base: "center" }}
      alignItems={{ base: "center" }}
      zIndex={{ base: zIndex }}
    >
      <Box
        position={{ base: "fixed" }}
        top={{ base: "0px" }}
        left={{ base: "0px" }}
        bg={{ base: "black" }}
        opacity={{ base: 0.7 }}
        w={{ base: "100%" }}
        h={{ base: "100%" }}
      />
      {/* <Box
        w={{ base: "350px", lg: "700px" }}
        h={{ base: "200px", lg: "400px" }}
        //zIndex={{ base: 11 }}
        bg={{ base: "black" }}
        justifyContent={{ base: "center" }}
        alignItems={{ base: "center" }}
        rounded={{ base: "md", lg: "lg" }}
      >
        {children}
  </Box>*/}
      {children}
    </Box>
  );
};

export default MyModal;
