import { Box } from "native-base";
import React from "react";
import { useWindowDimensions } from "react-native";
type t_modal_props = {
  children: React.ReactNode;
  is_open: boolean;
};
const MyNotification = ({ children, is_open }: t_modal_props) => {
  const { width, height } = useWindowDimensions();
  if (!is_open) {
    return null;
  }
  return (
    <Box
      position={{ base: "fixed" }}
      bottom={{ base: "0px" }}
      left={{ base: "0px" }}
      w={{ base: width }}
      h={{ base: "200px" }}
      //opacity={{ base: 0.7 }}
      justifyContent={{ base: "center" }}
      alignItems={{ base: "center" }}
      zIndex={{ base: 10 }}
      //p={{ base: "10px" }}
    >
      <Box
        bg={{ base: "black" }}
        rounded={{ base: "md", lg: "xl" }}
        w={{ base: "90%" }}
        h={{ base: "90%" }}
        justifyContent={{ base: "center" }}
        alignItems={{ base: "center" }}
      >
        {children}
      </Box>
    </Box>
  );
};

export default MyNotification;
