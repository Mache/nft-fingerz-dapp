import React from "react";
import { Heading, IHeadingProps } from "native-base";
interface IProps extends IHeadingProps {
  children: string; //React.ReactNode;
}
const BHeading = ({ children, ...rest }: IProps) => {
  return (
    <Heading color={{ base: "black" }} textAlign={{ base: "center" }} {...rest}>
      {children}
    </Heading>
  );
};

export default BHeading;
