import React, { useEffect, useState } from "react";
import { Box, Center, HStack, Image, useBreakpointValue } from "native-base";
interface IProps {
  data: string[];
}
const MyAutoCycling = ({ data }: IProps) => {
  const [index, changeIndex] = useState(0);

  useEffect(() => {
    const cycling_id = setInterval(() => {
      changeIndex((index) => {
        if (index < data.length - 1) {
          return ++index;
        } else {
          return 0;
        }
      });
    }, 5000);
    return () => clearInterval(cycling_id);
  }, []);

  return (
    <Center>
      <Box
        minW={{
          base: "300px",
          lg: "750px",
        }}
        //w="full"
        bg="black"
        rounded="2xl"
        p={{ base: "2px", lg: "5px" }}
        shadow="8"
      >
        <Image
          alt="Banner"
          src={data[index]}
          w={{
            base: "300px",
            lg: "750px",
          }}
          h={{
            base: "100px",
            lg: "250px",
          }}
          rounded="2xl"
          resizeMode="center"
        />
        <Image
          alt="Pattern"
          src={require("../../../assets/pattern.png")}
          w={{
            base: "300px",
            lg: "750px",
          }}
          h={{
            base: "100px",
            lg: "250px",
          }}
          rounded="2xl"
          resizeMode="center"
          mt={{
            base: "-100px",
            lg: "-250px",
          }}
          zIndex="1"
        />
      </Box>
    </Center>
  );
};

export default MyAutoCycling;
