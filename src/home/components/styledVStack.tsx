import React from "react";
import { VStack } from "native-base";
interface IProps {
  children: React.ReactNode;
}
const StyledVStack = ({ children }: IProps) => {
  return (
    <VStack
      bg={{
        linearGradient: {
          colors: ["fingerz.0", "fingerz.200"],
          start: [0, 0],
          end: [0, 1],
        },
      }}
      rounded="md"
      w="100%"
      p="10px"
      my="10px"
      borderColor={{ base: "black" }}
      borderWidth={{ base: "4px" }}
      shadow={{ base: 4 }}
    >
      {children}
    </VStack>
  );
};

export default StyledVStack;
