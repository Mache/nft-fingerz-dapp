import React from "react";
import { Box, Center, IBoxProps } from "native-base";

interface IProps {
  children: React.ReactNode;
}
const style_outter: IBoxProps = {
  bg: "white",
  w: "full",
  px: "30px",
};
const style_middle: IBoxProps = {
  bg: "yellow.100",
  w: "full",
  px: "30px",
  shadow: 8,
};
const style_inner: IBoxProps = {
  bg: "yellow.200",
  w: "full",
  px: "30px",
  shadow: 8,
};
const style_center: IBoxProps = {
  bg: "yellow.300",
  w: "full",
  px: "30px",
  shadow: 8,
  //justifyContent: "center",
  alignItems: "center",
};
const MyBg = ({ children }: IProps) => {
  return (
    <Center {...style_outter}>
      <Box {...style_middle}>
        <Box {...style_inner}>
          <Box {...style_center}>{children}</Box>
        </Box>
      </Box>
    </Center>
  );
};

export default MyBg;
