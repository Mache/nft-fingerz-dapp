import React from "react";
import { Text } from "native-base";
interface IProps {
  children: string; //React.ReactNode;
}
const BText = ({ children }: IProps) => {
  return <Text color={{ base: "black" }}>{children}</Text>;
};

export default BText;
