import React from "react";
import { HStack, Box, Text } from "native-base";
import Icon from "./icon";
import { PROFILE_PROVIDER, IProfile } from "../wallet/reducers";
interface IMiniProfile {
  profile: null | IProfile;
}
const MiniProfile = ({ profile }: IMiniProfile) => {
  return profile ? (
    <HStack>
      {profile.discord && (
        <Box
          flexDirection={{ base: "row" }}
          justifyContent={{ base: "center" }}
          alignItems={{ base: "center" }}
        >
          <Icon name="discord" />
          <Text>{`${profile.discord.username}#${profile.discord.discriminator}`}</Text>
        </Box>
      )}
      {profile.wallet && profile.wallet[PROFILE_PROVIDER.TZ_PROFILES] && (
        <Box
          flexDirection={{ base: "row" }}
          justifyContent={{ base: "center" }}
          alignItems={{ base: "center" }}
        >
          <Icon name="comment-account-outline" />
          <Text>{`[TZP] ${
            profile.wallet[PROFILE_PROVIDER.TZ_PROFILES].alias
          }`}</Text>
        </Box>
      )}
      {profile.wallet && profile.wallet[PROFILE_PROVIDER.TZ_DOMAINS] && (
        <Box
          flexDirection={{ base: "row" }}
          justifyContent={{ base: "center" }}
          alignItems={{ base: "center" }}
        >
          <Icon name="comment-account-outline" />
          <Text>{`[TZD] ${
            profile.wallet[PROFILE_PROVIDER.TZ_DOMAINS].alias
          }`}</Text>
        </Box>
      )}
    </HStack>
  ) : null;
};

export default MiniProfile;
