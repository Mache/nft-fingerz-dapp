import React from "react";
import {
  Box,
  Center,
  Heading,
  HStack,
  Image,
  Text,
  useBreakpointValue,
  VStack,
} from "native-base";
import MyTitle from "./components/my_title";

const Lore = () => {
  const containerW = useBreakpointValue({
    base: "300px",
    lg: "750px",
  });
  const imageSize = useBreakpointValue({
    base: "90px",
    lg: "150px",
  });

  return (
    <Box w="100%" justifyContent="center" alignItems="center" mt="10px">
      <Box w={containerW}>
        <HStack w="100%" alignItems="center">
          <Box mx="10px">
            <Image
              alt="Lore"
              src={require("../../assets/lore.png")}
              width={imageSize}
              height={imageSize}
              rounded="full"
              resizeMode="center"
              borderColor="black"
              borderWidth="5px"
              zIndex="1"
            />
          </Box>
          <Box w="60%">
            <Heading textAlign="center" fontSize={{ base: "2xl", lg: "6xl" }}>
              WHAT'S FINGERZ
            </Heading>
          </Box>
        </HStack>
        <Box w="100%" bg="gray.900" rounded="md" p="15px" mt="5px">
          <Text>
            Look at this guy, he's a fingerz. You may be wondering what a
            Fingerz is… {`\n`}They are special creatures whose experience is
            motivated by the good common, living in the utopia of cooperation
            and power decentralization. {`\n`}Regardless of their personal style
            or where they come from, they consider themselves as mutuals. {`\n`}
            There's no evidence of evil in their world, that’s why their shape
            reflects the simplicity of good intentions and the power of
            friendship.
          </Text>
        </Box>
      </Box>
    </Box>
  );
};

export default Lore;
