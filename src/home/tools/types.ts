import { IAddress } from "../contracts/types";
import { IQuestBase } from "../quest/types";
import { IProfile } from "../wallet/reducers";

export enum t_mimeType {
  PNG = "image/png",
  MP4 = "video/mp4",
}
export type t_attribute = { name: string; value: string };
export type t_attributes = t_attribute[];
export enum t_unit {
  PX = "px",
}
export interface t_formats {
  uri: string;
  mimeType: t_mimeType;
  dimensions: {
    value: string;
    unit: t_unit;
  };
}
export interface ITokenMetadata {
  id: number;
  name: string;
  description: string;
  decimals: number;
  isBooleanAmount: boolean;
  shouldPreferSymbol: boolean;
  creators: string[];
  artifactUri: string;
  displayUri: string;
  thumbnailUri: string;
  attributes: t_attributes;
  indexedHash: string;
  artifactHash: string;
  formats: t_formats[];
  royalties: {
    decimals: number;
    shares: {
      [key: string]: string;
    };
  };
}

export const MAX_CHARACTERS_PER_TWEET = 280;

export type ITwitterMeta = {
  text: string;
  cooldown: number;
};

export interface IHolders {
  [address: IAddress]: IQuestBase;
}
export interface IDistributeArgs {
  holders: IHolders;
  quest: string;
}

export interface IQuestExt extends IQuestBase {
  address: IAddress;
}
export interface IQuestList {
  [quest: string]: IQuestExt[];
}
export interface IQuestItems {
  name: string;
  quests: IQuestExt[];
}
