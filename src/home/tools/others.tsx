import React, { useState } from "react";
import {
  Box,
  Button,
  Center,
  Heading,
  Input,
  InputGroup,
  InputLeftAddon,
  Stack,
  TextArea,
  Text,
  VStack,
} from "native-base";
import { setWanted, set_white_list, t_fingerz } from "./d_api";
import { useSelector } from "react-redux";
import { RootState } from "../../settings/store";
import moment from "moment";

const Others = () => {
  const {
    wallet: { tezos },
  } = useSelector((state: RootState) => state);
  const [whitelist, setWhitelist] = useState("");
  const [wanted, setWantedJSON] = useState("");
  const [bonuz, setBonuzJSON] = useState("");
  const [expire, setExpire] = useState(moment().add(7, "days").format());
  const [reward, setReward] = useState("");
  const [fetching, setFetching] = useState(false);
  return (
    <VStack justifyContent={{ base: "center" }} space={{ base: "10px" }}>
      <Heading fontSize={{ base: "2xl" }}>WHITELIST</Heading>
      <Box alignItems="center" w="100%" mb={{ base: "10px" }}>
        <TextArea
          rounded="md"
          value={whitelist}
          onChangeText={(text) => {
            setWhitelist(text);
          }}
          w="100%"
          placeholder="WALLET ADDRESS PER LINE"
          //maxW="300"
        />
      </Box>
      <VStack space="5px">
        <Box>
          <Button
            onPress={async () => {
              if (tezos) {
                const list = whitelist
                  .split("\n")
                  .map((address) => address.trim());
                console.log(list);
                set_white_list(tezos, list);
              }
            }}
          >
            <Text>SET WHITELIST</Text>
          </Button>
        </Box>
        <Heading fontSize={{ base: "2xl" }} mt={{ base: "10px" }}>
          SET MISSION
        </Heading>
        <Box>
          <Stack alignItems="center">
            <InputGroup w="100%">
              <InputLeftAddon w={{ base: "25%" }} children={"WANTED:"} />
              <Input
                value={wanted}
                onChangeText={(text) => setWantedJSON(text.trim())}
                w="75%"
                placeholder="JSON"
              />
            </InputGroup>
          </Stack>
        </Box>
        <Box>
          <Stack alignItems="center">
            <InputGroup w="100%">
              <InputLeftAddon w={{ base: "25%" }} children={"BONUZ:"} />
              <Input
                value={bonuz}
                onChangeText={(text) => setBonuzJSON(text.trim())}
                w="75%"
                placeholder="JSON"
              />
            </InputGroup>
          </Stack>
        </Box>

        <Box>
          <Stack alignItems="center">
            <InputGroup w="100%">
              <InputLeftAddon w={{ base: "25%" }} children={"EXPIRE:"} />
              <Input
                value={expire}
                onChangeText={(text) => setExpire(text.trim())}
                w="75%"
                placeholder="UNIX TIME"
              />
            </InputGroup>
          </Stack>
        </Box>
        <Box>
          <Stack alignItems="center">
            <InputGroup w="100%">
              <InputLeftAddon w={{ base: "25%" }} children={"REWARD:"} />
              <Input
                value={reward}
                onChangeText={(text) => setReward(text.trim())}
                w="75%"
                placeholder="REWARD / 1000"
              />
            </InputGroup>
          </Stack>
        </Box>

        <Box>
          <Button
            isLoading={fetching}
            onPress={async () => {
              setFetching(true);
              try {
                if (tezos) {
                  const compose_wanted = JSON.parse(wanted) as t_fingerz[];
                  const compose_bonuz =
                    bonuz !== "" ? (JSON.parse(bonuz) as t_fingerz[]) : [];
                  const expire_on = moment(expire).unix(); //Number.parseInt(expire, 10);
                  const reward_ratio = Number.parseInt(reward, 10);
                  const add_reward = {
                    wanted: compose_wanted,
                    bonuz: compose_bonuz,
                    expire_on,
                    reward_ratio,
                    metadata: "",
                  };
                  console.log(add_reward);
                  await setWanted(tezos, [add_reward]);
                }
              } catch (error) {
                console.log(error);
              }
              setFetching(false);
            }}
          >
            <Text>SET WANTED</Text>
          </Button>
        </Box>
      </VStack>
    </VStack>
  );
};

export default Others;
