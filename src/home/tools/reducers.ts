import { createSlice, PayloadAction } from "@reduxjs/toolkit";

enum ITabs {
  QUEST = "QUEST",
}
interface IToolsApi {
  tab: ITabs;
}
const initial_state: IToolsApi = {
  tab: ITabs.QUEST,
};
const tools_api = createSlice({
  name: "tools_api",
  initialState: initial_state,
  reducers: {
    set_tools_tab: (state, action: PayloadAction<ITabs>) => {
      state.tab = action.payload;
    },
  },
});

export const { set_tools_tab } = tools_api.actions;
export default tools_api.reducer;
