import { Box, Button, HStack, Text, VStack } from "native-base";
import React, { useEffect, useState } from "react";
import ItemQuest from "./questList";
import { IQuestList } from "../types";
import { questStatus } from "../d_api";

const Quests = () => {
  const [currentTab, setCurrentTab] = useState<null | string>(null);
  const [questList, setQuestList] = useState<IQuestList>({});
  const [fetching, setFetching] = useState(false);

  return (
    <VStack justifyContent={{ base: "center" }} space={{ base: "10px" }}>
      <Box my={{ base: "5px" }}>
        <Button
          isLoading={fetching}
          disabled={fetching}
          onPress={async () => {
            try {
              setFetching(true);
              questStatus((query_quest_holders) => {
                setQuestList(query_quest_holders);
                setFetching(false);
              });
            } catch (error) {
              console.log(error);
            }
          }}
        >
          <Text>GET COMPLETED QUEST LIST</Text>
        </Button>
      </Box>
      <HStack justifyContent={{ base: "center" }} space={{ base: "10px" }}>
        {Object.keys(questList).map((tab) => {
          return (
            <Box key={tab}>
              <Button
                onPress={() => {
                  setCurrentTab(tab);
                }}
              >
                <Text> {tab.toUpperCase()}</Text>
              </Button>
            </Box>
          );
        })}
      </HStack>
      <Box>
        {currentTab !== null && (
          <ItemQuest name={currentTab} quests={questList[currentTab]} />
        )}
      </Box>
    </VStack>
  );
};

export default Quests;
