import React, { useEffect, useState } from "react";
import { Box, Button, Text } from "native-base";
import BHeading from "../../components/blackHeading";
import MiniProfile from "../../components/miniProfile";
import StyledVStack from "../../components/styledVStack";
import { rewardSelectedQuests } from "../d_api";
import { useSelector } from "react-redux";
import { RootState } from "../../../settings/store";
import { IHolders, IQuestItems } from "../types";

const QuestList = ({ name, quests }: IQuestItems) => {
  const {
    wallet: { tezos },
  } = useSelector((state: RootState) => state);
  const [selectedHolders, selectHolder] = useState<IHolders>({});
  const [fetching, setFetching] = useState(false);
  useEffect(() => {
    selectHolder({});
  }, [name]);
  return (
    <StyledVStack>
      <Box>
        <BHeading>{`[Q] ${name.toUpperCase().replace("_", " ")}`}</BHeading>
      </Box>
      <Box>
        <Button
          onPress={() => {
            selectHolder((state) => {
              if (Object.keys(state).length > 0) {
                return {};
              } else {
                const holders: IHolders = quests.reduce(
                  (prev, { address, ...quest }) => {
                    return quest.timesCompleted > quest.timesRewarded
                      ? { ...prev, [address]: quest }
                      : prev;
                  },
                  {}
                );
                return holders;
              }
            });
          }}
        >
          <Text> TOGGLE ALL</Text>
        </Button>
      </Box>
      <Box>
        <Text>{`SELECTED USERS: ${Object.keys(selectedHolders).length}`}</Text>
      </Box>
      <Box my={{ base: "10px" }}>
        <Button
          h={{ base: "40px" }}
          isLoading={fetching}
          disabled={fetching}
          onPress={async () => {
            try {
              if (tezos && Object.keys(selectedHolders).length > 0) {
                setFetching(true);
                await rewardSelectedQuests(tezos, selectedHolders, name);
                selectHolder({});
                setFetching(false);
              }
            } catch (error) {
              console.log(error);
            }
          }}
        >
          <Text>{`DISTRIBUTE REWARD ${name
            .toUpperCase()
            .replace("_", " ")}`}</Text>
        </Button>
      </Box>
      {quests.map((quest) => {
        return (
          <Box
            key={quest.address}
            bg={{ base: "gray.900" }}
            p={{ base: "10px" }}
            rounded={{ base: "md" }}
            my={{ base: "5px" }}
            flexDirection={{ base: "column", lg: "row" }}
            justifyContent={{ base: "center" }}
            alignItems={{ base: "center" }}
          >
            <Box w={{ lg: "60%" }}>
              <Text>{`WALLET: ${quest.address}`}</Text>
              <MiniProfile profile={quest.profile} />
              <Text>{`TIMES COMPLETED: ${quest.timesCompleted}`}</Text>
              <Text>{`TIMES REWARDED: ${quest.timesRewarded}`}</Text>
              <Text>{`REWARD ID#${quest.rewardID}`}</Text>
              <Text>{`REWARD NAME ${quest.rewardName}`}</Text>
              <Text>{`REWARD AMOUNT x${quest.rewardAmount}`}</Text>
              <Text>{`OP HASH ${
                quest.opHash.length > 0 ? quest.opHash.slice(-3).join("\n") : ""
              }`}</Text>
            </Box>
            <Box w={{ base: "100%", lg: "90px" }} h={{ lg: "90px" }}>
              {quest.timesCompleted > quest.timesRewarded && (
                <Button
                  w={{ base: "100%" }}
                  h={{ lg: "100%" }}
                  isPressed={
                    selectedHolders[quest.address] !== undefined ? true : false
                  }
                  onPress={() => {
                    selectHolder((state) => {
                      const query_quests = { ...state };
                      if (query_quests[quest.address] !== undefined) {
                        delete query_quests[quest.address];
                      } else {
                        query_quests[quest.address] = quest;
                      }
                      return {
                        ...query_quests,
                      };
                    });
                  }}
                >
                  <Text>
                    {selectedHolders[quest.address] !== undefined
                      ? "SELECTED"
                      : "SELECT"}
                  </Text>
                </Button>
              )}
            </Box>
          </Box>
        );
      })}
    </StyledVStack>
  );
};
export default QuestList;
