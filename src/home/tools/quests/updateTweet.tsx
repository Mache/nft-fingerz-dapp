import {
  Box,
  Button,
  Input,
  InputGroup,
  InputLeftAddon,
  Stack,
  Text,
  TextArea,
} from "native-base";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { set_twitter_meta } from "../../quest/reducers";
import { getTwitterMeta } from "../../quest/twitter/api";
import { setQuest } from "../d_api";
import { MAX_CHARACTERS_PER_TWEET } from "../types";

const UpdateTweet = () => {
  const [tweet, setTweet] = useState("");
  const [cooldown, setCooldown] = useState("604800");
  const [fetching, setFetching] = useState(false);
  const dispatch = useDispatch();
  useEffect(() => {
    setFetching(true);
    getTwitterMeta().then((meta) => {
      setFetching(false);
      if (meta) {
        dispatch(set_twitter_meta(meta));
        setTweet(meta.text);
        setCooldown(meta.cooldown.toString());
      }
    });
  }, []);
  return (
    <Box>
      <Text>TWEET TEXT</Text>
      <Box w={{ base: "100%" }}>
        <TextArea
          rounded="md"
          value={tweet}
          onChangeText={(text) => {
            if (tweet.length < MAX_CHARACTERS_PER_TWEET) {
              setTweet(text.trim());
            }
          }}
          w="100%"
          //maxW="300"
        />
      </Box>
      <Box>
        <Text>{`CHARACTERS: ${tweet.length}/${MAX_CHARACTERS_PER_TWEET}`}</Text>
      </Box>
      <Box>
        <Stack alignItems="center">
          <InputGroup w="100%">
            <InputLeftAddon children={"COOLDOWN IN SECONDS:"} />
            <Input
              value={cooldown}
              onChangeText={(text) => {
                if (Number.isInteger(text)) {
                  setCooldown(text);
                }
              }}
              w="100%"
              placeholder="cooldown"
            />
          </InputGroup>
        </Stack>
      </Box>
      <Box>
        <Button
          disabled={fetching}
          isLoading={fetching}
          onPress={async () => {
            if (tweet.length > 0) {
              try {
                setFetching(true);
                await setQuest({
                  text: tweet,
                  cooldown: Number.parseInt(cooldown, 10),
                });
              } catch (error) {
                console.log(error);
              }
            }

            setFetching(false);
          }}
        >
          <Text>SET TWEET QUEST</Text>
        </Button>
      </Box>
    </Box>
  );
};

export default UpdateTweet;
