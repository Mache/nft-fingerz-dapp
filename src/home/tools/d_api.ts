import {
  ContractAbstraction,
  OpKind,
  TezosToolkit,
  Wallet,
  WalletParamsWithKind,
} from "@taquito/taquito";

import axios from "axios";
import { get, onValue, ref, set, update } from "firebase/database";
import { getFunctions, httpsCallable } from "firebase/functions";
import { database } from "../../../App";
import {
  FA2_CONTRACT_ADDRESS,
  RPC_NODE,
  SALE_CONTRACT_ADDRESS,
} from "../contracts/api";
import {
  IAddress,
  IContractFA2Storage,
  IContractSalezStorage,
  IContractTokenMetadata,
} from "../contracts/types";
import { IQuestBase, questBase } from "../quest/types";
import {
  IDistributeArgs,
  IHolders,
  IQuestList,
  ITwitterMeta,
  ITokenMetadata,
} from "./types";
const { packDataBytes, unpackDataBytes } = require("@taquito/michel-codec");

export const unpackData = <T>(bytes: string, type: { prim: string }): T => {
  return unpackDataBytes({ bytes }, type);
};

export function packData(
  value: { [key: string]: any },
  type: { prim: string }
) {
  return packDataBytes(value, type).bytes;
}
//https://tezos.stackexchange.com/questions/4353/unpacking-return-unexpected-result
//Big thanks to https://tezos.stackexchange.com/users/318/rodrigo-quelhas
// Packing
packData({ int: 200 }, { prim: "nat" }); // 05008803

// Unpacking
unpackData("05008803", { prim: "nat" }); // {"int":"200"}
export type _error = { data: { with: { string: string } }[] };

export const get_entries = async (tezos: TezosToolkit) => {
  try {
    console.log("Getting contract entries");
    //const contract = await tezos.wallet.at(SALE_CONTRACT_ADDRESS);

    console.log("Waiting operation finish");
    //const result = await contract.storage<t_sale_contract_storage>();

    const contract = await tezos.contract.at(SALE_CONTRACT_ADDRESS);
    const my_entries = contract.parameterSchema.ExtractSignatures();
    console.log("Operation finished");
    console.log(my_entries);
    console.log(contract.entrypoints);
  } catch (error) {
    console.log(error);
    let my_error: _error = error as _error;
    console.log(my_error.data[1]?.with.string);
    return null;
  }
};

export const set_salez = async (tezos: TezosToolkit) => {
  try {
    console.log("Getting contract entries");
    console.log("Waiting operation finish");
    const contract = await tezos.wallet.at(FA2_CONTRACT_ADDRESS);
    const operation = await tezos.wallet.batch([
      {
        kind: OpKind.TRANSACTION,
        ...contract.methods
          .set_salez_contract("KT1SHzkNbXoH1oW5YdKEeEsYSt6B5PSK7SmD")
          .toTransferParams(),
      },
    ]);
    const batch_response = await operation.send();
    console.log(batch_response);
    batch_response.confirmation();
    console.log("Operation finished");
    return null;
  } catch (error) {
    console.log(error);
    let my_error: _error = error as _error;
    console.log(my_error.data[1]?.with.string);
    return null;
  }
};
export const send_founds = async (tezos: TezosToolkit) => {
  try {
    console.log("Getting contract entries");
    console.log("Waiting operation finish");

    const wallets: WalletParamsWithKind[] = [
      "tz1ggkUZ9nQYgkXGQVAPFWUyvhbCHnfRrVNn",
      "tz1gEEMESBfMQFrPLmGhkGaxSRe7Mxo1ousA",
      "tz1PP1gBnK7iFrF9747iHbCYsuJXnzmahQpJ",
    ].map((wallet) => ({
      kind: OpKind.TRANSACTION,
      amount: 100,
      mutez: false,
      to: wallet,
    }));
    const operation = await tezos.wallet.batch(wallets);
    const batch_response = await operation.send();
    console.log(batch_response);
    batch_response.confirmation();
    console.log("Operation finished");
    return null;
  } catch (error) {
    console.log(error);
    let my_error: _error = error as _error;
    console.log(my_error.data[1]?.with.string);
    return null;
  }
};
export const batch_transaction = async (
  tezos: TezosToolkit,
  CONTRACT_ADDRESS: string,
  callback: (
    contract: ContractAbstraction<Wallet>,
    kind: typeof OpKind
  ) => WalletParamsWithKind[]
) => {
  try {
    console.log("Getting contract entries");
    console.log("Waiting operation finish");
    const contract = await tezos.wallet.at(CONTRACT_ADDRESS);
    const operation = await tezos.wallet.batch(callback(contract, OpKind));
    const batch_response = await operation.send();
    console.log(batch_response);
    batch_response.confirmation();
    console.log("Operation finished");
    return null;
  } catch (error) {
    console.log(error);
    let my_error: _error = error as _error;
    console.log(my_error.data[1]?.with.string);
    return null;
  }
};

export const set_hidden_folder_hash = (tezos: TezosToolkit) =>
  batch_transaction(tezos, SALE_CONTRACT_ADDRESS, (contract, kind) => [
    {
      kind: kind.TRANSACTION,
      ...contract.methods
        .set_hidden_folder_hash(
          "bafybeid6f6fvujvtahv7xr7ebaon3jjtfvhgil3ldn67z6zejdxnftoftq"
        )
        .toTransferParams(),
    },
  ]);

export const set_white_list = async (
  tezos: TezosToolkit,
  white_list: string[]
) => {
  try {
    console.log("Executing...");
    const contract = await tezos.wallet.at(SALE_CONTRACT_ADDRESS);

    /*const white_list: string[] = [
      "tz1f6DJvWbHtYPqMmepeMzsbkzjmutj6fYFL",
      "tz1RVDaPWGzPFM9dxRSmZ8rtayjD4TWBCnh9",
      "tz1PP1gBnK7iFrF9747iHbCYsuJXnzmahQpJ",
      "tz2RBTGwEeHtkAdy3PK6Nmh3sXPDBCswH6cd",
      "tz1c9BiA1vN6davAYJbwn89714Bf4LEQHnLz",
      "tz1ZLhhgpy7DL57Us49GbToTEiXCk2EbYhyF",
      "tz1gE4fdnqAhgDUhETAHWmNVg41yv3MozMHF",
      "tz1S1pBBLFjev8XLVj3KcCbn8vygE5cRA3rw",
      "tz1RL4NfUYLnswA3cvhsroVgPJtUq6Nk4cak",
      "tz1LgP5DCcgBZ6AaHQw5iwDynCi21G9MKXxy",
      "tz1Kj6qdWxrbBSXYw1daXGVSGD6GLjPfhJQ2",
      "tz1VPAbBxuonXbM4UpVDSRd9Q5mCTgjiGsen",
      "tz1T1LYcbrQ39Zd4PUKjKDm4WSVri2AZvar6",
      "tz1hLZ3ES9ZzvP2bBWV2z4ZB2z4GxFrb6kuL",
      "tz2LYELYe16viwVBaRjZc8GsauKRsxTGPvB1",
      "tz1Tyzw7utK9VHt5DcWGNSMBydJDBZvA8JeA",
      "tz1dKvh9via8SaiLXkutjGZsmdoKB3DBp19A",
      "tz2CcTzrLDhqFK1Bqt9iiVSPGDezvbnXntpe",
      "tz1drG6SyFWUVvDGar1VCWJdTEEx63QetxCt",
      "tz1cATgZf5oodR6vqafZhuYP3ymRChimkdWM",
      "tz1cs7z3fvsprtG8UtrT3hbRcwyHiW1SAcCJ",
      "tz2DMm5oDTPLte4ajcGmzbMNBPKwMNyqdkU1",
      "tz1VChh1HhdBroffKUsxpC4AiRbKz8HmWhKt",
      "tz1f3dYpTuVgAKBFVDccoG9E7QdvfLqqdEhK",
      "tz1RWrMTjVTDuQHB5u3yuXFDN4Nm4QeYYHPg",
      "tz1LMNXEyCMUMRqjCzu17wXoyrseZeun1XHH",
      "tz1L8GnDG3a9eEcobk2m5igY1tKkGy9NAj8B",
      "tz2RbMChvyhF3iccWSDUrd49yV3xxcWZV3W9",
      "tz1READXiim7P3bi115v3dJUCj7oVDkpcENN",
      "tz1ae2d1BJt7YUqaaec6Xenh3mBqS7VjSZtK",
      "tz1a95jRX4Z9WG7iHbPXZoQMLB9qN6wJvp4W",
      "tz1PjXHQ4btHN6eP5gvmLNftZXzYqQxgJcve",
      "tz2TUpiC4Cdamu1DPYZPR46C8HkCJrGSmwA5",
      "tz1auxQZSVmRkkcByiEJpANgGwQp2XBKVtoH",
      "tz1MRj7ox1firCSNSjiN5QpbZq4NvDrdYnLo",
      "tz1WMoJivTbf62hWLC5e4QvRwk9dps2r6tNs",
      "tz1PjgWqPzW9UwKeGCvr5DvQ2oWKkLtwzzeY",
      "tz1iHUe1gN3S5U8oPD3YrGRTKeJuzXVYADX7",
      "tz1ewRNTNDWYQtWo8ZT7My9dEuZ99gArdVEG",
      "tz1WjcomtxRtcBK46FfiNa4svgXZ9jSPteEn",
      "tz1ghu8YcCTnmUEV2xKBvSxbet3myMiWUGjo",
      "tz1bCY3YAujKdKx5uXnq1VVRDJWuANWypggx",
      "tz1MinM1m5XLaSwpKtGaNQTpfjnWxGMEqBqK",
      "tz1VTZoiw1tyZkPrigGUF634hjaLLVaa6yRn",
      "tz1f8cSoUs93Vms39hhheNDR3ruzEjiee41t",
      "tz2QyvJXTDUaDcHs3aeVoxkgUDx9mrUz2pNB",
      "tz1UCe7sGov6pPRu2mt4gje3mhiBpnfzBNg3",
      "tz1abaw4vnnefV2REKLAt96JXdpXTNrFaGgu",
      "tz1PEDxHRBfMTSeMUeXid5SaWCbsX38f5e7x",
      "tz1UDEV4RAYqjRCHKQD72Z2WjfyChmXECRNB",
      "tz1gEEMESBfMQFrPLmGhkGaxSRe7Mxo1ousA",
      "tz1Zd2JPDmKa2xm2B1yZJUvjya5dJCdAW5FG",
      "tz1Nw1BFWk5f2xc7McVvbS5udKkWfGAq3VxT",
      "tz1MnWvXnrcYmuN9tcaUR1gryewniXB91GJo",
      "tz1eA5EcParQ4w7TWZa4fJrgqmXX8Nx7csG2",
      "tz1YEHg9XGeW5xvPp7QFe9J2mptxW3zzh4Za",
      "tz1SYj4S3GnWLMHL63UfGiuDj9B2wfxgkSqn",
      "tz2WNxPcE7JZhAFfqGEHkMtd2gcHaeiJKMWE",
      "tz1aojoSy815BfRF3423C44k58iB1ZjcZ7xe",
      "tz2DUK7JX39ZKiSZ2Q9FkDNSzRmsoCud9HDF",
      "tz1iU9t1FGEfuVKMfodbTh4U3Hb5Tp9mbJPU",
      "tz1R2TEK7ePgYzpvYYpCDyRzbHW64soAZr7R",
      "tz1Whv5PSM1KHned6rkVPhvz7W98rVv6ZsxN",
      "tz1gVbpaamDxgfZySTqKmG69AHrKZeb2pxmy",
      "tz1ihzuke79QCbnPUVG7Kj5srYS3zUbHtouh",
      "tz2EfHhguK3fSrwdxxoRtXcm4CrkssJRRxMc",
      "tz1M6b1rskdc4DDvfKtPgPztPmTMU1UMqzRb",
      "tz1iDL2YsQTQLkr3pHz3tdGZuFpQdn9v2ZQW",
      "tz2DurpCmTtZp7YPHmP2hPyonZ3G6VMxqWSQ",
      "tz1dzjZBfSJZ5K52WGs3ZV5uU9zQzkLELpqa",
      "tz1TnC3tqoPK9yU43eqxD5RT2Yi4xkwjUwAb",
      "tz1XKtXFxz9SFeh751KbSuAyeK2GQmgS5xJM",
      "tz2AkCePTJCnNbyBL2vfyC9jndzP9dj6ASEf",
      "tz2PvPSuA4ZX3MY8DSyHyFqs2HosAAghkUoN",
      "tz2KjEZcsMpxr2nEfBRZfnBiF85z8jQ6MGLV",
      "tz1UqY2zsjxh3vCGXmBKRGLgESU4mgMJo1Sh",
      "tz1gS7VbdxweZJBGzArr457dVf4ngpAJuvqM",
      "tz1XT3UpUAGM7zse1Y8PTnoojPRjNu4zQQe8",
      "tz1hgBnPS3hhCfxHQNyAaFuaoXdzbn3wRGuB",
      "tz1ModnzoHjjVaKTReeQUQviaTx38fNe7Cxa",
      "tz1VXBUKZNb6NSJjw8xQkcC7Eh6fGE3FckVh",
      "tz2XLinaZ25P76XJhc2KjDnx5Exw4jtvifRN",
      "tz1bfZcQrezSJka4iKfUVJZufCzpPzQCDRWn",
      "tz1RWrMTjVTDuQHB5u3yuXFDN4Nm4QeYYHPg",
      "tz1WCVbp4CNtAU7THuetheRNDhdGTfE7YQ3n",
      "tz1VRVgxo1BsQJNUvvxsCXVmGhMjPMEdVowZ",
      "tz1iEYZQzNSgVYN4ABGt8Cu8W8aJexmgqsA8",
    ];*/

    const operation = await tezos.wallet.batch([
      {
        kind: OpKind.TRANSACTION,
        ...contract.methods
          .add_address_to_white_list(white_list)
          .toTransferParams(),
      },
    ]);
    const batch_response = await operation.send();
    console.log(batch_response);
    batch_response.confirmation();

    console.log("Operation finished");
    return null;
  } catch (error) {
    console.log(error);
    let my_error: _error = error as _error;
    console.log(my_error.data[1]?.with.string);
    return null;
  }
};
export const create_phase = async (tezos: TezosToolkit) => {
  try {
    console.log("Getting contract entries");
    //const contract = await tezos.wallet.at(SALE_CONTRACT_ADDRESS);

    console.log("Waiting operation finish");
    //const result = await contract.storage<t_sale_contract_storage>();
    const contract = await tezos.wallet.at(SALE_CONTRACT_ADDRESS);

    const operation = await tezos.wallet.batch([
      {
        kind: OpKind.TRANSACTION,
        ...contract.methods
          .create_new_phase(
            1653400800, //FROM
            100, //MINT X TX
            0, //MINT X WALLET
            "TEST", //NAME
            1653487200, //TO
            300, //SUPPLY
            1000000, // UTZ COST
            false //WHITE LISTED ONLY
          )
          .toTransferParams(),
      },
    ]);
    const batch_response = await operation.send();
    console.log(batch_response);
    batch_response.confirmation();
    //const my_entries = response.methodsObject;
    console.log("Operation finished");
    return null;
  } catch (error) {
    console.log(error);
    let my_error: _error = error as _error;
    console.log(my_error.data[1]?.with.string);
    return null;
  }
};
export const create_allow_distribute_coin = async (tezos: TezosToolkit) => {
  try {
    console.log("Getting contract entries");
    //const contract = await tezos.wallet.at(SALE_CONTRACT_ADDRESS);

    console.log("Waiting operation finish");
    //const result = await contract.storage<t_sale_contract_storage>();
    const contract = await tezos.wallet.at(SALE_CONTRACT_ADDRESS);

    const operation = await tezos.wallet.batch([
      {
        kind: OpKind.TRANSACTION,
        ...contract.methods
          .create_coin(
            "ipfs://bafkreiapdbfyk6c25zw7taiymym2did4ju5nkgqr76vkjok3citbygotyi"
          )
          .toTransferParams(),
      },
      {
        kind: OpKind.TRANSACTION,
        ...contract.methods
          .add_allowed_coin([
            {
              coin_id: 0,
              required: 1,
            },
          ])
          .toTransferParams(),
      },
      {
        kind: OpKind.TRANSACTION,
        ...contract.methods
          .mint_coin(2, "tz1ggkUZ9nQYgkXGQVAPFWUyvhbCHnfRrVNn", 0)
          .toTransferParams(),
      },
    ]);

    const batch_response = await operation.send();
    console.log(batch_response);
    batch_response.confirmation();
    //const my_entries = response.methodsObject;
    console.log("Operation finished");
    return null;
  } catch (error) {
    console.log(error);
    let my_error: _error = error as _error;
    console.log(my_error.data[1]?.with.string);
    return null;
  }
};
export const close_lasIPhase = async (tezos: TezosToolkit) => {
  try {
    console.log("Getting contract entries");
    //const contract = await tezos.wallet.at(SALE_CONTRACT_ADDRESS);

    console.log("Waiting operation finish");
    //const result = await contract.storage<t_sale_contract_storage>();
    const contract = await tezos.wallet.at(SALE_CONTRACT_ADDRESS);

    const operation = await tezos.wallet.batch([
      {
        kind: OpKind.TRANSACTION,
        ...contract.methods.close_lasIPhase(false).toTransferParams(),
      },
    ]);

    const batch_response = await operation.send();
    console.log(batch_response);
    batch_response.confirmation();
    //const my_entries = response.methodsObject;
    console.log("Operation finished");
    return null;
  } catch (error) {
    console.log(error);
    let my_error: _error = error as _error;
    console.log(my_error.data[1]?.with.string);
    return null;
  }
};

export const queryMetadata = async (
  storage: IContractFA2Storage | IContractSalezStorage,
  token_id: number
) => {
  let metadata: null | ITokenMetadata = null;
  try {
    const query_token_metadata =
      await storage.token_metadata.get<IContractTokenMetadata>(token_id);
    //console.log(query_token_metadata);
    if (query_token_metadata) {
      const query_ipfs = query_token_metadata.token_info.get("");
      //console.log(query_ipfs);
      if (query_ipfs) {
        const query_uri: string = Object.values(
          unpackData(query_ipfs, { prim: "string" })
        )[0] as string;
        //console.log("token_metadata_uri: ", query_uri);
        const query_metadata = await axios.get<ITokenMetadata>(
          query_uri.replace("ipfs://", "https://gateway.ipfs.io/ipfs/")
        );
        //console.log("token_metadata: ", metadata);
        metadata = query_metadata.data;
      }
    }
  } catch (error) {
    console.log(error);
  }
  return metadata;
};

export type t_fingerz = {
  token_id: number;
  level: number;
};
export type t_wanted = {
  bonuz: t_fingerz[];
  wanted: t_fingerz[];
  expire_on: number;
  reward_ratio: number;
  metadata: string;
};

export const setWanted = async (tezos: TezosToolkit, wanted: t_wanted[]) => {
  try {
    const contract = await tezos.wallet.at(SALE_CONTRACT_ADDRESS);

    const operation = tezos.wallet.batch([
      {
        kind: OpKind.TRANSACTION,
        ...contract.methods.add_rewards(wanted).toTransferParams(),
      },
    ]);

    const batch_response = await operation.send();
    console.log(batch_response);
    batch_response.confirmation();
    //const my_entries = response.methodsObject;
    console.log("Operation finished");
  } catch (error) {
    console.log(error);
  }
  return null;
};
export type t_coin_holders = {
  [address: string]: {
    token_ids: number[];
    total: number;
    alias: string;
  };
};
export const mintCoins = async (
  tezos: TezosToolkit,
  coin_holders: t_coin_holders,
  coin_id: number = 0,
  multiplier: number = 1
) => {
  try {
    const contract = await tezos.wallet.at(SALE_CONTRACT_ADDRESS);

    const transaction: WalletParamsWithKind[] = Object.keys(coin_holders).map(
      (address) => {
        return {
          kind: OpKind.TRANSACTION,
          ...contract.methods
            .mint_coin(
              coin_holders[address].total * multiplier,
              address,
              coin_id
            )
            .toTransferParams(),
        };
      }
    );
    const operation = tezos.wallet.batch(transaction);

    const batch_response = await operation.send();
    console.log(batch_response);
    batch_response.confirmation();
    //const my_entries = response.methodsObject;
    console.log("Operation finished");
  } catch (error) {
    console.log(error);
  }
  return null;
};

export const questStatus = async (
  onUpdate: (questList: IQuestList) => void
) => {
  //let questList: t_holders_by_quest = {};
  try {
    const questRef = ref(database, `quests`);
    onValue(questRef, (quest_snapshot) => {
      if (quest_snapshot.exists() && quest_snapshot.hasChildren()) {
        const holders = quest_snapshot.val() as {
          [address: IAddress]: { [name: string]: IQuestBase };
        };
        let questList: IQuestList = {};
        Object.entries(holders).forEach(([address, quests]) => {
          Object.entries(quests).forEach(([name, quest]) => {
            const queryQuestList = questList[name] || [];
            questList = {
              ...questList,
              [name]: queryQuestList.concat({
                ...questBase,
                ...quest,
                address,
              }),
            };
          });
        });
        onUpdate(questList);
      }
    });
    //const quest_snapshot = await get(quest_ref);
  } catch (error) {
    console.log(error);
  }
  //console.log(questList);
  //return { questList };
};

export const mintQuestRewards = async (
  tezos: TezosToolkit,
  holders: IHolders
) => {
  try {
    const contract = await tezos.wallet.at(SALE_CONTRACT_ADDRESS);
    const transaction: WalletParamsWithKind[] = Object.entries(holders).map(
      ([address, quest]) => {
        const amount =
          quest.rewardAmount * quest.timesCompleted - quest.timesRewarded;
        return {
          kind: OpKind.TRANSACTION,
          ...contract.methods
            .mint_coin(amount, address, quest.rewardID)
            .toTransferParams(),
        };
      }
    );
    const operation = tezos.wallet.batch(transaction);
    const response = await operation.send();
    const confirmation = await response.confirmation();
    //console.log(batch_confirmation);

    return {
      operation,
      response,
      confirmation,
    };
  } catch (error) {
    console.log(error);
    return null;
  }
};
export const updateHolders = (
  holders: IHolders,
  queryOpHash: string
): IHolders => {
  return Object.entries(holders).reduce((prev, [address, quest]) => {
    const opHash = quest.opHash.concat(queryOpHash);
    const timesRewarded = quest.timesRewarded + 1;
    return { ...prev, [address]: { ...quest, opHash, timesRewarded } };
  }, {});
};
export const rewardSelectedQuests = async (
  tezos: TezosToolkit,
  holders: IHolders,
  quest: string
) => {
  try {
    const queryTransaction = await mintQuestRewards(tezos, holders);
    if (queryTransaction?.confirmation.completed) {
      const queryOpHash = queryTransaction
        ? queryTransaction.response.opHash
        : "";
      console.log("Writing to Firebase");
      const rewardQuest = httpsCallable<IDistributeArgs, boolean>(
        getFunctions(),
        "rewardQuest"
      );
      await rewardQuest({
        holders: updateHolders(holders, queryOpHash),
        quest,
      });
    }
  } catch (error) {
    console.log(error);
    return null;
  }
};
export const setQuest = async (content: ITwitterMeta) => {
  try {
    const setupTwitterQuest = httpsCallable<ITwitterMeta, void>(
      getFunctions(),
      "setupTwitterQuest"
    );
    await setupTwitterQuest(content);
  } catch (error) {
    console.log(error);
  }
};

export const fixQuests = async () => {
  try {
    const fixQuest = httpsCallable(getFunctions(), "fixQuest");
    await fixQuest();
  } catch (error) {
    console.log(error);
  }
};
