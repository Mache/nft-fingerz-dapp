import { Box, Button, Heading, HStack, VStack, Text } from "native-base";
import React, { useState } from "react";
import Legacy from "./legacy";
import Others from "./others";
import Quests from "./quests";
import UpdateTweet from "./quests/updateTweet";
interface IToolsTabs {
  tab: string;
}
const ToolsTabs = ({ tab }: IToolsTabs) => {
  switch (tab) {
    case "QUESTS":
      return (
        <Box my={{ base: "10px" }}>
          <Heading fontSize={{ base: "2xl" }}>QUESTS</Heading>
          <UpdateTweet />
          <Quests />
        </Box>
      );
    case "LEGACY":
      return <Legacy />;
    case "OTHERS":
      return <Others />;
    default:
      return null;
  }
};
const Tools = () => {
  const [currentTab, setCurrentTab] = useState("");
  return (
    <Box
      w={{ base: "350px", lg: "750px" }}
      minH={{ base: "250px", lg: "550px" }}
      justifyContent="center"
    >
      <Box>
        <Heading>TOOLS</Heading>
      </Box>
      <HStack justifyContent={{ base: "center" }} space={{ base: "10px" }}>
        {["QUESTS", "OTHERS", "LEGACY"].map((tab) => {
          return (
            <Box key={tab}>
              <Button
                onPress={() => {
                  setCurrentTab(tab);
                }}
              >
                <Text>{tab.toUpperCase()}</Text>
              </Button>
            </Box>
          );
        })}
      </HStack>
      <VStack space="10px">
        <ToolsTabs tab={currentTab} />
      </VStack>
    </Box>
  );
};
export default Tools;
