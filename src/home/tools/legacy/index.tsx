import { SigningType } from "@airgap/beacon-sdk";
import {
  Box,
  Heading,
  TextArea,
  Stack,
  InputGroup,
  InputLeftAddon,
  Input,
  Button,
  Text,
  VStack,
} from "native-base";
import React, { useState } from "react";
import { t_coin_holders, mintCoins, get_entries } from "../d_api";

import { validateSignature } from "@taquito/utils";
import { useSelector } from "react-redux";
import { RootState } from "../../../settings/store";
const Legacy = () => {
  const {
    wallet: { tezos, beacon },
  } = useSelector((state: RootState) => state);
  const [coin_holders, setCoinHolders] = useState("");
  const [coin_id, setCoinID] = useState("0");
  const [multiplier, setMultiplier] = useState("1");
  return (
    <VStack space={{ base: "10px" }} justifyContent={{ base: "center" }}>
      <Heading fontSize={{ base: "2xl" }} mt={{ base: "10px" }}>
        MINT COINS TO HOLDERS
      </Heading>
      <Box alignItems="center" w="100%">
        <TextArea
          rounded="md"
          value={coin_holders}
          onChangeText={(text) => {
            setCoinHolders(text);
          }}
          w="100%"
          placeholder="JSON FROM BOT"
          //maxW="300"
        />
      </Box>
      <Box>
        <Stack alignItems="center">
          <InputGroup w="100%">
            <InputLeftAddon w={{ base: "25%" }} children={"COIN ID #:"} />
            <Input
              value={coin_id}
              onChangeText={(text) => setCoinID(text.trim())}
              w="75%"
              //placeholder="#"
            />
          </InputGroup>
        </Stack>
      </Box>
      <Box>
        <Stack alignItems="center">
          <InputGroup w="100%">
            <InputLeftAddon w={{ base: "25%" }} children={"MULTIPLIER X:"} />
            <Input
              value={multiplier}
              onChangeText={(text) => setMultiplier(text.trim())}
              w="75%"
              //placeholder="#"
            />
          </InputGroup>
        </Stack>
      </Box>
      <Box>
        <Button
          onPress={async () => {
            if (tezos) {
              const holders = JSON.parse(coin_holders) as t_coin_holders;
              mintCoins(
                tezos,
                holders,
                Number.parseInt(coin_id, 10),
                Number.parseInt(multiplier, 10)
              );
            }
          }}
        >
          <Text>MINT COINS</Text>
        </Button>
      </Box>
      <Box>
        <Button
          onPress={async () => {
            const result = await beacon?.client.requestSignPayload({
              signingType: SigningType.RAW,
              payload: "HOLA",
            });
            console.log(result?.signature);
            if (result?.signature) {
              const verify = validateSignature(result.signature);
              console.log(verify);
            }
          }}
        >
          <Text>TEST SIGNING</Text>
        </Button>
      </Box>
      <Box>
        <Button
          onPress={async () => {
            if (tezos) {
              get_entries(tezos);
            }
          }}
        >
          <Text>GET ENTRIES</Text>
        </Button>
      </Box>
    </VStack>
  );
};

export default Legacy;
