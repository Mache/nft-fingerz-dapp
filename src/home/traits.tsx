import React from "react";
import { Box, Heading, Image } from "native-base";

const Traits = () => {
  return (
    <Box alignItems="center" mt="10px">
      <Heading>TRAITS</Heading>
      <Box bg="gray.900" rounded="md" p="15px" mt="5px">
        <Image
          alt="Traits"
          src={require("../../assets/traits.png")}
          w={{ base: "250px", lg: "500px" }}
          h={{ base: "250px", lg: "500px" }}
          resizeMode="center"
        />
      </Box>
    </Box>
  );
};

export default Traits;
