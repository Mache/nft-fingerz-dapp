import React from "react";
import { HStack, VStack, useBreakpointValue } from "native-base";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import NewBadge from "./components/new_badge";

const Badges = () => {
  const iconSize = useBreakpointValue({
    base: 18,
    lg: 30,
  });
  const containerW = useBreakpointValue({
    base: "300px",
    lg: "750px",
  });
  return (
    <VStack w="100%" justifyContent="center" alignItems="center">
      <HStack
        w={containerW}
        justifyContent="center"
        flexWrap="wrap"
        //space="5px"
        justifyItems="center"
      >
        <NewBadge
          icon={
            <MaterialCommunityIcons
              name="account-box"
              size={iconSize}
              color="white"
            />
          }
          description="PFP"
        />
        <NewBadge
          icon={
            <MaterialCommunityIcons
              name="account-group"
              size={iconSize}
              color="white"
            />
          }
          description="10K GENERATIVE"
        />
        <NewBadge
          icon={
            <MaterialCommunityIcons
              name="code-tags"
              size={iconSize}
              color="white"
            />
          }
          description="OWN CONTRACT"
        />
        <NewBadge
          icon={
            <MaterialCommunityIcons
              name="console-network-outline"
              size={iconSize}
              color="white"
            />
          }
          description="TEZOS ECOSYSTEM"
        />
        <NewBadge
          icon={
            <MaterialCommunityIcons
              name="nature"
              size={iconSize}
              color="white"
            />
          }
          description="LOW CARBON FOOTPRINT"
        />
        <NewBadge
          icon={
            <MaterialCommunityIcons
              name="gauge-empty"
              size={iconSize}
              color="white"
            />
          }
          description="LOW GAS FEE"
        />
        <NewBadge
          icon={
            <MaterialCommunityIcons
              name="account-convert-outline"
              size={iconSize}
              color="white"
            />
          }
          description="DYNAMIC NFT"
        />
        <NewBadge
          icon={
            <MaterialCommunityIcons
              name="currency-usd"
              size={iconSize}
              color="white"
            />
          }
          description="REWARD SYSTEM"
        />
        <NewBadge
          icon={
            <MaterialCommunityIcons
              name="open-source-initiative"
              size={iconSize}
              color="white"
            />
          }
          description="OPEN SOURCE"
        />
      </HStack>
    </VStack>
  );
};

export default Badges;
