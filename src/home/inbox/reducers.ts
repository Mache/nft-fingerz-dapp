import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { serverTimestamp } from "firebase/database";
import { IProfile, ITezosProfile } from "../wallet/reducers";
type IFirebaseTime = ReturnType<typeof serverTimestamp>;
export interface IFirebaseMessage {
  timestamp: IFirebaseTime;
  text: string;
  read: boolean;
}
export interface IBoxMessage {
  timestamp: number;
  text: string;
  read: boolean;
  profile: null | IProfile;
  message_id: string;
}
export interface IBoxIncome extends IBoxMessage {
  from_address: string;
}
export interface IBoxOutcome extends IBoxMessage {
  to_address: string;
}

export enum IBoxTab {
  INBOX,
  OUTBOX,
  CREATE,
  PROFILE,
}

export interface IBoxApi {
  to_address: null | string;
  body: string;
  outcome: IBoxOutcome[];
  income: IBoxIncome[];
  module: null | IBoxTab;
  filter: null | string;
}
const initial_state: IBoxApi = {
  to_address: null,
  body: "",
  income: [],
  outcome: [],
  module: IBoxTab.INBOX,
  filter: null,
};
const inbox_api = createSlice({
  name: "inbox_api",
  initialState: initial_state,
  reducers: {
    set_to_address: (state, action: PayloadAction<null | string>) => {
      state.to_address = action.payload;
    },
    set_body: (state, action: PayloadAction<string>) => {
      state.body = action.payload;
    },
    set_income: (state, action: PayloadAction<IBoxIncome[]>) => {
      state.income = action.payload;
    },
    set_outcome: (state, action: PayloadAction<IBoxOutcome[]>) => {
      state.outcome = action.payload;
    },

    set_module: (state, action: PayloadAction<null | IBoxTab>) => {
      state.module = action.payload;
    },
    set_filter: (state, action: PayloadAction<null | string>) => {
      state.filter = action.payload;
    },
  },
});

export const {
  set_to_address,
  set_income,
  set_outcome,
  set_body,
  set_module,
  set_filter,
} = inbox_api.actions;
export default inbox_api.reducer;
