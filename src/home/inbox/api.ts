import {
  get,
  onValue,
  push,
  ref,
  remove,
  serverTimestamp,
  set,
} from "firebase/database";
import { database } from "../../../App";
import { IProfile } from "../wallet/reducers";
import {
  IBoxIncome,
  IBoxMessage,
  IBoxOutcome,
  IFirebaseMessage,
} from "./reducers";

export const outcomes = async (
  address: string,
  onOutcome: (messages: IBoxOutcome[]) => void
) => {
  try {
    const messages_sent_ref = ref(database, `messages/${address}/outcome`);
    onValue(messages_sent_ref, (snapshot) => {
      let message_outcome: IBoxOutcome[] = [];
      if (snapshot.exists()) {
        snapshot.forEach((wallet) => {
          let to_address = wallet.key as string;
          wallet.forEach((message) => {
            let body = message.toJSON() as IBoxMessage;
            const message_id = message.key as string;
            message_outcome.push({ ...body, to_address, message_id });
          });
        });
      }
      onOutcome(
        message_outcome.sort(
          (message_a, message_b) => message_b.timestamp - message_a.timestamp
        )
      );
    });
  } catch (error) {}
};
export const incomes = async (
  address: string,
  onIncome: (messages: IBoxIncome[]) => void
) => {
  try {
    const messages_received_ref = ref(database, `messages/${address}/income`);
    onValue(messages_received_ref, async (snapshot) => {
      let message_income: IBoxIncome[] = [];
      if (snapshot.exists()) {
        snapshot.forEach((wallet) => {
          const from_address = wallet.key as string;
          wallet.forEach((message) => {
            const body = message.toJSON() as IBoxMessage;
            const message_id = message.key as string;
            message_income.push({ ...body, from_address, message_id });
          });
        });
      }

      onIncome(
        message_income.sort(
          (message_a, message_b) => message_b.timestamp - message_a.timestamp
        )
      );
    });
  } catch (error) {}
};

export const sendMessage = async (
  from_addres: string,
  to_address: string,
  text: string,
  profile: null | IProfile
) => {
  try {
    const messages_sent_ref = ref(
      database,
      `messages/${from_addres}/outcome/${to_address}`
    );
    const messages_received_ref = ref(
      database,
      `messages/${to_address}/income/${from_addres}`
    );
    const receiver_profile_ref = ref(database, `profiles/${to_address}`);
    const query_profile = await get(receiver_profile_ref);
    const receiver_profile = query_profile.exists()
      ? (query_profile.val() as IProfile)
      : null;
    const timestamp = serverTimestamp();
    const message: IFirebaseMessage = {
      timestamp,
      text,
      read: false,
    };
    await set(push(messages_sent_ref), {
      ...message,
      profile: receiver_profile,
    });
    await set(push(messages_received_ref), {
      ...message,
      profile,
    });
  } catch (error) {
    console.log(error);
  }
};
export const removeIncomeMessage = async (
  my_address: string,
  from_addres: string,
  message_id: string
) => {
  try {
    const messages_received_ref = ref(
      database,
      `messages/${my_address}/income/${from_addres}/${message_id}`
    );
    await remove(messages_received_ref);
  } catch (error) {
    console.log(error);
  }
};
export const removeOutcomeMessage = async (
  my_address: string,
  to_address: string,
  message_id: string
) => {
  try {
    const messages_sent_ref = ref(
      database,
      `messages/${my_address}/outcome/${to_address}/${message_id}`
    );
    await remove(messages_sent_ref);
  } catch (error) {
    console.log(error);
  }
};
