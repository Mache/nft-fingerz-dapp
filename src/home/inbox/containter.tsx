import { Box, IBoxProps } from "native-base";
import React from "react";

interface IProps {
  children: React.ReactNode;
}
const style: IBoxProps = {
  w: { base: "350px", lg: "750px" },
  minH: { base: "400px", lg: "550px" },
};
export const InboxContainer: React.FC<IProps> = ({ children }: IProps) => {
  return <Box {...style}>{children}</Box>;
};
