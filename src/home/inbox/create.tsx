import React, { useState } from "react";
import {
  Box,
  Button,
  Input,
  InputGroup,
  InputLeftAddon,
  Stack,
  TextArea,
  VStack,
  Text,
} from "native-base";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../settings/store";
import { set_body, set_module, set_to_address, IBoxTab } from "./reducers";
import { sendMessage } from "./api";
import { MaterialCommunityIcons } from "@expo/vector-icons";

const Create = () => {
  const {
    wallet: { address, profile },
    inbox: { to_address, body },
  } = useSelector((state: RootState) => state);
  const dispatch = useDispatch();
  const [sending, setSending] = useState(false);
  return (
    <VStack space="10px" p="10px">
      <Box>
        <Stack alignItems="center">
          <InputGroup w="100%">
            <InputLeftAddon children={"wallet:"} />
            <Input
              onChangeText={(text) => dispatch(set_to_address(text.trim()))}
              w="100%"
              placeholder="address"
            />
          </InputGroup>
        </Stack>
      </Box>
      <Box alignItems="center" w="100%">
        <TextArea
          rounded="md"
          value={body}
          onChangeText={(text) => {
            if (body.length < 300) {
              dispatch(set_body(text));
            }
          }}
          w="100%"
          //maxW="300"
        />
      </Box>
      <Text>{`characters: ${body.length}/300`}</Text>
      <Box>
        <Button
          rounded="md"
          rightIcon={
            <MaterialCommunityIcons name="send" size={16} color="black" />
          }
          disabled={!(address && to_address) || sending}
          isLoading={sending}
          onPress={async () => {
            if (address && to_address) {
              setSending(true);
              await sendMessage(address, to_address, body, profile);
              dispatch(set_body(""));
              dispatch(set_module(IBoxTab.OUTBOX));
              setSending(false);
            }
          }}
        >
          SEND
        </Button>
      </Box>
    </VStack>
  );
};

export default Create;
