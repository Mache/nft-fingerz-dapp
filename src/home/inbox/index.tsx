import React, { useEffect, useState } from "react";
import { Box, Button, Heading, HStack, Spinner, VStack } from "native-base";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../settings/store";

import { set_income, set_module, set_outcome, IBoxTab } from "./reducers";
import Create from "./create";
import Sent from "./outcome";
import Received from "./income";
import { incomes, outcomes } from "./api";
import { InboxContainer } from "./containter";

const WalletNotSynced = () => {
  return (
    <Box justifyContent="center" h="full">
      <Box bg="black" borderRadius="3xl" p="20px">
        <Heading>SYNC WALLET FIRST</Heading>
      </Box>
    </Box>
  );
};
const Fetching = () => {
  return (
    <HStack space={2} justifyContent="center">
      <Spinner accessibilityLabel="Loading posts" />
      <Heading color="primary.500" fontSize="md">
        Loading
      </Heading>
    </HStack>
  );
};
const InboxModule = () => {
  const { module } = useSelector((state: RootState) => state.inbox);
  switch (module) {
    case IBoxTab.INBOX:
      return <Received />;
    case IBoxTab.OUTBOX:
      return <Sent />;
    case IBoxTab.CREATE:
      return <Create />;

    default:
      return null;
  }
};
const TopBar = () => {
  const { module } = useSelector((state: RootState) => state.inbox);
  const dispatch = useDispatch();
  return (
    <Button.Group isAttached mx="auto" size="md" mt="10px">
      <Button
        onPress={() => dispatch(set_module(IBoxTab.INBOX))}
        variant={module === IBoxTab.INBOX ? "outline" : "solid"}
      >
        INBOX
      </Button>
      <Button
        onPress={() => dispatch(set_module(IBoxTab.OUTBOX))}
        variant={module === IBoxTab.OUTBOX ? "outline" : "solid"}
      >
        OUTBOX
      </Button>
      <Button
        onPress={() => dispatch(set_module(IBoxTab.CREATE))}
        variant={module === IBoxTab.CREATE ? "outline" : "solid"}
      >
        CREATE
      </Button>
    </Button.Group>
  );
};
const Inbox = () => {
  const [fetching, setFetching] = useState(false);
  const { address, profile } = useSelector((state: RootState) => state.wallet);
  const dispatch = useDispatch();
  useEffect(() => {
    if (address) {
      incomes(address, (income) => dispatch(set_income(income)));
      outcomes(address, (outcome) => dispatch(set_outcome(outcome)));
    }
  }, [address]);
  return (
    <InboxContainer>
      {address ? (
        <Box>
          {fetching ? (
            <Fetching />
          ) : (
            <Box>
              <VStack>
                <TopBar />
                <InboxModule />
              </VStack>
            </Box>
          )}
        </Box>
      ) : (
        <WalletNotSynced />
      )}
    </InboxContainer>
  );
};

export default Inbox;
