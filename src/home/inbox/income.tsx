import React, { useEffect } from "react";
import {
  Box,
  Input,
  InputGroup,
  InputLeftAddon,
  InputRightAddon,
  Pressable,
  Stack,
  Text,
  VStack,
} from "native-base";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../settings/store";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { set_filter } from "./reducers";
import { removeIncomeMessage } from "./api";
import MiniProfile from "../components/miniProfile";

const Income = () => {
  const {
    inbox: { filter, income },
    wallet: { address },
  } = useSelector((state: RootState) => state);
  const dispatch = useDispatch();

  return (
    <VStack space="10px" p="10px">
      <Box>
        <Stack alignItems="center">
          <InputGroup w="full">
            <InputLeftAddon children={"wallet:"} />
            <Input
              value={filter !== null ? filter : ""}
              onChangeText={(text) => dispatch(set_filter(text))}
              w="full"
              placeholder="address"
            />
            <InputRightAddon
              children={
                <MaterialCommunityIcons
                  name="account-search"
                  size={16}
                  color="black"
                />
              }
            />
          </InputGroup>
        </Stack>
      </Box>
      <VStack>
        {income
          .filter((message) => {
            if (filter && filter.length >= 3) {
              return message.from_address.includes(filter);
            } else {
              return true;
            }
          })
          .map((message) => {
            return (
              <Box
                key={message.timestamp}
                bg="gray.900"
                rounded="md"
                m="10px"
                p="10px"
              >
                <Box alignSelf="flex-end">
                  <Pressable
                    onPress={() => {
                      if (address) {
                        removeIncomeMessage(
                          address,
                          message.from_address,
                          message.message_id
                        );
                      }
                    }}
                  >
                    <MaterialCommunityIcons
                      name="close-thick"
                      size={16}
                      color="black"
                    />
                  </Pressable>
                </Box>
                <Box borderBottomWidth="2px" borderBottomColor="gray.600">
                  <Text fontSize="xs">{`FROM: ${message.from_address}`}</Text>
                  <MiniProfile profile={message.profile} />
                </Box>

                <Box borderBottomWidth="2px" borderBottomColor="gray.600">
                  <Text>{message.text}</Text>
                </Box>
                <Box>
                  <Text fontSize="xs">
                    {`DATE TIME: ${new Date(message.timestamp)
                      .toString()
                      .slice(4, 24)}`}
                  </Text>
                </Box>
              </Box>
            );
          })}
      </VStack>
    </VStack>
  );
};

export default Income;
