import React from "react";
import { VStack } from "native-base";
import Mint from "./mint";
import RoadMap from "./roadmap";
import Lore from "./lore";
import Logo from "./logo";
import MyAutoCycling from "./components/my_auto_cycling";
import Badges from "./badges";
import Traits from "./traits";
import Faq from "./faq";
import Team from "./team";
import Intro from "./intro";
const Landing = () => {
  return (
    <VStack>
      <Logo />
      <MyAutoCycling
        data={[
          require("../../assets/banner1.png"),
          require("../../assets/banner2.png"),
          require("../../assets/banner3.png"),
          require("../../assets/banner4.png"),
          require("../../assets/banner5.png"),
        ]}
      />
      <Badges />
      <Mint />
      <Lore />
      <RoadMap />
      <Traits />
      <Faq />
      <Team />
    </VStack>
  );
};
export default Landing;
