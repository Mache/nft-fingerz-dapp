import React from "react";
import { Image, Box } from "native-base";

const Logo = () => {
  return (
    <Box
      justifyContent="center"
      alignItems="center"
      //mt={{ base: "-20px" }}
      mb={{ base: "10px" }}
      w="100%"
      //bg="black"
    >
      <Image
        alt="Logo"
        src={require("../../assets/logo.png")}
        width={{
          base: "320px",
          lg: "1200px",
        }}
        height={{
          base: "80px",
          lg: "300px",
        }}
        resizeMode="center"
      />
    </Box>
  );
};

export default Logo;
