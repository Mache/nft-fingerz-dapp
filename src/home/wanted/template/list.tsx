import React from "react";
import { Box, Button, Divider } from "native-base";

import moment from "moment";
import { collecIContractReward } from "../api";
import { t_list_props } from "../types";
import Item from "./item";
import { useSelector } from "react-redux";
import { RootState } from "../../../settings/store";
import StyledVStack from "../../components/styledVStack";
import BHeading from "../../components/blackHeading";
import BText from "../../components/blackText";

const List = ({ wanted }: t_list_props) => {
  const { tezos } = useSelector((state: RootState) => state.wallet);

  if (Object.keys(wanted).length < 1) {
    return <BText>SOON</BText>;
  }
  return (
    <Box w={{ base: "300px", lg: "700px" }}>
      {Object.keys(wanted).map((reward_id) => {
        const multiplier = wanted[reward_id].complete
          ? wanted[reward_id].multiplied - 1
          : Object.values(wanted[reward_id].bonuz).filter(
              (bonuz) => bonuz.owned_level
            ).length;

        return (
          <StyledVStack key={reward_id}>
            <BHeading>{`MISSION #${reward_id}`}</BHeading>
            <BText>{`CREATED ${moment
              .unix(wanted[reward_id].created)
              .format()}`}</BText>
            <BText>{`PRIZE ${wanted[reward_id].prize / 10}% (+${
              (wanted[reward_id].prize / 10) * multiplier
            }%)`}</BText>
            <BText>{`MULTIPLIED X${multiplier + 1}`}</BText>
            {wanted[reward_id].complete && (
              <BText>{`CASHED ${wanted[reward_id].cashed / 1000000} TZ`}</BText>
            )}
            <BText>{`EXPIRE ${moment
              .unix(wanted[reward_id].expire_on)
              .format()}`}</BText>
            <Divider thickness="2" my="10px" />
            <BHeading>WANTED</BHeading>
            <Box
              flexDirection={{ base: "column", lg: "row" }}
              flexWrap={{ base: "wrap" }}
              justifyContent={{ base: "center" }}
            >
              {Object.keys(wanted[reward_id].wanted).map((token_id) => {
                return (
                  <Item
                    key={token_id}
                    fingerz={wanted[reward_id].wanted[token_id]}
                    token_id={token_id}
                  />
                );
              })}
            </Box>
            <Divider thickness="2" my="10px" />
            <BHeading>BONUZ</BHeading>

            <Box
              flexDirection={{ base: "column", lg: "row" }}
              flexWrap={{ base: "wrap" }}
              justifyContent={{ base: "center" }}
            >
              {Object.keys(wanted[reward_id].bonuz).map((token_id) => {
                return (
                  <Item
                    key={token_id}
                    fingerz={wanted[reward_id].bonuz[token_id]}
                    token_id={token_id}
                  />
                );
              })}
            </Box>
            <BText>
              Reward is multiplied for each additional bonus objective complete
            </BText>
            {!wanted[reward_id].complete && (
              <Box>
                <Divider thickness="2" my="10px" />
                <Button
                  onPress={() => {
                    if (tezos) {
                      collecIContractReward(
                        tezos,
                        Number.parseInt(reward_id, 10)
                      ).catch((error) => console.log(error));
                    }
                  }}
                >
                  <BText>CLAIM</BText>
                </Button>
              </Box>
            )}
          </StyledVStack>
        );
      })}
    </Box>
  );
};
export default List;
