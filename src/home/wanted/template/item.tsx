import React, { useState } from "react";
import { Box, HStack, Text, useBreakpointValue, Image } from "native-base";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { t_item_props } from "../types";
const Item = ({ fingerz, token_id }: t_item_props) => {
  //const [fetching,setFetching] = useState(false)
  const iconSize = useBreakpointValue({
    base: 16,
    lg: 24,
  });

  return (
    <Box
      /*bg="black"
      shadow="2"
      justifyContent="center"
      w={{ base: "80px", lg: "80px" }}
      h={{ base: "80px", lg: "80px" }}
      p="5px"
      m="5px"
      rounded="md"
      alignItems="center"*/
      bg="black"
      rounded={{ base: "md" }}
      w={{ base: "250px" }}
      p={{ base: "10px" }}
      m={{ base: "10px" }}
    >
      <HStack space={{ base: "10px" }} justifyContent={{ base: "center" }}>
        <Box
        //w={{base:"50%"}}
        >
          <Image
            alt={`#${token_id}`}
            src={require("../../../../assets/wanted.png")}
            width={{ base: "80px", lg: "120px" }}
            height={{ base: "100px", lg: "150px" }}
          />
        </Box>
        <Box
        //w={{ base: "50%" }}
        >
          <Text fontSize="xs">{`ID #${token_id}`}</Text>

          <HStack alignItems={{ base: "center" }}>
            <Box>
              <Text fontSize="xs">{`LVL #${fingerz.required_level}`}</Text>
            </Box>
            {fingerz.owned && (
              <Box mx={{ base: "10px" }}>
                {fingerz.owned_level ? null : (
                  <MaterialCommunityIcons
                    name="alert"
                    size={iconSize}
                    color="white"
                  />
                )}
              </Box>
            )}
          </HStack>
          <HStack alignItems={{ base: "center" }}>
            <Box>
              <Text fontSize="xs">{`OWNED`}</Text>
            </Box>
            {fingerz.owned ? (
              <MaterialCommunityIcons
                name="check-bold"
                size={iconSize}
                color="white"
              />
            ) : (
              <MaterialCommunityIcons
                name="close-thick"
                size={iconSize}
                color="white"
              />
            )}
          </HStack>
        </Box>
      </HStack>
    </Box>
  );
};

export default Item;
