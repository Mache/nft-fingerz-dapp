import { createSlice, PayloadAction } from "@reduxjs/toolkit";
export interface t_fingerz {
  required_level: number;
  owned: boolean;
  owned_level: boolean;
}
export interface t_wanted {
  [token_id: string]: t_fingerz;
}
export interface t_mission {
  wanted: t_wanted;
  bonuz: t_wanted;
  prize: number;
  expire_on: number;
  created: number;
  cashed: number;
  complete: boolean;
  bonuz_jailed: number[];
  multiplied: number;
}
export interface t_mission_list {
  [key: string]: t_mission;
}
export interface t_owned {
  [key: string]: { level: number };
}

interface t_wanted_api {
  active: t_mission_list;
  fetching_active: boolean;
  inactive: t_mission_list;
  fetching_inactive: boolean;
}
const initial_state: t_wanted_api = {
  active: {},
  inactive: {},
  fetching_active: false,
  fetching_inactive: false,
};
const wanted_api = createSlice({
  name: "wanted_api",
  initialState: initial_state,
  reducers: {
    set_active: (state, action: PayloadAction<t_mission_list>) => {
      state.active = action.payload;
    },
    set_inactive: (state, action: PayloadAction<t_mission_list>) => {
      state.inactive = action.payload;
    },
    set_fetching_active: (state, action: PayloadAction<boolean>) => {
      state.fetching_active = action.payload;
    },
    set_fetching_inactive: (state, action: PayloadAction<boolean>) => {
      state.fetching_inactive;
    },
  },
});

export const {
  set_active,
  set_inactive,
  set_fetching_active,
  set_fetching_inactive,
} = wanted_api.actions;
export default wanted_api.reducer;
