import React from "react";
import { VStack } from "native-base";
import { useSelector } from "react-redux";
import { RootState } from "../../settings/store";
import List from "./template/list";

const Inactive = () => {
  const {
    wanted: { inactive },
  } = useSelector((state: RootState) => state);

  return <List wanted={inactive} />;
};

export default Inactive;
