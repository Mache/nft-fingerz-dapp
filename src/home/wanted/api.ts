import {
  BigMapAbstraction,
  MichelsonMap,
  OpKind,
  TezosToolkit,
} from "@taquito/taquito";
import Big from "big.js";
import { RPC_NODE, SALE_CONTRACT_ADDRESS } from "../contracts/api";
import {
  IAddress,
  IContractReward,
  IContractTokenMetadata,
  _error,
} from "../contracts/types";
import { t_mission_list, t_wanted } from "./reducers";
import {
  packDataBytes,
  unpackDataBytes,
  MichelsonData,
} from "@taquito/michel-codec";
import { getUnixTime } from "../time/api";

const unpackData = (
  bytes: string,
  type: any // { prim: string }
) => {
  return unpackDataBytes({ bytes }, type);
};

const packData = (
  value: MichelsonData, //{ [key: string]: any },
  type: any // { prim: string }
) => {
  return packDataBytes(value, type).bytes;
};

// Packing
packData({ int: "200" }, { prim: "nat" }); // 05008803

// Unpacking
unpackData("05008803", { prim: "nat" }); // {"int":"200"}

export const getMissions = async (
  rewards_list: MichelsonMap<Big, IContractReward>,
  ledger: BigMapAbstraction,
  token_metadata: BigMapAbstraction,
  address: IAddress,
  unix_time: number
  /*onMissions?: (missions: {
    active: t_mission_list;
    inactive: t_mission_list;
  }) => void*/
) => {
  let active: t_mission_list = {};
  let inactive: t_mission_list = {};
  try {
    for (let [id, mission] of rewards_list.entries()) {
      let wanted: t_wanted = await Promise.all<t_wanted>(
        mission.wanted.map(async ({ token_id, level }) => {
          const query_owner = await ledger.get<IAddress>(token_id);
          const owned = query_owner === address;
          let owned_level = true;

          return {
            [token_id.toString()]: {
              required_level: level.toNumber(),
              owned,
              owned_level,
            },
          };
        })
      ).then((query_wanted) => {
        return query_wanted.reduce((prev, next) => {
          return { ...prev, ...next };
        }, {});
      });
      let bonuz: t_wanted = await Promise.all<t_wanted>(
        mission.bonuz.map(async ({ token_id, level }) => {
          const query_owner = await ledger.get<IAddress>(token_id);
          const owned = query_owner === address;
          let owned_level = false;
          if (owned) {
            const query_metadata =
              await token_metadata.get<IContractTokenMetadata>(token_id);
            if (query_metadata) {
              const query_level = query_metadata.token_info.get("level");
              if (query_level) {
                const unpacked_level = Object.values(
                  unpackData(query_level, { prim: "nat" })
                )[0] as string;
                //console.log(unpacked_level);
                owned_level =
                  Number.parseInt(unpacked_level, 10) >= level.toNumber();
              }
            }
          }
          return {
            [token_id.toString()]: {
              required_level: level.toNumber(),
              owned,
              owned_level,
            },
          };
        })
      ).then((query_bonuz) => {
        return query_bonuz.reduce((prev, next) => {
          return { ...prev, ...next };
        }, {});
      });
      const reward_id = id.toString();
      const prize = mission.reward_ratio.toNumber();
      const expire_on = mission.expire_on.toNumber();
      const created = mission.created.toNumber();
      const cashed = mission.cashed ? mission.cashed.toNumber() : 0;
      const multiplied = mission.multiplied ? mission.multiplied.toNumber() : 1;
      const complete = mission.complete;
      const bonuz_jailed = mission.bonuz_jailed
        ? mission.bonuz_jailed.map((bnuz_j) => bnuz_j.toNumber())
        : [];
      if (unix_time < expire_on && !complete) {
        active = {
          ...active,
          [reward_id]: {
            wanted,
            bonuz,
            prize,
            expire_on,
            created,
            cashed,
            multiplied,
            complete,
            bonuz_jailed,
          },
        };
      } else {
        if (unix_time >= expire_on && !complete) {
          const expired_wanted = Object.keys(wanted).reduce(
            (prev, token_id) => {
              return {
                ...prev,
                [token_id]: {
                  ...wanted[token_id],
                  owned: false,
                  owned_level: false,
                },
              };
            },
            {} as t_wanted
          );
          const expired_bonuz = Object.keys(bonuz).reduce((prev, token_id) => {
            return {
              ...prev,
              [token_id]: {
                ...bonuz[token_id],
                owned: false,
                owned_level: false,
              },
            };
          }, {} as t_wanted);
          inactive = {
            ...inactive,
            [reward_id]: {
              wanted: expired_wanted,
              bonuz: expired_bonuz,
              prize,
              expire_on,
              created,
              cashed,
              multiplied,
              complete,
              bonuz_jailed,
            },
          };
        } else {
          const completed_wanted = Object.keys(wanted).reduce(
            (prev, token_id) => {
              return {
                ...prev,
                [token_id]: {
                  ...wanted[token_id],
                  owned: true,
                  owned_level: true,
                },
              };
            },
            {} as t_wanted
          );
          const completed_bonuz = Object.keys(bonuz).reduce(
            (prev, token_id) => {
              const owned = bonuz_jailed.includes(
                Number.parseInt(token_id, 10)
              );
              return {
                ...prev,
                [token_id]: {
                  ...bonuz[token_id],
                  owned: owned,
                  owned_level: owned,
                },
              };
            },
            {} as t_wanted
          );
          inactive = {
            ...inactive,
            [reward_id]: {
              wanted: completed_wanted,
              bonuz: completed_bonuz,
              prize,
              expire_on,
              created,
              cashed,
              multiplied,
              complete,
              bonuz_jailed,
            },
          };
        }
      }
    }
  } catch (error) {
    console.log(error);
  }

  //onMissions && onMissions({ active, inactive });
  return { active, inactive };
};
/*export const getOwned = async (
  fa2_storage: IContractFA2Storage,
  active: t_mission_list,
  address: string,
  onOwned: (owned: t_owned) => void
) => {
  try {
    const fugitives: number[] = [];
    Object.keys(active).forEach((reward_id) => {
      active[reward_id].wanted.forEach((wanted) => {
        fugitives.push(wanted.token_id);
      });
      active[reward_id].bonuz.forEach((bonuz) => {
        fugitives.push(bonuz.token_id);
      });
    });
    const fetch = await fa2_storage.ledger.getMultipleValues<string>(fugitives);
    let owned: t_owned = {};

    for (const [key, value] of fetch.entries()) {
      if (value === address) {
        const token_id = Number.parseInt(key.toString(), 10);
        const metadata = await fa2_storage.token_metadata.get<IContractTokenMetadata>(
          token_id
        );
        console.log(metadata);
        if (metadata) {
          const token_level = metadata.token_info.get("level");
          console.log(token_level);
          if (token_level) {
            const level = Number.parseInt(token_level.slice(-2), 10);

            console.log(level);
            owned = {
              ...owned,
              [token_id]: { level },
            };
          }
        }
      }
    }
    onOwned(owned);
  } catch (error) {
    console.log(error);
  }
};
*/
export const collecIContractReward = async (
  tezos: TezosToolkit,
  reward_id: number
) => {
  try {
    console.log("Collecting ", reward_id);
    console.log("Waiting operation finish");

    const contract = await tezos.wallet.at(SALE_CONTRACT_ADDRESS);
    const operation = contract.methods
      .collecIContractReward(reward_id)
      .toTransferParams();
    //const operation_cost = await tezos.estimate.transfer(operation);
    //console.log(operation_cost);
    const batch_op = await tezos.wallet.batch([
      {
        kind: OpKind.TRANSACTION,
        ...operation,
        //amount: operation_cost.totalCost,
        //mutez: true,
      },
    ]);
    const batch_response = await batch_op.send();
    console.log(batch_response);
    batch_response.confirmation();
    console.log("Operation finished");
    return null;
  } catch (error) {
    console.log(error);
    let my_error: _error = error as _error;
    console.log(my_error.data[1]?.with.string);
    return null;
  }
};
