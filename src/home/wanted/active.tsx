import React from "react";
import { VStack } from "native-base";
import { useSelector } from "react-redux";
import { RootState } from "../../settings/store";
import List from "./template/list";

const Active = () => {
  const {
    wanted: { active },
  } = useSelector((state: RootState) => state);

  return <List wanted={active} />;
};
export default Active;
