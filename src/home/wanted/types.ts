import { IAddress } from "../contracts/types";
import { t_fingerz, t_mission_list } from "./reducers";

export type t_item_props = {
  fingerz: t_fingerz;
  token_id: string;
};

export type t_list_props = {
  wanted: t_mission_list;
};
