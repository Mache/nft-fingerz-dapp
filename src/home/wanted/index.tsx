import React, { useEffect, useState } from "react";
import {
  Box,
  Button,
  Text,
  Image,
  HStack,
  Heading,
  Spinner,
  Divider,
  useBreakpointValue,
} from "native-base";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import Active from "./active";
import Inactive from "./inactive";
import { useSelector } from "react-redux";
import { RootState } from "../../settings/store";
const Fetching = () => {
  return (
    <HStack justifyContent={{ base: "center" }} alignItems={{ base: "center" }}>
      <Box>
        <Spinner size={{ base: "sm", lg: "lg" }} />
      </Box>
      <Box>
        <Text>Fetching contract data ...</Text>
      </Box>
    </HStack>
  );
};
const Wanted = () => {
  const { fetching_active, fetching_inactive } = useSelector(
    (state: RootState) => state.wanted
  );
  const [showActive, toggleActive] = useState(false);
  const [showExpired, toggleExpired] = useState(false);
  const iconSize = useBreakpointValue({
    base: 24,
    lg: 36,
  });
  return (
    <Box
      alignItems="center"
      //bg="gray.900"
      //rounded="md"
      w={{ base: "350px", lg: "750px" }}
      minH={{ base: "250px", lg: "550px" }}
      p={{ base: "10px" }}
    >
      <Box alignItems="center">
        <Heading>WANTED FINGERZ</Heading>
        <HStack
          justifyContent={{ base: "center" }}
          alignItems={{ base: "center" }}
          space={{ base: "10px" }}
        >
          <Box>
            <Image
              alt="Wanted"
              src={require("../../../assets/wanted.png")}
              width={{ base: "90px", lg: "150px" }}
              height={{ base: "110px", lg: "180px" }}
              rounded="md"
              resizeMode="center"
              borderColor="black"
              borderWidth="5px"
            />
          </Box>
          <Box
            bg="gray.900"
            rounded="md"
            p={{ base: "10px" }}
            w={{ base: "60%" }}
          >
            <Text>
              Some of our fingerz went outlaws, they had committed crimes, stuck
              in places where they shouldn't and for that we are listing them as
              wanted.
            </Text>
          </Box>
        </HStack>

        <Box my={{ base: "10px" }} w={{ base: "100%" }}>
          <Button
            //size={{ base: "100%" }}
            w={{ base: "100%" }}
            h={{ base: "24px", lg: "36px" }}
            endIcon={
              <MaterialCommunityIcons
                name={showActive ? "chevron-up" : "chevron-down"}
                size={iconSize}
                color="black"
              />
            }
            onPress={() => {
              toggleActive((state) => !state);
            }}
          >
            <Text>ACTIVE</Text>
          </Button>
        </Box>
        {showActive && (fetching_active ? <Fetching /> : <Active />)}
        <Box my={{ base: "10px" }} w={{ base: "100%" }}>
          <Button
            //size={{ base: "100%" }}
            w={{ base: "100%" }}
            h={{ base: "24px", lg: "36px" }}
            endIcon={
              <MaterialCommunityIcons
                name={showExpired ? "chevron-up" : "chevron-down"}
                size={iconSize}
                color="black"
              />
            }
            onPress={() => {
              toggleExpired((state) => !state);
            }}
          >
            <Text>INACTIVE</Text>
          </Button>
        </Box>
        {showExpired && (fetching_inactive ? <Fetching /> : <Inactive />)}
      </Box>
    </Box>
  );
};

export default Wanted;
