import {
  Button,
  Center,
  Modal,
  VStack,
  Text,
  Box,
  Heading,
  HStack,
} from "native-base";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../settings/store";
import { set_cookies } from "./leftPanel/reducers";

import MyNotification from "./components/my_notification";
const Cookies = () => {
  const {
    leftPanel: { showCookies },
    wallet: { cookies },
  } = useSelector((state: RootState) => state);
  const dispatch = useDispatch();
  return (
    <MyNotification is_open={showCookies && cookies === null}>
      <VStack
        p={{ base: "10px" }}
        justifyContent={{ base: "center" }}
        alignItems={{ base: "center" }}
      >
        <Box>
          <Heading fontSize={{ lg: "4xl" }}>COOKIES POLICY</Heading>
        </Box>
        <Box>
          <Text>
            This site use cookies, they are files stored on the local device
            that allow us to remember you.
          </Text>
          <Text>They are strictly necessary, used to stay signed in.</Text>
          <Text>
            If you proceed you're consenting the usage of cookies, otherwise
            leave this site.
          </Text>
        </Box>
        <HStack>
          <Button
            m="5px"
            onPress={() => {
              window.open("https://www.disney.com/", "_self");
            }}
          >
            <Text>LEAVE THIS SITE</Text>
          </Button>
          <Button
            m="5px"
            onPress={() => {
              dispatch(set_cookies(false));
              //AsyncStorage.setItem(ASYNC_STORAGE.COOKIES, "accepted");
            }}
          >
            <Text>PROCEED</Text>
          </Button>
        </HStack>
      </VStack>
    </MyNotification>
  );
};

export default Cookies;
