import React, { useEffect, useState } from "react";
import {
  Heading,
  Text,
  VStack,
  Image,
  HStack,
  Pressable,
  Box,
  Divider,
} from "native-base";
import { FA2_CONTRACT_ADDRESS, SALE_CONTRACT_ADDRESS } from "./contracts/api";

const Footer = () => {
  return (
    <Box w="100%" justifyContent="center" alignItems="center">
      <VStack
        w={{ base: "350px", lg: "750px" }}
        justifyContent="center"
        alignItems="center"
      >
        <VStack p="15px" justifyContent="center" alignItems="center">
          <Heading>CONTRACTS</Heading>
          <Box bg="gray.900" rounded="md" p="15px" mt="5px">
            <Text>{`FA2:   ${FA2_CONTRACT_ADDRESS}`}</Text>
            <Text>{`SALES: ${SALE_CONTRACT_ADDRESS}`}</Text>
            <Text>
              PROVENANCE:
              e419f1e168284060c38363db4a90201696745c39ebe7b96a286fabe478825f95
            </Text>
            <Text>{`REV #${require("../../revision.json").hash}`}</Text>
          </Box>
        </VStack>
        <VStack p="15px" justifyContent="center" alignItems="center">
          <Heading>AS SEEN ON</Heading>
          <HStack>
            <Pressable
              onPress={() => {
                window.open(
                  "https://nftcalendar.io/event/nft-world-of-fingerz/",
                  "_blank"
                );
              }}
            >
              <Image
                alt="NFT Calendar"
                src={require("../../assets/nftcalendar.svg")}
                width={{ base: "90px", lg: "200px" }}
                height={{ base: "90px", lg: "200px" }}
                resizeMode="center"
              />
            </Pressable>
            <Pressable
              onPress={() => {
                window.open(
                  "https://objkt.com/collection/nft_fingerz",
                  "_blank"
                );
              }}
            >
              <Image
                alt="Objkt.com"
                src={require("../../assets/objkt.png")}
                width={{ base: "70px", lg: "150px" }}
                height={{ base: "70px", lg: "150px" }}
                resizeMode="center"
              />
            </Pressable>
            <Pressable
              onPress={() => {
                window.open("https://medium.com/@m88ache_85359", "_blank");
              }}
            >
              <Image
                alt="Medium"
                src={require("../../assets/medium.png")}
                width={{ base: "120px", lg: "250px" }}
                height={{ base: "70px", lg: "150px" }}
                resizeMode="center"
              />
            </Pressable>
          </HStack>
        </VStack>
      </VStack>
    </Box>
  );
};

export default Footer;
