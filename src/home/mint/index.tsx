import React, { useState } from "react";
import {
  Box,
  Button,
  Center,
  Heading,
  HStack,
  Input,
  Image,
  Text,
  VStack,
  Divider,
} from "native-base";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import MyFrame from "../components/my_frame";
import { userMintCoin, userMintTZ } from "./api";
import moment from "moment";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../settings/store";

import { Video } from "expo-av";

const Mint = () => {
  const [amount, setAmount] = useState(1);
  const [minting, setMinting] = useState(false);
  const dispatch = useDispatch();
  const {
    contract: { salez_storage, fa2_storage },
    wallet: { tezos, address },
    mint: { allowed_coins, max_mint_per_tx },
  } = useSelector((state: RootState) => state);

  return (
    <Center>
      <MyFrame
        Body={
          <Box alignItems="center">
            {minting ? (
              <Video
                style={{ width: "300px", height: "300px" }}
                source={{
                  uri: require("../../../assets/hole.mp4"),
                }}
                shouldPlay
                isLooping
              />
            ) : (
              <Image
                alt="Unknown"
                src={require("../../../assets/unknown.png")}
                width="300px"
                height="300px"
                rounded="full"
                resizeMode="center"
                //borderColor="black"
                borderWidth="10px"
              />
            )}

            <HStack justifyContent="center" m="5px" space="5px">
              <Button
                rounded="full"
                size="80px"
                onPress={() => {
                  setAmount((state) => (state > 2 ? state - 1 : 1));
                }}
              >
                <MaterialCommunityIcons name="minus" size={50} color="white" />
              </Button>

              <Input
                h="80px"
                w="160px"
                textAlign="center"
                fontSize="2xl"
                fontFamily="heading"
                variant="rounded"
                value={`${amount}`.padStart(3, "0")}
                onChangeText={(text) => {
                  let value = Number.parseInt(text);
                  if (Number.isInteger(value)) {
                    if (value < max_mint_per_tx) {
                      setAmount(value);
                    } else {
                      setAmount(max_mint_per_tx);
                    }
                  }
                }}
              />

              <Button
                rounded="full"
                size="80px"
                onPress={() => {
                  setAmount((state) =>
                    state < max_mint_per_tx ? state + 1 : max_mint_per_tx
                  );
                }}
              >
                <MaterialCommunityIcons name="plus" size={50} color="white" />
              </Button>
            </HStack>
            <HStack mt="10px">
              <Button
                rounded="2xl"
                h="80px"
                w="140px"
                onPress={async () => {
                  if (tezos && salez_storage?.last_phase) {
                    console.log("MINT");
                    setMinting(true);
                    try {
                      await userMintTZ(
                        tezos,
                        amount,
                        salez_storage?.last_phase.utz_cost.toNumber()
                      );
                      setMinting(false);
                    } catch (error) {
                      setMinting(false);
                    }
                  }
                }}
                disabled={salez_storage?.last_phase === null || minting}
                variant="outline"
              >
                <Box justifyContent="center" alignItems="center">
                  <MaterialCommunityIcons
                    name="fingerprint"
                    size={50}
                    color="white"
                  />
                  <Text fontSize="md" textAlign="center">
                    MINT
                  </Text>
                </Box>
              </Button>
              {allowed_coins.length > 0 && (
                <VStack
                  h="80px"
                  justifyContent="center"
                  alignItems="center"
                  mx="10px"
                >
                  <Divider orientation="vertical" thickness="3px" maxH="30px" />
                  <Text textAlign="center">OR</Text>
                  <Divider orientation="vertical" thickness="3px" maxH="30px" />
                </VStack>
              )}
              {allowed_coins.length > 0 && (
                <Button
                  rounded="2xl"
                  h="80px"
                  w="140px"
                  onPress={async () => {
                    if (tezos && salez_storage?.last_phase) {
                      console.log("MINT");
                      setMinting(true);
                      try {
                        await userMintCoin(tezos, amount);
                        setMinting(false);
                      } catch (error) {
                        setMinting(false);
                      }
                    }
                  }}
                  disabled={salez_storage?.last_phase === null || minting}
                  variant="outline"
                >
                  <Box justifyContent="center" alignItems="center">
                    <Image
                      alt="Insert coin"
                      source={require("../../../assets/coin.svg")}
                      style={{ width: 50, height: 50, tintColor: "white" }}
                    />
                    <Text fontSize="md" textAlign="center">
                      INSERT COIN
                    </Text>
                  </Box>
                </Button>
              )}
            </HStack>
          </Box>
        }
        Footer={
          <Box>
            <Center>
              <Heading>PHASE</Heading>
            </Center>
            {salez_storage?.last_phase ? (
              <Box>
                <Text>{`FROM: ${moment
                  .unix(salez_storage.last_phase.from_date.toNumber())
                  .format()}`}</Text>
                <Text>{`TO: ${moment
                  .unix(salez_storage.last_phase.to_date.toNumber())
                  .format()}`}</Text>

                {salez_storage.last_phase.white_listed_only && (
                  <Box>
                    <Text>{`WHITE LISTED ONLY `}</Text>
                    <Text>{`MAX MINT PER WALLET: ${salez_storage.last_phase.max_tokens_per_white_listed_wallet.toNumber()}`}</Text>
                  </Box>
                )}
                <Text>{`COST: ${
                  salez_storage.last_phase.utz_cost.toNumber() / 1000000
                } TZ`}</Text>
                <Text>{`MAX MINT PER TX: ${salez_storage.last_phase.max_mint_per_tx.toNumber()}`}</Text>
                <Text>{`SUPPLY: ${
                  salez_storage.last_phase.to_token_id.toNumber() -
                  salez_storage.last_phase.from_token_id.toNumber()
                }`}</Text>

                {salez_storage && (
                  <Text>{`REMAIN: ${
                    salez_storage.last_phase.to_token_id.toNumber() -
                    salez_storage.next_token_id.toNumber()
                  }`}</Text>
                )}

                {allowed_coins.map((coin) => (
                  <Text key={coin.token_id}>{`USABLE COIN ID #${coin.token_id
                    .toString()
                    .padStart(4, "0")} [REQUIRED ${
                    coin.required
                  } COINS/MINT]`}</Text>
                ))}
              </Box>
            ) : (
              <Text bold>COMING SOON</Text>
            )}
          </Box>
        }
      />
    </Center>
  );
};

export default Mint;
