import { OpKind, TezosToolkit } from "@taquito/taquito";
import { SALE_CONTRACT_ADDRESS } from "../contracts/api";
import { IContractSalezStorage } from "../contracts/types";
import { ICoinInfo } from "./reducers";

export const getMaxPerTx = (
  salez_storage: IContractSalezStorage
  //onMaxPerTx?: (max_per_tx: number) => void
) => {
  let max_per_tx = 0;
  try {
    if (salez_storage.last_phase) {
      const query_max_per_tx = salez_storage.last_phase.max_mint_per_tx;
      max_per_tx = query_max_per_tx.toNumber();
    }
  } catch (error) {
    console.log(error);
  }
  //onMaxPerTx && onMaxPerTx(max_per_tx);
  return max_per_tx;
};
export const getAllowedCoins = (
  salez_storage: IContractSalezStorage
  //onCoins?: (allowed_coins: ICoinInfo[]) => void
) => {
  let allowed_coin: ICoinInfo[] = [];
  try {
    if (salez_storage.allowed_coin) {
      const query_allowed_coins = salez_storage.allowed_coin.entries();
      for (let [token_id, required] of query_allowed_coins) {
        allowed_coin = allowed_coin.concat({
          token_id: token_id.toNumber(),
          required: required.toNumber(),
        });
      }
    }
  } catch (error) {
    console.log(error);
  }
  //onCoins && onCoins(allowed_coin);
  return allowed_coin;
};
export const userMintTZ = async (
  tezos: TezosToolkit,
  quantity: number,
  cost: number
) => {
  try {
    const contract = await tezos.wallet.at(SALE_CONTRACT_ADDRESS);
    const operation = contract.methods
      .mint(0, quantity, false)
      .toTransferParams({ amount: quantity * cost, mutez: true });
    //const operation_cost = await tezos.estimate.transfer(operation);
    //console.log(operation_cost);
    const batch_op = await tezos.wallet.batch([
      {
        kind: OpKind.TRANSACTION,

        ...operation,
        //amount: quantity * cost + operation_cost.totalCost,
        //mutez: true,
      },
    ]);
    const batch_response = await batch_op.send();
    batch_response.confirmation();
  } catch (error) {
    console.log(error);
  }
  return null;
};
export const userMintCoin = async (
  tezos: TezosToolkit,
  quantity: number,
  coin_id: number = 0
) => {
  try {
    const contract = await tezos.wallet.at(SALE_CONTRACT_ADDRESS);
    const operation = contract.methods
      .mint(coin_id, quantity, true)
      .toTransferParams({});
    //const operation_cost = await tezos.estimate.transfer(operation);
    //console.log(operation_cost);

    const batch_op = await tezos.wallet.batch([
      {
        kind: OpKind.TRANSACTION,

        ...operation,
        //amount: operation_cost.totalCost,
        //mutez: true,
      },
    ]);
    const batch_response = await batch_op.send();
    batch_response.confirmation();
  } catch (error) {
    console.log(error);
  }
  return null;
};
