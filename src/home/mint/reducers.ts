import { createSlice, PayloadAction } from "@reduxjs/toolkit";
export interface ICoinInfo {
  token_id: number;
  required: number;
}
interface IMintApi {
  allowed_coins: ICoinInfo[];
  max_mint_per_tx: number;
}
const initial_state: IMintApi = {
  allowed_coins: [],
  max_mint_per_tx: 10,
};
const mint_api = createSlice({
  name: "mint_api",
  initialState: initial_state,
  reducers: {
    set_allowed_coins: (state, action: PayloadAction<ICoinInfo[]>) => {
      state.allowed_coins = action.payload;
    },
    set_max_mint_per_tx: (state, action: PayloadAction<number>) => {
      state.max_mint_per_tx = action.payload;
    },
  },
});

export const { set_allowed_coins, set_max_mint_per_tx } = mint_api.actions;
export default mint_api.reducer;
