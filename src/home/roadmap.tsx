import React from "react";
import {
  Box,
  Center,
  Heading,
  HStack,
  VStack,
  Image,
  Text,
  useBreakpointValue,
  Divider,
} from "native-base";
import MyCycling from "./components/my_cycling";
import { MaterialCommunityIcons } from "@expo/vector-icons";
const RoadMap = () => {
  const iconSize = useBreakpointValue({
    base: 25,
    lg: 50,
  });
  return (
    <VStack w="100%" justifyContent="center" alignItems="center" mt="10px">
      <Box
        w={{ base: "300px", lg: "750px" }}
        justifyContent="center"
        alignItems="center"
      >
        <Box textAlign="center">
          <Heading fontSize={{ base: "2xl", lg: "6xl" }}>ROAD MAP</Heading>
        </Box>
        <HStack
          justifyContent="center"
          alignItems="center"
          my="5px"
          bg="gray.900"
          rounded="md"
        >
          <Box w={{ base: "100px", lg: "200px" }} px="10px">
            <Image
              alt="WL"
              src={require("../../assets/wl.png")}
              w={{ base: "100px", lg: "200px" }}
              h={{ base: "100px", lg: "200px" }}
              resizeMode="center"
            />
          </Box>
          <Box w={{ base: "200px", lg: "550px" }} px="10px">
            <Text wordBreak="break-word">
              Start: July 01, 2022 00:00 UTC+0{`\n`}
              End: July 29, 2022 00:00 UTC+0{`\n`}
              About: Verify in our discord server and drop your wallet
              https://discord.gg/UxB82twcym
            </Text>
          </Box>
        </HStack>
        <MaterialCommunityIcons
          name="source-commit"
          size={iconSize}
          color="black"
        />
        <HStack
          justifyContent="center"
          alignItems="center"
          my="5px"
          bg="gray.900"
          rounded="md"
        >
          <Box w={{ base: "100px", lg: "200px" }} px="10px">
            <Image
              alt="PS"
              src={require("../../assets/ps.png")}
              w={{ base: "100px", lg: "200px" }}
              h={{ base: "100px", lg: "200px" }}
              resizeMode="center"
            />
          </Box>
          <Box w={{ base: "200px", lg: "550px" }} px="10px">
            <Text wordBreak="break-word">
              Start: July 30, 2022 00:00 UTC+0 {`\n`}
              End: July 31, 2022 00:00 UTC+0{`\n`}
              About: Access through our dapp at https://nftfingerz.xyz
            </Text>
          </Box>
        </HStack>
        <MaterialCommunityIcons
          name="source-commit"
          size={iconSize}
          color="black"
        />
        <HStack
          justifyContent="center"
          alignItems="center"
          my="5px"
          bg="gray.900"
          rounded="md"
        >
          <Box w={{ base: "100px", lg: "200px" }} px="10px">
            <Image
              alt="MS"
              src={require("../../assets/ms.png")}
              w={{ base: "100px", lg: "200px" }}
              h={{ base: "100px", lg: "200px" }}
              resizeMode="center"
            />
          </Box>
          <Box w={{ base: "200px", lg: "550px" }} px="10px">
            <Text wordBreak="break-word">
              Start: July 31, 2022 00:00 UTC+0 {`\n`}
              End: August 02, 2022 00:00 UTC+0{`\n`}
              About: Access through our dapp at https://nftfingerz.xyz
            </Text>
          </Box>
        </HStack>
        <MaterialCommunityIcons
          name="source-commit"
          size={iconSize}
          color="black"
        />
        <HStack
          justifyContent="center"
          alignItems="center"
          my="5px"
          bg="gray.900"
          rounded="md"
        >
          <Box w={{ base: "100px", lg: "200px" }} px="10px">
            <Image
              alt="FR"
              src={require("../../assets/fr.png")}
              w={{ base: "100px", lg: "200px" }}
              h={{ base: "100px", lg: "200px" }}
              resizeMode="center"
            />
          </Box>
          <Box w={{ base: "200px", lg: "550px" }} px="10px">
            <Text wordBreak="break-word">
              Start: August 02, 2022 00:00 UTC+0 {`\n`}
              End: August 03, 2022 00:00 UTC+0{`\n`}
              About: We will reveal minted tokens all at once, following the
              higher antiscam protocol
            </Text>
          </Box>
        </HStack>
        <MaterialCommunityIcons
          name="source-commit"
          size={iconSize}
          color="black"
        />
        <HStack
          justifyContent="center"
          alignItems="center"
          my="5px"
          bg="gray.900"
          rounded="md"
        >
          <Box w={{ base: "100px", lg: "200px" }} px="10px">
            <Image
              alt="MISSION"
              src={require("../../assets/mission.png")}
              w={{ base: "100px", lg: "200px" }}
              h={{ base: "100px", lg: "200px" }}
              resizeMode="center"
            />
          </Box>
          <Box w={{ base: "200px", lg: "550px" }} px="10px">
            <Text wordBreak="break-word">
              Start: August 03, 2022 00:00 UTC+0 {`\n`}
              End: August 04, 2022 00:00 UTC+0{`\n`}
              About: Public draw to assign the wanted tokens, following the
              higher antiscam protocol
            </Text>
          </Box>
        </HStack>
        <MaterialCommunityIcons
          name="source-commit"
          size={iconSize}
          color="black"
        />
        <HStack
          justifyContent="center"
          alignItems="center"
          my="5px"
          bg="gray.900"
          rounded="md"
        >
          <Box w={{ base: "100px", lg: "200px" }} px="10px">
            <Image
              alt="REWARD"
              src={require("../../assets/reward.png")}
              w={{ base: "100px", lg: "200px" }}
              h={{ base: "100px", lg: "200px" }}
              resizeMode="center"
            />
          </Box>
          <Box w={{ base: "200px", lg: "550px" }} px="10px">
            <Text wordBreak="break-word">
              Start: August 04, 2022 00:00 UTC+0 {`\n`}
              End: August 08, 2022 00:00 UTC+0 {`\n`}
              About: Hunting beggin, the first person to get the listed Fingerz
              by the previous draw will be able to collect the pool prize
            </Text>
          </Box>
        </HStack>
        <MaterialCommunityIcons
          name="source-commit"
          size={iconSize}
          color="black"
        />
        <HStack
          justifyContent="center"
          alignItems="center"
          my="5px"
          bg="gray.900"
          rounded="md"
        >
          <Box w={{ base: "100px", lg: "200px" }} px="10px">
            <Image
              alt="REPEAT"
              src={require("../../assets/repeat.png")}
              w={{ base: "100px", lg: "200px" }}
              h={{ base: "100px", lg: "200px" }}
              resizeMode="center"
            />
          </Box>
          <Box w={{ base: "200px", lg: "550px" }} px="10px">
            <Text wordBreak="break-word">
              Start: - {`\n`}
              End:- {`\n`}
              About: Once we reach this point, everything start over until the
              complete collection is sold out.
            </Text>
          </Box>
        </HStack>
      </Box>
    </VStack>
  );
};

export default RoadMap;
