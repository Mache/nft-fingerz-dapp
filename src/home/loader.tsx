import { Heading, HStack, Spinner, Text, VStack } from "native-base";
import React from "react";
import { useSelector } from "react-redux";
import { RootState } from "../settings/store";
import MyModal from "./components/my_modal";

const Loading = () => {
  const { isLoading } = useSelector((state: RootState) => state.leftPanel);
  return (
    <MyModal is_open={isLoading}>
      <HStack
        space={2}
        justifyContent="center"
        alignItems="center"
        bg={{ base: "black" }}
        rounded={{ base: "md", lg: "lg" }}
        p={{ base: "10px" }}
      >
        <Spinner
          //accessibilityLabel="Loading"
          size={{ base: "sm", lg: "lg" }}
        />
        <VStack>
          <Heading color="primary.500" lineHeight={{ base: "xs" }}>
            Loading
          </Heading>
          <Text
            color="primary.500"
            lineHeight={{ base: "xs" }}
            mt={{ lg: "-15px" }}
          >
            CONTRACT DATA
          </Text>
        </VStack>
      </HStack>
    </MyModal>
  );
};

export default Loading;
