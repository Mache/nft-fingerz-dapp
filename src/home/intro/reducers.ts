import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { DEV } from "../contracts/api";

interface IIntroApi {
  open: boolean;
}
const initialState: IIntroApi = {
  open: true, //!DEV,
};
const intro_api = createSlice({
  name: "intro_api",
  initialState: initialState,
  reducers: {
    set_open: (state, action: PayloadAction<boolean>) => {
      state.open = action.payload;
    },
  },
});

export const { set_open } = intro_api.actions;
export default intro_api.reducer;
