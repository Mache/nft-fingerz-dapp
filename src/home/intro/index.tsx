import { Box, Button, Center, Modal, useBreakpointValue } from "native-base";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../settings/store";
import { set_open } from "./reducers";

import { MaterialCommunityIcons } from "@expo/vector-icons";
import YouTube, { YouTubeEvent } from "react-youtube";
import { useWindowDimensions } from "react-native";
import MyModal from "../components/my_modal";
const Intro = () => {
  const { open } = useSelector((state: RootState) => state.intro);
  const dispatch = useDispatch();
  //const video = React.useRef<YouTube>(null);
  const [player, setPlayer] = React.useState<null | YouTubeEvent>(null);
  const { width, height } = useWindowDimensions();
  const iconSize = useBreakpointValue({
    base: 24,
    lg: 34,
  });
  return (
    <MyModal is_open={open} zIndex={15}>
      <Box>
        <Box>
          <YouTube
            videoId="9q3QPC8vbMk"
            opts={{
              width: width * 0.9,
              height: height > width ? width * 0.9 : height * 0.9,
              playerVars: { autoplay: 0, controls: 0 },
            }}
            onReady={(event) => {
              setPlayer(event);
            }}
            //ref={video}
          />
        </Box>
        <Box>
          <Button
            leftIcon={
              <MaterialCommunityIcons
                name="close"
                size={iconSize}
                color="white"
              />
            }
            onPress={() => {
              dispatch(set_open(false));
              if (player) {
                player.target.stopVideo();
              }
            }}
          >
            CLOSE
          </Button>
        </Box>
      </Box>
    </MyModal>
  );
};
export default Intro;
/*
<Video
              ref={video}
              source={{
                uri: require("../../../assets/Intro.mp4"),
              }}
              useNativeControls
              resizeMode="contain"
              shouldPlay={open}
              onPlaybackStatusUpdate={(status) => setStatus(() => status)}
              style={{ height: 640, width: 1138 }}
            />
*/
