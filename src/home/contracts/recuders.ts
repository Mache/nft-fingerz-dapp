import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { IContractFA2Storage, IContractSalezStorage } from "./types";

interface IContractApi {
  salez_storage: null | IContractSalezStorage;
  fa2_storage: null | IContractFA2Storage;
}
const initial_state: IContractApi = {
  salez_storage: null,
  fa2_storage: null,
};
const contract_api = createSlice({
  name: "contract_api",
  initialState: initial_state,
  reducers: {
    set_fa2_storage: (
      state,
      action: PayloadAction<null | IContractFA2Storage>
    ) => {
      state.fa2_storage = action.payload;
    },
    set_salez_storage: (
      state,
      action: PayloadAction<null | IContractSalezStorage>
    ) => {
      state.salez_storage = action.payload;
    },
  },
});

export const { set_salez_storage, set_fa2_storage } = contract_api.actions;
export default contract_api.reducer;
