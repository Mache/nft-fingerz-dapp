import { BigMapAbstraction, MichelsonMap } from "@taquito/taquito";
import Big from "big.js";
export interface IPhase {
  name: string;
  utz_cost: Big;
  from_date: Big;
  to_date: Big;
  from_token_id: Big;
  to_token_id: Big;
  white_listed_only: boolean;
  max_tokens_per_white_listed_wallet: Big;
  max_mint_per_tx: Big;
  last_token_block_level: null | Big;
  minted_tokens: null | Big;
  distribution_starting_from: null | Big;
}
export interface IContractReward {
  wanted: { token_id: Big; level: Big }[];
  bonuz: { token_id: Big; level: Big }[];
  expire_on: Big;
  reward_ratio: Big;
  bonuz_jailed: Big[];
  multiplied: Big;
  cashed: Big;
  pool: Big;
  complete: boolean;
  created_with_block_level: Big;
  fugitives_hash: string;
  created: Big;
}
export interface IContractSalezStorage {
  administrator: string;
  collection_max_tokens: Big;
  next_token_id: Big;
  next_coin_id: Big;
  next_phase_id: Big;
  provenance_hash: string;
  managers: string[];
  fa2_contract: string;
  hidden_folder_hash: string;
  white_list: MichelsonMap<string, Big[]>;
  last_token_block_level: Big;
  allowed_coin: MichelsonMap<Big, Big>;
  last_phase: null | IPhase;
  phases_history: MichelsonMap<Big, IPhase>;
  rewards_mint_share: Big;
  rewards_list: MichelsonMap<Big, IContractReward>;
  next_reward_id: Big;
  jailed: MichelsonMap<Big, null>;
  ledger: BigMapAbstraction;
  metadata: BigMapAbstraction;
  supply: BigMapAbstraction;
  token_metadata: BigMapAbstraction;
}
export interface IContractLevel {
  level: string;
  exp: Big;
}
export interface IContractFA2Storage {
  administrator: string;
  exp_chart: MichelsonMap<Big, IContractLevel>;
  fa2_manager: string;
  next_token_id: Big;
  next_token_id_to_reveal: Big;
  ledger: BigMapAbstraction;
  metadata: BigMapAbstraction;
  operators: BigMapAbstraction;
  token_metadata: BigMapAbstraction;
}

export interface IContractTokenMetadata {
  token_id: Big;
  token_info: MichelsonMap<string, string>;
}
/*{
  "": string;
  container: string;
  exp: Big;
  level: Big;
};*/
export type IAddress = string;
