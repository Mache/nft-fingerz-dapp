import { TezosToolkit } from "@taquito/taquito";
import { Network, NetworkType } from "@airgap/beacon-sdk";
import { IContractFA2Storage, IContractSalezStorage } from "./types";

export const DEV = false;
const RPC_NODES = {
  MAIN: "https://mainnet.api.tez.ie",
  TEST: "https://jakartanet.ecadinfra.com",
};
export const RPC_NODE = DEV ? RPC_NODES.TEST : RPC_NODES.MAIN;
const FA2_CONTRACTS = {
  MAIN: "KT1D71degk4dMLtAWdpWgk8Ycg1tfeNdBNHz",
  JAKARTANET: "KT1NkgDyfP2Y3bLLcvwckScVbi9hLxZRboFs",
  ITHACANET: "KT1BhHk3rXNFYy8vd34RAQokgBhXsHr2hQbG",
};
const SALE_CONTRACTS = {
  MAIN: "KT1W8Q1facYYeiwMLh1JVEGE9YZcsUXwXrk3",
  JAKARTANET: "KT1RY1kkQ3uAZc3NakWeVw1kveas7XFPoSsp",
  ITHACANET: "KT1SHzkNbXoH1oW5YdKEeEsYSt6B5PSK7SmD",
};
export const SALE_CONTRACT_ADDRESS = DEV
  ? SALE_CONTRACTS.JAKARTANET
  : SALE_CONTRACTS.MAIN;
export const FA2_CONTRACT_ADDRESS = DEV
  ? FA2_CONTRACTS.JAKARTANET
  : FA2_CONTRACTS.MAIN;

export const network: Network = DEV
  ? { type: NetworkType.JAKARTANET }
  : { type: NetworkType.MAINNET };

export const getFA2Storage = async (
  tezos: TezosToolkit
  //onData?: (fa2_storage: IContractFA2Storage | null) => void
) => {
  let fa2_storage: IContractFA2Storage | null = null;
  try {
    console.log("Getting contract");

    const query_contract = await tezos.contract.at(FA2_CONTRACT_ADDRESS);
    fa2_storage = await query_contract.storage<IContractFA2Storage>();
  } catch (error) {
    console.log(error);
  }
  //onData && onData(fa2_storage);
  return fa2_storage;
};
export const getSalezStorage = async (
  tezos: TezosToolkit
  //onData?: (sale_storage: IContractSalezStorage | null) => void
) => {
  let sale_storage: IContractSalezStorage | null = null;
  try {
    console.log("Getting contract");
    const query_contract = await tezos.contract.at(SALE_CONTRACT_ADDRESS);
    sale_storage = await query_contract.storage<IContractSalezStorage>();
  } catch (error) {
    console.log(error);
  }
  //onData && onData(sale_storage);
  return sale_storage;
};
