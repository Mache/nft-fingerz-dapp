import { getFunctions, httpsCallable } from "firebase/functions";

export const getTwitts = async (onTwitts: (twitts: string) => void) => {
  try {
    const myFunctions = getFunctions();
    const twitts = httpsCallable<null, string>(myFunctions, "twitts");
    const result = await twitts();
    onTwitts(result.data);
  } catch (error) {
    console.log(error);
  }
};
