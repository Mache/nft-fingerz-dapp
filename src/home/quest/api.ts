import { onValue, ref } from "firebase/database";
import { database } from "../../../App";
import { IUserQuests } from "./types";

export const queryQuests = async (
  address: string,
  onQuery: (quests: IUserQuests | null) => void
) => {
  try {
    const userQuestsRef = ref(database, `quests/${address}`);
    onValue(userQuestsRef, (snapshot) => {
      if (snapshot.exists()) {
        const queryQuest = snapshot.val() as IUserQuests;
        onQuery(queryQuest);
      } else {
        onQuery(null);
      }
    });
  } catch (error) {
    console.log(error);
  }
};
