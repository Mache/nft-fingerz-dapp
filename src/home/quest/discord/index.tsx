import { Button, Box, Text, useBreakpointValue } from "native-base";
import React, { useEffect, useState } from "react";
import StyledVStack from "../../components/styledVStack";
import BText from "../../components/blackText";
import BHeading from "../../components/blackHeading";
import {
  completeQuest,
  setDiscordProfile,
  startQuest,
  IDiscordIdentity,
  IDiscordQuest,
  discordQuest,
  questStatus,
  IDiscordQuery,
} from "./api";
import { useSelector } from "react-redux";
import { RootState } from "../../../settings/store";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import ItemQuest from "../items";
import { REPEATABLE } from "../reducers";

const Discord = () => {
  const {
    wallet: { address },
    leftPanel: { query },
    quests: { discord_link },
  } = useSelector((state: RootState) => state);
  const [fetching, setFetching] = useState(false);
  const [discordIdentity, setDiscordIdentity] =
    useState<IDiscordIdentity | null>(null);
  const iconSize = useBreakpointValue({
    base: 24,
    lg: 34,
  });

  useEffect(() => {
    if (address) {
      setFetching(true);
      discordQuest(query).then((identity) => {
        if (identity) {
          setDiscordIdentity(identity);
        }
        setFetching(false);
      });
    }
  }, [address]);
  return (
    <Box w={{ base: "300px", lg: "700px" }}>
      <StyledVStack>
        <BHeading>DISCORD LINK</BHeading>
        <BText>
          Connect your discord profile to our Dapp to access exclusive content
        </BText>
        <BText>{`REPEATABLE: ${REPEATABLE.ONCE}`}</BText>
        <ItemQuest quest={discord_link} />
        {discord_link.timesCompleted === 0 && (
          <Box
            w={{ base: "100%" }}
            justifyContent={{ base: "center" }}
            alignItems={{ base: "center" }}
            my={{ base: "10px" }}
          >
            {discordIdentity === null ? (
              <Button
                disabled={fetching}
                isLoading={fetching}
                leftIcon={
                  <MaterialCommunityIcons
                    name="comment-question-outline"
                    size={iconSize}
                    color="white"
                  />
                }
                onPress={() => {
                  startQuest();
                }}
              >
                <Text>PRESS TO START</Text>
              </Button>
            ) : (
              <Button
                disabled={fetching}
                isLoading={fetching}
                leftIcon={
                  <MaterialCommunityIcons
                    name="exclamation-thick"
                    size={iconSize}
                    color="white"
                  />
                }
                onPress={async () => {
                  if (address && discordIdentity) {
                    setFetching(true);
                    await setDiscordProfile(address, discordIdentity);
                    await completeQuest(address);
                    setFetching(false);
                  }
                }}
              >
                <Text>PRESS TO COMPLETE</Text>
              </Button>
            )}
          </Box>
        )}
        <Box
          justifyContent={{ base: "center" }}
          alignItems={{ base: "center" }}
          my={{ base: "10px" }}
        >
          {discord_link.timesCompleted >= 1 && <BHeading>COMPLETED</BHeading>}
        </Box>
      </StyledVStack>
    </Box>
  );
};
export default Discord;
