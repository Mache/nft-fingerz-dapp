import axios from "axios";
import * as Linking from "expo-linking";
import { get, onValue, ref, set } from "firebase/database";
import { database } from "../../../../App";
import { IProfile, IQueryParams } from "../../wallet/reducers";
import { IQuestBase, questBase } from "../types";
import { getUnixTime } from "../../time/api";

export interface IDiscordQuest extends IQuestBase {}
export interface IDiscordQuery {
  code: string;
  quest: string;
}

export const QUEST_NAME = "discord_link";
export const DISCORD_OAUTH_LINK =
  "https://discord.com/api/oauth2/authorize?client_id=1004862222712979547&redirect_uri=https%3A%2F%2Fnftfingerz.xyz%2Fholders%2F%3Fquest%3Ddiscord_link&response_type=code&scope=identify";

export const startQuest = () => {
  Linking.openURL(DISCORD_OAUTH_LINK);
};
export type IOauthResponse = {
  access_token: string;
  token_type: string;
  expires_in: number;
  refresh_token: string;
  scope: string;
};
export type IDiscordIdentity = {
  id: number;
  username: string;
  discriminator: number;
};
const config = require("../../../../config.json");
const getToken = async (code: string) => {
  try {
    const data = new URLSearchParams({
      client_id: config.discord.clientID,
      client_secret: config.discord.clientSecret,
      code,
      grant_type: "authorization_code",
      redirect_uri: `https://nftfingerz.xyz/holders/?quest=discord_link`,
      scope: "identify",
    });
    const queryToken = await axios.post<IOauthResponse>(
      "https://discord.com/api/oauth2/token",
      data,
      {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
      }
    );
    return queryToken.data;
  } catch (error) {
    console.log(error);
    return null;
  }
};
const getIdentity = async (queryToken: IOauthResponse) => {
  try {
    const { token_type, access_token } = queryToken;
    const query_identity = await axios.get(
      "https://discord.com/api/users/@me",
      {
        headers: {
          authorization: `${token_type} ${access_token}`,
        },
      }
    );
    return query_identity.data as IDiscordIdentity;
  } catch (error) {
    console.log(error);
    return null;
  }
};
export const discordQuest = async (query: IQueryParams) => {
  try {
    if (query.code && query.quest) {
      const { code, quest } = query;
      if (quest === QUEST_NAME) {
        const queryToken = await getToken(code);
        if (queryToken) {
          const queryIdentity = await getIdentity(queryToken);
          if (queryIdentity) {
            return queryIdentity;
          }
        }
      }
    }
  } catch (error) {
    console.log(error);
  }
  return null;
};
export const setDiscordProfile = async (
  address: string,
  profile: IDiscordIdentity
) => {
  try {
    const profileRef = ref(database, `profiles/${address}/discord`);
    await set(profileRef, profile);
  } catch (error) {
    console.log(error);
  }
};
export const getProfile = async (address: string) => {
  let profile: null | IProfile = null;
  try {
    const profileRef = ref(database, `profiles/${address}/profile`);
    const queryProfile = await get(profileRef);
    if (queryProfile.exists()) {
      profile = queryProfile.val() as IProfile;
    }
  } catch (error) {
    console.log(error);
  }
  return profile;
};

export const completeQuest = async (address: string) => {
  try {
    const unixTime = await getUnixTime();
    const profile = await getProfile(address);
    const questRef = ref(database, `quests/${address}/${QUEST_NAME}`);
    await set(questRef, {
      ...questBase,
      timesCompleted: 1,
      lastTimeCompleted: unixTime,
      profile,
    });
  } catch (error) {
    console.log(error);
  }
  return true;
};
export const questStatus = async (
  address: string,
  onStatus: (status: IDiscordQuest | null) => void
) => {
  try {
    const questRef = ref(database, `quests/${address}/${QUEST_NAME}`);
    onValue(questRef, (snapshot) => {
      if (snapshot.exists()) {
        const queryQuest = snapshot.val() as IDiscordQuest;
        onStatus(queryQuest);
      } else {
        onStatus(null);
      }
    });
  } catch (error) {
    console.log(error);
  }
};
