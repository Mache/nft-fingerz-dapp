import { IProfile } from "../wallet/reducers";

export interface IQuestBase {
  timesCompleted: number;
  lastTimeCompleted: number;
  timesRewarded: number;
  rewardID: number;
  rewardName: string;
  rewardAmount: number;
  opHash: string[];
  profile: null | IProfile;
}

export const questBase: IQuestBase = {
  timesCompleted: 0,
  lastTimeCompleted: 0,
  timesRewarded: 0,
  rewardID: 0,
  rewardName: "FINGERZ COIN",
  rewardAmount: 1,
  opHash: [],
  profile: null,
};

export interface IUserQuests {
  discord_link: IQuestBase;
  twitter: IQuestBase;
}
