import {
  Button,
  Box,
  HStack,
  useBreakpointValue,
  Spinner,
  Input,
  Stack,
  InputGroup,
  InputLeftAddon,
  Text,
  Heading,
} from "native-base";
import React, { useEffect, useState } from "react";
import { REPEATABLE, set_twitter_meta } from "../reducers";
import StyledVStack from "../../components/styledVStack";
import BText from "../../components/blackText";
import BHeading from "../../components/blackHeading";
import { completeQuest, generateTwitterLink, getTwitterMeta } from "./api";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../../settings/store";
import moment from "moment";
import ItemQuest from "../items";

const Twitter = () => {
  const {
    wallet: { address, profile },
    quests: { twitter, twitterMeta },
    time: { unix_time },
  } = useSelector((state: RootState) => state);
  const [fetching, setFetching] = useState(false);
  const [link, setLink] = useState<string>("");
  const dispatch = useDispatch();
  const iconSize = useBreakpointValue({
    base: 24,
    lg: 34,
  });

  useEffect(() => {
    if (address) {
      setFetching(true);
      getTwitterMeta().then((meta) => {
        if (meta) {
          dispatch(set_twitter_meta(meta));
        }
        setFetching(false);
      });
    }
  }, [address]);

  return (
    <Box w={{ base: "300px", lg: "700px" }}>
      <StyledVStack>
        <BHeading>TWITTER</BHeading>
        <BText>
          {`Promote NFT Fingerz on Twitter to get a Community Participation Reward.
          \nIt's easy just click "START".
          \nPublish on twitter the pre-made content without modifications. 
          \nCopy the share link and paste it in the input box below.
          \nThen press "COMPLETE" and wait until system check completion.`}
        </BText>
        <BText>{`REPEATABLE: ${REPEATABLE.WEEKLY}`}</BText>
        <ItemQuest quest={twitter} />
        {twitter.timesCompleted > 0 && (
          <Box>
            <BText>{`LAST TIME COMPLETED: ${moment
              .unix(twitter.lastTimeCompleted)
              .format()}`}</BText>
            <BText>{`ON COOLDOWN UNTIL: ${moment
              .unix(twitter.lastTimeCompleted + twitterMeta.cooldown)
              .format()}`}</BText>
          </Box>
        )}

        {unix_time > twitter.lastTimeCompleted + twitterMeta.cooldown ? (
          <Box>
            <Box my={{ base: "5px" }}>
              <Button
                onPress={() => {
                  window.open(generateTwitterLink(twitterMeta.text), "_blank");
                }}
              >
                <Text>START</Text>
              </Button>
            </Box>
            <Stack alignItems="center">
              <InputGroup w="100%">
                <InputLeftAddon children={"SHARE LINK:"} />
                <Input
                  value={link}
                  onChangeText={(text) => setLink(text.trim())}
                  w="100%"
                  placeholder="LINK"
                />
              </InputGroup>
            </Stack>
            <Box my={{ base: "5px" }}>
              <Button
                disabled={fetching}
                isLoading={fetching}
                onPress={async () => {
                  if (profile && twitter) {
                    try {
                      setFetching(true);
                      await completeQuest(link, twitter, twitterMeta, profile);
                      setFetching(false);
                    } catch (error) {
                      console.log(error);
                    }
                  }
                }}
              >
                <Text>COMPLETE</Text>
              </Button>
            </Box>
          </Box>
        ) : (
          <Box>
            <Heading>ON COOLDOWN</Heading>
          </Box>
        )}
      </StyledVStack>
    </Box>
  );
};
export default Twitter;
