import { get, onValue, ref, serverTimestamp, set } from "firebase/database";
import { getFunctions, httpsCallable } from "firebase/functions";
import { database } from "../../../../App";
import { getUnixTime } from "../../time/api";
import { ITwitterMeta } from "../../tools/types";
import { IProfile } from "../../wallet/reducers";
import { IQuestBase } from "../types";
export interface ITwitterQuest extends IQuestBase {}

export const TWITTER_QUEST = "twitter";

export const generateTwitterLink = (text: string) => {
  const query = new URLSearchParams({
    text,
  });
  return `https://twitter.com/intent/tweet?${query.toString()}`;
};
export const setTwitterProfile = async (address: string, username: string) => {
  try {
    const profile_ref = ref(database, `profiles/${address}/twitter`);
    await set(profile_ref, username);
  } catch (error) {
    console.log(error);
  }
};
export const getTwitterMeta = async () => {
  let twitterMeta: ITwitterMeta | null = null;
  try {
    const twitterMetaRef = ref(database, `dash/${TWITTER_QUEST}`);
    const queryTwitterMeta = await get(twitterMetaRef);
    if (queryTwitterMeta.exists()) {
      twitterMeta = queryTwitterMeta.val();
    }
  } catch (error) {
    console.log(error);
  }
  return twitterMeta;
};

export type ITwitterRequest = {
  link: string;
  profile: IProfile;
};
export type ITwitterResponse = {
  id: string;
  text: string;
  user: string;
};
export const completeQuest = async (
  link: string,
  quest: ITwitterQuest,
  meta: ITwitterMeta,
  profile: IProfile
): Promise<ITwitterQuest> => {
  let twitterQuest = { ...quest, profile };
  try {
    const unixTime = await getUnixTime();
    if (unixTime >= twitterQuest.lastTimeCompleted + meta.cooldown) {
      const completeTwitterQuest = httpsCallable<
        ITwitterRequest,
        ITwitterResponse
      >(getFunctions(), "completeTwitterQuest");
      const queryTwitter = await completeTwitterQuest({ link, profile });
    }
  } catch (error) {
    console.log(error);
  }
  return twitterQuest;
};

export const questStatus = async (
  address: string,
  onStatus: (status: ITwitterQuest) => void
) => {
  try {
    const quest_ref = ref(database, `quests/${address}/twitter`);
    onValue(quest_ref, (snapshot) => {
      if (snapshot.exists()) {
        const queryQuest = snapshot.val() as ITwitterQuest;
        onStatus(queryQuest);
      }
    });
  } catch (error) {
    console.log(error);
  }
};
