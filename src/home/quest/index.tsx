import { Heading, Box, Text, Image, HStack } from "native-base";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../settings/store";
import { queryQuests } from "./api";
import Discord from "./discord";
import { set_discord_link_quest, set_twitter_quest } from "./reducers";
import Twitter from "./twitter";

const Quest = () => {
  const { address } = useSelector((state: RootState) => state.wallet);
  const dispatch = useDispatch();
  useEffect(() => {
    if (address) {
      queryQuests(address, (quests) => {
        if (quests?.twitter) {
          dispatch(set_twitter_quest(quests.twitter));
        }
        if (quests?.discord_link) {
          dispatch(set_discord_link_quest(quests.discord_link));
        }
      });
    }
  }, [address]);
  return (
    <Box
      alignItems="center"
      justifyContent={{ base: "center" }}
      //bg="black"
      //rounded="md"
      w={{ base: "350px", lg: "750px" }}
      minH={{ base: "250px", lg: "550px" }}
      p={{ base: "10px" }}
    >
      <Box alignItems="center">
        <Heading>QUESTS</Heading>
        <HStack
          justifyContent={{ base: "center" }}
          alignItems={{ base: "center" }}
          space={{ base: "10px" }}
        >
          <Box>
            <Image
              alt="Bot"
              src={require("../../../assets/bot.png")}
              width={{ base: "90px", lg: "150px" }}
              height={{ base: "90px", lg: "150px" }}
              rounded="full"
              resizeMode="center"
              borderColor="black"
              borderWidth="5px"
            />
          </Box>
          <Box
            bg="gray.900"
            rounded="md"
            p={{ base: "10px" }}
            w={{ base: "60%" }}
          >
            <Text>
              List of tasks you can complete in the given time frame to acquire
              a reward detailed in each quest.
            </Text>
          </Box>
        </HStack>

        <Discord />
        <Twitter />
      </Box>
    </Box>
  );
};
export default Quest;
