import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ITwitterMeta } from "../tools/types";
import { IQuestBase, questBase } from "./types";
export enum REPEATABLE {
  ONCE = "ONCE",
  DAILY = "DAILY",
  WEEKLY = "WEEKLY",
  MONTHLY = "MONTHLY",
}
interface IQuestApi {
  discord_link: IQuestBase;
  twitter: IQuestBase;
  twitterMeta: ITwitterMeta;
}
const initial_state: IQuestApi = {
  discord_link: questBase,
  twitter: questBase,
  twitterMeta: { cooldown: 0, text: "" },
};
const quest_api = createSlice({
  name: "quest_api",
  initialState: initial_state,
  reducers: {
    set_discord_link_quest: (state, action: PayloadAction<IQuestBase>) => {
      state.discord_link = action.payload;
    },
    set_twitter_quest: (state, action: PayloadAction<IQuestBase>) => {
      state.twitter = action.payload;
    },
    set_twitter_meta: (state, action: PayloadAction<ITwitterMeta>) => {
      state.twitterMeta = action.payload;
    },
  },
});

export const { set_discord_link_quest, set_twitter_quest, set_twitter_meta } =
  quest_api.actions;
export default quest_api.reducer;
