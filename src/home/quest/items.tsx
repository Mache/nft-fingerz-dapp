import { Box } from "native-base";
import React from "react";
import BText from "../components/blackText";
import { REPEATABLE } from "./reducers";
import { IQuestBase } from "./types";

interface IProps {
  quest: IQuestBase;
}
const ItemQuest = ({ quest }: IProps) => {
  return (
    <Box>
      <BText>{`TIMES COMPLETED: ${quest && quest.timesCompleted}`}</BText>
      <BText>{`TIMES REWARDED: ${quest && quest.timesRewarded}`}</BText>
      <BText>{`REWARD ID: #${quest.rewardID
        .toString()
        .padStart(3, "0")}`}</BText>

      <BText>{`REWARD NAME: ${quest.rewardName}`}</BText>
      <BText>{`REWARD AMOUNT x${quest.rewardAmount}`}</BText>
      <BText>{`OP HASH ${
        quest.opHash && quest.opHash.length > 0
          ? quest.opHash.slice(-3).join("\n")
          : ""
      }`}</BText>
    </Box>
  );
};

export default ItemQuest;
