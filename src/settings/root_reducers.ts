import { combineReducers } from "@reduxjs/toolkit";
import wallet from "../home/wallet/reducers";
import leftPanel from "../home/leftPanel/reducers";
import inbox from "../home/inbox/reducers";
import time from "../home/time/reducers";
import contract from "../home/contracts/recuders";
import wanted from "../home/wanted/reducers";
import intro from "../home/intro/reducers";
import mine from "../home/mine/reducers";
import mint from "../home/mint/reducers";
import rarity from "../home/rarity/reducers";
import holders from "../home/holders/reducers";
import quests from "../home/quest/reducers";
const root_reducers = combineReducers({
  wallet,
  leftPanel,
  inbox,
  time,
  contract,
  wanted,
  intro,
  mine,
  mint,
  rarity,
  holders,
  quests,
});

export default root_reducers;
