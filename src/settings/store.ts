import { configureStore } from "@reduxjs/toolkit";
import rootReducer from "./root_reducers";
import logger from "redux-logger";

export const store = configureStore({
  reducer: rootReducer,
  middleware: [logger] as const,
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
