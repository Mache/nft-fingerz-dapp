import { LinearGradient } from "expo-linear-gradient";
import {
  extendTheme,
  IBadgeProps,
  IDividerProps,
  IHeadingProps,
  IInputProps,
  ITextProps,
} from "native-base";

const colors = {
  fingerz: {
    0: "#FF00FF",
    100: "#FFFF00",
    200: "#00FFFF",
  },
};
const config = {
  useSystemColorMode: false,
  //initialColorMode: "dark",
};
interface Components {
  Input: { defaultProps: IInputProps };
  Heading: { defaultProps: IHeadingProps };
  Divider: { defaultProps: IDividerProps };
  Button: {
    defaultProps:
      | IBadgeProps
      | { _hover: IBadgeProps; _pressed: IBadgeProps; _focus: IBadgeProps };
  };
  Text: { defaultProps: ITextProps };
}
const components: Components = {
  Text: {
    defaultProps: {
      color: "white",
      fontFamily: "body",
      fontWeight: "normal",
      fontSize: { base: "10px", lg: "16px" },
    },
  },
  Button: {
    defaultProps: {
      justifyContent: "center",
      alignItems: "center",
      shadow: "8",
      bg: { base: "gray.800" },
      borderColor: { base: "black" },
      _hover: {
        bg: { base: "gray.400" },
        borderColor: { base: "black" },
      },
      _pressed: {
        bg: { base: "gray.400" },
        borderColor: { base: "black" },
      },
      _focus: {
        bg: { base: "gray.800" },
        borderColor: { base: "black" },
      },
    },
  },
  Input: {
    defaultProps: {
      borderWidth: "2",
      bg: "white",
    },
  },
  Divider: {
    defaultProps: {
      thickness: "4",
      bg: "white",
    },
  },
  Heading: {
    defaultProps: {
      textAlign: "center",
      color: "white",
      fontWeight: "normal",
      fontFamily: "heading",
      //size: { base: "md", lg: "lg" },
      fontSize: { base: "2xl", lg: "6xl" },
    },
  },
};
const cnf_gen = (font_name: string) =>
  [100, 200, 300, 400, 500, 600, 700, 800].reduce((prev, curr) => {
    return { ...prev, [curr]: { normal: font_name } };
  }, {});

export const Lerty_Regular = require("../../assets/fonts/Lerty-Regular.otf");
export const Wizland = require("../../assets/fonts/WIZLAND.otf");
const fontConfig = {
  Lerty: cnf_gen("Lerty_Regular"),
  Wizland: cnf_gen("Wizland"),
};
const fonts = {
  heading: "Lerty",
  body: "Wizland",
};
export const dependencies = {
  "linear-gradient": LinearGradient,
};

export const customTheme = extendTheme({
  colors,
  config,
  components,
  fontConfig,
  fonts,
});
